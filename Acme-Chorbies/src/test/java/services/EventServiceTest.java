
package services;

import java.util.Calendar;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Chorbi;
import domain.CreditCard;
import domain.Event;
import domain.Manager;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class EventServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private EventService	eventService;
	@Autowired
	private ChorbiService	chorbiService;
	@Autowired
	private ManagerService	managerService;


	// Templates --------------------------------------------------------------

	/*
	 * A user who is not authenticated must be able to: Browse a listing that includes
	 * every event that was registered in the system. Past events must be greyed out;
	 * events that are going to be organised in less than one month and have seats available
	 * must also be somewhat highlighted; the rest of events must be displayed normally.
	 * 
	 * En este caso de uso se llevar� a cabo la visualizaci�n de un listado de eventos por parte
	 * de usuarios no autentificados y chorbies. Para el coloreados de las columnas de la lista se ha introducido
	 * un atributo para los enventos, llamado "type" el cual puede ser bien o "none", "pastEvent" o
	 * "oneMonthAndSeats". Puede producirse error cuando:
	 * 
	 * � El usuario que intenta acceder est� autentificado (manager o admin)
	 */
	public void listEvents(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que la persona no est� autentificada o lo est� como chorbi

			if (username != null)
				this.chorbiService.checkIfChorbi();
			else
				Assert.isTrue(username == null);

			// Visualizamos el listado de eventos
			this.eventService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * "An actor who is authenticated as an chorbi must be able to:
	 * - Register to an event as long as there are enough seats available"
	 * 
	 * En este casao de uso se llevara a cabo el registro de un chorbi en un evento
	 * Para forzar el error pueden darse casos como:
	 * 
	 * � Se intente esto siendo admin o sin autenticarse.
	 */

	public void registerChorbiInEvent(final String username, final int eventId, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);
			Event e;
			e = this.eventService.findOne2(eventId);
			this.eventService.getRegisterEvent(e);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	/*
	 * "An actor who is authenticated as an chorbi must be able to:
	 * - Unregister from an event to which he or she's registered"
	 * 
	 * En este casao de uso se llevara a cabo el desregistro de un chorbi en un evento
	 * Para forzar el error pueden darse casos como:
	 * 
	 * � Se intente esto siendo admin o sin autenticarse.
	 */

	public void unRegisterChorbiInEvent(final String username, final int eventId, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);
			Event e;
			e = this.eventService.findOne2(eventId);
			this.eventService.getUnRegisterEvent(e);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: Manage the events that
	 * he or she organises, which includes listing, registering, mod-ifying, and deleting them.
	 * In order to register a new event, he must have registered a valid credit card that must
	 * not expire in less than one day.
	 * 
	 * En este caso de uso se llevar� a cabo el listado de eventos de un manager autentificado. La edici�n
	 * y creaci�n se llevar� a cabo en otras plantillas para faclitar los ejemplos. Para forzar el error
	 * pueden ocurrir cualquiera de los siguientes casos:
	 * 
	 * � El usuario no esta autentificado
	 * � El usuario est� autentificado pero no como manager
	 */
	public void listEventsManager(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Manager manager = new Manager();

			// Comprobamos que el usuario autentificado es manager
			this.eventService.checkIfManager();

			// Inicializamos el usuario
			manager = this.eventService.findManagerByPrincipal();

			// Visualizamos el listado de eventos
			manager.getEvents();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: Manage the events that
	 * he or she organises, which includes listing, registering, mod-ifying, and deleting them.
	 * In order to register a new event, he must have registered a valid credit card that must
	 * not expire in less than one day.
	 * 
	 * En este caso de uso se llevar� a cabo la edici�n de un evento. Para forzar el error puede ocurrir que:
	 * 
	 * � El usuario no est� autentificado
	 * � El usuario est� autentificado pero no como manager
	 * � El manager autentificado no es propietario del evento
	 * � La id del evento no existe
	 * � Los atributos no son ingresados correctamente
	 */
	public void editEventManager(final String username, final int eventId, final String title, final Date organisedMoment, final String description, final String picture, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos si es manager
			this.eventService.checkIfManager();

			// Localizamos el evento
			final Event event = this.eventService.findOne(eventId);

			// Editamos el evento y salvamos
			event.setDescription(description);
			event.setOrganisedMoment(organisedMoment);
			event.setPicture(picture);
			event.setTitle(title);

			this.eventService.checkAttributes(event);

			this.eventService.save(event);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: Manage the events that
	 * he or she organises, which includes listing, registering, mod-ifying, and deleting them.
	 * In order to register a new event, he must have registered a valid credit card that must
	 * not expire in less than one day.
	 * Every time he or she registers an event, the system will simulate that he or she's charged a 1.00euro fee.
	 * 
	 * En este caso de uso se llevar� a cabo la creaci�n de un evento. Para forzar el error puede ocurrir que:
	 * 
	 * � El usuario no est� autentificado
	 * � El usuario est� autentificado pero no como manager
	 * � Los atributos ingresados son err�neos
	 * � La tarjeta de cr�dito del manager autentificado no es v�lida
	 */
	public void createEventManager(final String username, final CreditCard creditCard, final String title, final Date organisedMoment, final String description, final String picture, final Integer seats, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos si es manager
			this.eventService.checkIfManager();

			// Cambiamos la credit card del manager para comprobar el funcionamiento del check
			// (el cu�l se realiza a la hora de salvar el evento)
			final Manager manager = this.eventService.findManagerByPrincipal();

			manager.setCreditCard(creditCard);
			this.managerService.saveForManager(manager);

			// Creamos un evento e inicializamos atributos y salvamos
			// Al crear un evento automaticamente se genera un fee
			final Event event = this.eventService.create();

			event.setDescription(description);
			event.setOrganisedMoment(organisedMoment);
			event.setPicture(picture);
			event.setTitle(title);
			event.setSeats(seats);

			this.eventService.checkAttributes(event);

			this.eventService.saveCreate(event);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * A user who is not authenticated must be able to: Browse the listing of events that
	 * are going to be organised in less than one month and have seats available.
	 * 
	 * En este caso de uso se llevar� a cabo la visualizaci�n del listado de eventos
	 * , por parte de un usuario no autentificado, que vayan a organizarse en menos de un mes
	 * y tengan asientos libres. Para forzar el error pueden darse varios escenarios:
	 * 
	 * � El usuario est� autentificado como admin o manager
	 */

	public void listEventsSpecial(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que la persona no est� autentificada o lo est� como chorbi

			if (username != null)
				this.chorbiService.checkIfChorbi();
			else
				Assert.isTrue(username == null);

			// Visualizamos el listado de eventos (especial)
			this.eventService.findSpecials();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticate as a chorbi must be able to: Browse the list of
	 * events to which he or she's registered.
	 * 
	 * En este caso de uso se llevar� a cabo la visualizaci�n del listado de eventos
	 * , por parte de un chorbi autenticado, estos eventos son aquellos en los cuales
	 * el chorbi est� registrado. Para forzar el error pueden darse varios escenarios:
	 * 
	 * � El usuario est� autentificado como admin o manager.
	 */

	public void listMyEvents(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que la persona no est� autentificada o lo est� como chorbi

			this.chorbiService.checkIfChorbi();

			// Visualizamos el listado de eventos (especial)
			final Chorbi c = this.chorbiService.findByPrincipal();
			c.getEvents();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------
	@Test
	public void driverListEvents() {

		final Object testingData[][] = {
			// Visualizar listado como admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			// Visualizar listado como manager -> false
			{
				"manager1", IllegalArgumentException.class
			},
			// Visualizar listado como chorbi -> true
			{
				"chorbi1", null
			},
			// Visualizar listado como no autentificado -> true
			{
				null, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listEvents((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverListEventsSpecial() {

		final Object testingData[][] = {
			// Visualizar listado especial como admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			// Visualizar listado especial como manager -> false
			{
				"manager1", IllegalArgumentException.class
			},
			// Visualizar listado especial como chorbi -> true
			{
				"chorbi1", null
			},
			// Visualizar listado especial como no autentificado -> true
			{
				null, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listEventsSpecial((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverListEventsManager() {

		final Object testingData[][] = {
			// Visualizar listado de eventos propios como admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			// Visualizar listado de eventos propios como manager -> true
			{
				"manager1", null
			},
			// Visualizar listado de eventos propios como chorbi -> false
			{
				"chorbi1", IllegalArgumentException.class
			},
			// Visualizar listado de eventos propios como no autentificado -> false
			{
				null, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listEventsManager((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverCreateEventManager() {

		final CreditCard badOne1 = new CreditCard();
		final CreditCard badOne2 = new CreditCard();
		final CreditCard goodOne = new CreditCard();
		final CreditCard goodOne1 = new CreditCard();

		badOne1.setBrandName("VISAa"); // Brand name inv�lido
		badOne1.setCvvCode(123);
		badOne1.setExpirationMonth(10);
		badOne1.setExpirationYear(2020);
		badOne1.setHolderName("Holder name");
		badOne1.setNumber("6382187214962573");

		badOne2.setBrandName("AMEX");
		badOne2.setCvvCode(123);
		badOne2.setExpirationMonth(10);
		badOne2.setExpirationYear(2020);
		badOne2.setHolderName("Holder name");
		badOne2.setNumber("3213387654563254657"); // N� inv�lido

		goodOne.setBrandName("MASTERCARD");
		goodOne.setCvvCode(123);
		goodOne.setExpirationMonth(10);
		goodOne.setExpirationYear(2020);
		goodOne.setHolderName("Holder name");
		goodOne.setNumber("6382187214962573");

		goodOne1.setBrandName("VISA");
		goodOne1.setCvvCode(123);
		goodOne1.setExpirationMonth(10);
		goodOne1.setExpirationYear(2020);
		goodOne1.setHolderName("Holder name");
		goodOne1.setNumber("6382187214962573");

		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 60);

		final Calendar cal1 = Calendar.getInstance();
		cal.add(Calendar.DATE, -30);

		final Date goodDate = cal.getTime();
		final Date badDate = cal1.getTime();

		final Object testingData[][] = {
			// Crear evento con usuario no autentificado -> false
			{
				null, goodOne, "title", goodDate, "description", "https://www.picture.com", 20, IllegalArgumentException.class
			},
			// Crear evento con admin -> false
			{
				"admin", goodOne, "title", goodDate, "description", "https://www.picture.com", 20, IllegalArgumentException.class
			},
			// Crear evento con chorbi -> false
			{
				"chorbi", goodOne, "title", goodDate, "description", "https://www.picture.com", 20, IllegalArgumentException.class
			},
			// Crear evento con creditCard inv�lida (1) -> false
			{
				"manager1", badOne1, "title", goodDate, "description", "https://www.picture.com", 20, IllegalArgumentException.class
			},
			// Crear evento con creditCard inv�lida (2) -> false
			{
				"manager1", badOne2, "title", goodDate, "description", "https://www.picture.com", 20, IllegalArgumentException.class
			},
			// Crear evento con fecha incorrecta -> false
			{
				"manager1", goodOne, "title", badDate, "description", "https://www.picture.com", 20, IllegalArgumentException.class
			},
			// Crear evento con algunos atributos incorrectos o vac�os (1) -> false
			{
				"manager1", goodOne, "", goodDate, "description", "https://www.picture.com", 20, IllegalArgumentException.class
			},
			// Crear evento con algunos atributos incorrectos o vac�os (2) -> false
			{
				"manager1", goodOne, "title", goodDate, "description", "not url", 20, IllegalArgumentException.class
			},
			// Crear evento con algunos atributos incorrectos o vac�os (2) -> false
			{
				"manager1", goodOne, "title", goodDate, "description", "https://www.picture.com", -20, IllegalArgumentException.class
			},
			// Crear evento con todo correcto (1) -> true
			{
				"manager1", goodOne, "title 222", goodDate, "description", "https://www.picture.com", 20, null
			},
			// Crear evento con todo correcto (2) -> true
			{
				"manager2", goodOne, "title 22211", goodDate, "description", "https://www.picture.com", 20, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.createEventManager((String) testingData[i][0], (CreditCard) testingData[i][1], (String) testingData[i][2], (Date) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Integer) testingData[i][6],
				(Class<?>) testingData[i][7]);
	}

	@Test
	public void driverEditEventManager() {

		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 60);

		final Calendar cal1 = Calendar.getInstance();
		cal.add(Calendar.DATE, -30);

		final Date goodDate = cal.getTime();
		final Date badDate = cal1.getTime();

		final Object testingData[][] = {
			// Editar evento con usuario no autentificado -> false
			{
				null, 138, "title", goodDate, "description", "https://www.picture.com", IllegalArgumentException.class
			},
			// Editar evento con admin -> false
			{
				"admin", 138, "title", goodDate, "description", "https://www.picture.com", IllegalArgumentException.class
			},
			// Editar evento con chorbi -> false
			{
				"chorbi1", 138, "title", goodDate, "description", "https://www.picture.com", IllegalArgumentException.class
			},
			// Editar evento con algunos fecha incorrecta -> false
			{
				"manager1", 138, "title", badDate, "description", "https://www.picture.com", IllegalArgumentException.class
			},
			// Editar evento con algunos atributos incorrectos o vac�os (1) -> false
			{
				"manager1", 138, "", goodDate, "description", "https://www.picture.com", IllegalArgumentException.class
			},
			// Editar evento con algunos atributos incorrectos o vac�os (2) -> false
			{
				"manager1", 138, "title", goodDate, "", "bad url", IllegalArgumentException.class
			},
			// Editar evento con id inexistente -> false
			{
				"manager1", 9999, "title", goodDate, "description", "https://www.picture.com", IllegalArgumentException.class
			},
			// Editar evento no siendo autor del mismo -> false
			{
				"manager2", 138, "title", goodDate, "description", "https://www.picture.com", IllegalArgumentException.class
			},
			// Editar evento con todo correcto -> true
			{
				"manager1", 138, "title", goodDate, "description", "https://www.picture.com", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.editEventManager((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (Date) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}
	@Test
	public void driverMyEvents() {

		final Object testingData[][] = {
			// Visualizar listado de eventos propios como admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			// Visualizar listado de eventos propios como chorbi -> true
			{
				"chorbi1", null
			},
			// Visualizar listado de eventos propios como manager -> false
			{
				"manager1", IllegalArgumentException.class
			},
			// Visualizar listado de eventos propios como no autentificado -> false
			{
				null, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listMyEvents((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
	@Test
	public void driverRegisterChorbiInEvent() {

		final Object testingData[][] = {
			// Registrarse en un evento como admin -> false
			{
				"admin", 138, IllegalArgumentException.class
			},
			// Visualizar listado de eventos propios como chorbi -> true
			{
				"chorbi1", 140, null
			},
			// Visualizar listado de eventos propios como no autentificado -> false
			{
				null, 138, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerChorbiInEvent((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverUnRegisterChorbiInEvent() {

		final Object testingData[][] = {
			// Registrarse en un evento como admin -> false
			{
				"admin", 138, IllegalArgumentException.class
			},
			// Visualizar listado de eventos propios como chorbi -> true
			{
				"chorbi1", 138, null
			},
			// Visualizar listado de eventos propios como no autentificado -> false
			{
				null, 138, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.unRegisterChorbiInEvent((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
