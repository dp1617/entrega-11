
package services;

import java.util.Calendar;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Chorbi;
import domain.CreditCard;
import forms.ChorbiForm;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ChorbiServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private ChorbiService	chorbiService;


	// Templates --------------------------------------------------------------

	/*
	 * "An actor who is authenticated as a chorbi must be able to:
	 * - Edit his or her profile."
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de editar un perfil.
	 * Para forzar el error pueden darse varios casos, como son:
	 * 
	 * � La persona autentificada es un administrador
	 * � La persona no est� autentificada
	 * � Alguno de los atributos no tiene el formato correcto o no es v�lido
	 * � La Id del chorbi no existe
	 */
	public void profileEdit(final String username, final String city, final String country, final String genre, final String kindOfRelationship, final String brandName, final Integer expirationMonth, final Integer expirationYear, final Integer cvvCode,
		final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos si la persona autentificada es chorbi
			this.chorbiService.checkIfChorbi();

			// Inicializamos los atributos para la edici�n
			Chorbi chorbi;
			CreditCard creditCard;

			chorbi = this.chorbiService.findByPrincipal();
			creditCard = chorbi.getCreditCard();

			chorbi.setCity(city);
			chorbi.setCountry(country);
			chorbi.setGenre(genre);
			chorbi.setKindOfRelationship(kindOfRelationship);

			creditCard.setBrandName(brandName);
			creditCard.setExpirationMonth(expirationMonth);
			creditCard.setExpirationYear(expirationYear);
			creditCard.setCvvCode(cvvCode);

			chorbi.setCreditCard(creditCard);

			// Comprobamos atributos
			this.chorbiService.checkAttributes(chorbi);

			// Guardamos
			this.chorbiService.save(chorbi);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Vista para ver todos los Chorbies
	 * 
	 * En este caso de uso se listan los chorbies desde su correspondiente vista
	 * Para provocar el error intentamos acceder
	 * al listado de chorbies sin autenticarnos.
	 */
	public void listChorbiTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.chorbiService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Vista para ver los likes que ha recibido el chorbie logueado
	 * 
	 * 
	 * 
	 * En este caso de uso se listan los chorbies desde su correspondiente vista
	 * Para provocar el error intentamos acceder
	 * al listado de chorbies sin autenticarnos.
	 */
	public void listLikesChorbiTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);
			final Chorbi chorbi = this.chorbiService.findByPrincipal();

			chorbi.getReceivedLikes();
			this.chorbiService.checkIfChorbi();
			Assert.isTrue(ActorService.validateCreditCard(chorbi.getCreditCard()), "<spring:message code='chorbi.genre'/>");

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Registro de chorbi
	 * 
	 * En este caso de uso se llevara a cabo el registro de un chorbi en el sistema
	 * por parte de un administrador. Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � CreditCard inv�lida
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerChorbi(final String username, final String newUsername, final String name, final String surname, final String email, final String phone, final String picture, final String description, final String kindOfRelationship,
		final Date birthDate, final String genre, final CreditCard creditCard, final String country, final String state, final String province, final String city, final String password, final String secondPassword, final Boolean checkBox,
		final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);

			// Inicializamos los atributos para la creaci�n
			final ChorbiForm chorbi = new ChorbiForm();

			final CreditCard creditCard1 = new CreditCard();

			chorbi.setEmail(email);
			chorbi.setName(name);
			chorbi.setSurname(surname);
			chorbi.setPhone(phone);
			chorbi.setBirthDate(birthDate);
			chorbi.setCity(city);
			chorbi.setCountry(country);
			chorbi.setDescription(description);
			chorbi.setGenre(genre);
			chorbi.setKindOfRelationship(kindOfRelationship);
			chorbi.setPicture(picture);
			chorbi.setProvince(province);
			chorbi.setState(state);

			chorbi.setUsername(newUsername);
			chorbi.setPassword(password);
			chorbi.setSecondPassword(secondPassword);

			creditCard1.setBrandName(creditCard.getBrandName());
			creditCard1.setExpirationMonth(creditCard.getExpirationMonth());
			creditCard1.setExpirationYear(creditCard.getExpirationYear());
			creditCard1.setCvvCode(creditCard.getCvvCode());

			chorbi.setCheckBox(checkBox);

			// Comprobamos atributos
			this.chorbiService.checkAttributes(chorbi);

			// Reestructuramos y asignamos la credit card
			final Chorbi chorbiR = this.chorbiService.reconstruct(chorbi);
			chorbiR.setCreditCard(creditCard);

			// Guardamos
			this.chorbiService.saveForm(chorbiR);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------
	@Test
	public void driverRegisterChorbi() {

		final CreditCard badOne1 = new CreditCard();
		final CreditCard goodOne = new CreditCard();

		badOne1.setBrandName("VISAa"); // Brand name inv�lido
		badOne1.setCvvCode(123);
		badOne1.setExpirationMonth(10);
		badOne1.setExpirationYear(2020);
		badOne1.setHolderName("Holder name");
		badOne1.setNumber("6382187214962573");

		goodOne.setBrandName("MASTERCARD");
		goodOne.setCvvCode(123);
		goodOne.setExpirationMonth(10);
		goodOne.setExpirationYear(2020);
		goodOne.setHolderName("Holder name");
		goodOne.setNumber("6382187214962573");

		final Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 1996);
		final Date goodDate = cal.getTime();

		final Calendar cal1 = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2001);
		final Date badDate = cal1.getTime();

		final Object testingData[][] = {
			// Creaci�n de chorbi como autentificado (1) -> false
			{
				"admin", "username", "name", "surname", "email@domain.com", "678432123", "https://www.picture.com", "description", "activities", goodDate, "man", goodOne, "a", "b", "c", "d", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de chorbi como autentificado (2) -> false
			{
				"chorbi1", "username1", "name", "surname", "email@domain.com", "678432123", "https://www.picture.com", "description", "activities", goodDate, "man", goodOne, "a", "b", "c", "d", "password1", "password1", true,
				IllegalArgumentException.class
			},
			// Creaci�n de chorbi como autentificado (3) -> false
			{
				"manager1", "username2", "name", "surname", "email@domain.com", "678432123", "https://www.picture.com", "description", "activities", goodDate, "man", goodOne, "a", "b", "c", "d", "password1", "password1", true,
				IllegalArgumentException.class
			},
			// Creaci�n de chorbi con phone incorrecto -> false
			{
				null, "userame5", "name", "surname", "email@domain.com", "31231232131233", "https://www.picture.com", "description", "activities", goodDate, "man", goodOne, "a", "b", "c", "d", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de chorbi con picture incorrecto -> false
			{
				null, "userame6", "name", "surname", "email@domain.com", "678432123", "notanurl", "description", "activities", goodDate, "man", goodOne, "a", "b", "c", "d", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de chorbi con genre incorrecto -> false
			{
				null, "userame7", "name", "surname", "email@domain.com", "678432123", "https://www.picture.com", "description", "notthis", goodDate, "man", goodOne, "a", "b", "c", "d", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de chorbi con kindOfRelationship incorrecto -> false
			{
				null, "userame8", "name", "surname", "email@domain.com", "678432123", "https://www.picture.com", "description", "activities", goodDate, "notnotthis", goodOne, "a", "", "", "d", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de chorbi con creditCard incorrecta -> false
			{
				null, "userame9", "name", "surname", "email@domain.com", "678432123", "https://www.picture.com", "description", "activities", goodDate, "man", badOne1, "a", "b", "c", "d", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de chorbi con edad incorrecta -> false
			{
				null, "userame10", "name", "surname", "email@domain.com", "678432123", "https://www.picture.com", "description", "activities", badDate, "man", goodOne, "a", "b", "c", "d", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de chorbi sin aceptar t�rminos -> false
			{
				null, "userame11", "name", "surname", "email@domain.com", "678432123", "https://www.picture.com", "description", "activities", goodDate, "man", goodOne, "a", "b", "c", "d", "password1", "password1", false, IllegalArgumentException.class
			},
			//	Creaci�n de chorbi con usuario no �nico -> false
			{
				null, "chorbi1", "name", "surname", "email@domain.com", "678432123", "https://www.picture.com", "description", "activities", goodDate, "man", goodOne, "a", "b", "c", "d", "password1", "password1", true, IllegalArgumentException.class
			},
			//Creaci�n de chorbi con contrase�as no coincidentes -> false
			{
				null, "userame12", "name", "surname", "email@domain.com", "678432123", "https://www.picture.com", "description", "activities", goodDate, "man", goodOne, "a", "b", "c", "d", "password1", "password1ewe", true, IllegalArgumentException.class
			},
			// Creaci�n de chorbi con todo correcto -> true
			{
				null, "userame13", "name", "surname", "email@domain.com", "678432123", "https://www.picture.com", "description", "activities", goodDate, "man", goodOne, "a", "b", "c", "d", "password1", "password1", true, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerChorbi((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (Date) testingData[i][9], (String) testingData[i][10], (CreditCard) testingData[i][11], (String) testingData[i][12], (String) testingData[i][13], (String) testingData[i][14], (String) testingData[i][15],
				(String) testingData[i][16], (String) testingData[i][17], (Boolean) testingData[i][18], (Class<?>) testingData[i][19]);
	}
	@Test
	public void driverProfileEdit() {

		final Object testingData[][] = {
			// Editar profile con administrador autentificado -> false
			{
				"admin", "city", "country", "man", "love", "VISA", 7, 2018, 854, IllegalArgumentException.class
			},
			// Editar profile sin autentificar -> false
			{
				null, "city", "country", "man", "love", "VISA", 7, 2018, 854, IllegalArgumentException.class
			},
			// Editar profile con brandName no v�lido -> false
			{
				"chorbi1", "city", "country", "man", "love", "VISAAAAEAA", 7, 2018, 854, IllegalArgumentException.class
			},
			// Editar profile con expirationYear no v�lido -> false
			{
				"chorbi1", "city", "country", "man", "love", "VISA", 7, 1992, 854, IllegalArgumentException.class
			},
			// Editar profile con cvv no v�lido -> false
			{
				"chorbi1", "city", "country", "man", "love", "VISA", 7, 2018, 4, IllegalArgumentException.class
			},
			// Editar profile de chorbi(1) y atributos correctos (todos) -> true
			{
				"chorbi1", "city", "country", "man", "love", "VISA", 7, 2018, 854, null
			},
			// Editar profile de chorbi(2) y atributos correctos (todos) -> true
			{
				"chorbi2", "city", "country", "woman", "love", "VISA", 7, 2018, 854, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.profileEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Integer) testingData[i][6], (Integer) testingData[i][7],
				(Integer) testingData[i][8], (Class<?>) testingData[i][9]);
	}

	@Test
	public void driverListChorbiTest() {
		final Object testingData[][] = {
			// Se accede con admin -> true
			{
				"admin", null
			},
			// Se accede con chorbi -> true
			{
				"chorbi1", null
			},
			//  Se accede con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listChorbiTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverListLikesChorbiTest() {
		final Object testingData[][] = {
			// Se accede con chorbi -> true
			{
				"chorbi1", null
			},
			// Se accede con chorbi -> true
			{
				"chorbi2", null
			},
			// Se accede con chorbi -> true
			{
				"chorbi3", null
			},
			// Se accede con chorbi -> true
			{
				"chorbi4", null
			},
			// Se accede con chorbi -> true
			{
				"chorbi5", null
			},
			// Se accede con chorbi -> true
			{
				"chorbi6", null
			},
			// Se accede con chorbi -> true
			{
				"chorbi7", null
			},
			// Se accede con admin -> false
			{
				"admin", IllegalArgumentException.class
			},

			//  Se accede con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			},

			//  El usuario logueado no tiene una credit card valida -> false
			{
				"chorbi8", IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listLikesChorbiTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
