
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Chorbi;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class AdministratorServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private AdministratorService	administratorService;

	@Autowired
	private ChorbiService			chorbiService;


	// Templates --------------------------------------------------------------

	/*
	 * "An actor who is authenticated as an administrator must be able to:
	 * - Ban a chorbi, that is, to disable his or her account."
	 * 
	 * En este casao de uso se llevara a cabo el baneo de un chorbi.
	 * Para forzar el error pueden darse casos como:
	 * 
	 * � Un usuario no administrador banee a otro usuario
	 */

	public void banChorbi(final String username, final int chorbiId, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);
			Chorbi c;
			c = this.chorbiService.findOne(chorbiId);
			this.administratorService.ban(c);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * "An actor who is authenticated as an administrator must be able to:
	 * - Unban a chorbi, that is, to disable his or her account."
	 * 
	 * En este casao de uso se llevara a cabo el baneo de un chorbi.
	 * Para forzar el error pueden darse casos como:
	 * 
	 * � Un usuario no administrador desbanee a otro usuario
	 */

	public void unbanChorbi(final String username, final int chorbiId, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);
			Chorbi c;
			c = this.chorbiService.findOne(chorbiId);
			this.administratorService.unban(c);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * "An actor who is authenticated as an administrator must be able to:
	 * - Ban a chorbi, that is, to disable his or her account."
	 * 
	 * En este casao de uso se llevara a cabo el baneo de un chorbi.
	 * Para forzar el error pueden darse casos como:
	 * 
	 * � Un usuario baneado se loguee
	 */

	public void accessBanned(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverBanChorbi() {
		final Object testingData[][] = {
			// Banear un chorbi autentificado como admin -> true
			{
				"admin", 95, null
			},
			// Banear un chorbi autentificado como chorbi -> false
			{
				"chorbi2", 95, IllegalArgumentException.class
			},
			// Banear un chorbi sin autentificarse -> false
			{
				null, 95, IllegalArgumentException.class
			},
			// Banear un chorbi que no existe -> false
			{
				"admin", 9999, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.banChorbi((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverUnbanChorbi() {
		final Object testingData[][] = {
			// Desbanear un chorbi autentificado como admin -> true
			{
				"admin", 111, null
			},
			// Desbanear un chorbi autentificado como chorbi -> false
			{
				"chorbi2", 111, IllegalArgumentException.class
			},
			// Desbanear un chorbi sin autentificarse -> false
			{
				null, 111, IllegalArgumentException.class
			},
			// Desbanear un chorbi que no existe -> false
			{
				"admin", 9999, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.unbanChorbi((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverAccesBanned() {
		final Object testingData[][] = {
			// Acceder como chorbi no baneado -> true
			{
				"chorbi1", null
			},
			// Acceder como chorbi baneado -> false
			{
				"chorbi9", IllegalArgumentException.class
			},
		};

		for (int i = 0; i < testingData.length; i++)
			this.accessBanned((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

}
