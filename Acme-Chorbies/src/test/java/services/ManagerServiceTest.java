
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.CreditCard;
import domain.Manager;
import forms.ManagerForm;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ManagerServiceTest extends AbstractTest {

	// Services and repositories

	//@Autowired
	//private ActorService	actorService;
	@Autowired
	private ManagerService			managerService;
	@Autowired
	private AdministratorService	administratorService;


	// Templates --------------------------------------------------------------

	/*
	 * Registro de un manager por parte de admin.
	 * 
	 * En este caso de uso se llevara a cabo el registro de un manager en el sistema
	 * por parte de un administrador. Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario no esta autentificado
	 * � El usuario no esta autentificado como admin
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � CreditCard inv�lida
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerManager(final String actualUsername, final String managerUsername, final String name, final String surname, final String email, final String phone, final String company, final String VATNumber, final CreditCard creditCard,
		final String password, final String secondPassword, final Boolean checkBox, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(actualUsername);

			// Comprobamos si la persona autentificada es admin
			this.administratorService.checkIfAdministrator();

			// Inicializamos los atributos para la creaci�n
			final ManagerForm manager = new ManagerForm();

			final CreditCard creditCard1 = new CreditCard();

			manager.setCompany(company);
			manager.setEmail(email);
			manager.setName(name);
			manager.setSurname(surname);
			manager.setPhone(phone);
			manager.setVATNumber(VATNumber);

			manager.setUsername(managerUsername);
			manager.setPassword(password);
			manager.setSecondPassword(secondPassword);

			creditCard1.setBrandName(creditCard.getBrandName());
			creditCard1.setExpirationMonth(creditCard.getExpirationMonth());
			creditCard1.setExpirationYear(creditCard.getExpirationYear());
			creditCard1.setCvvCode(creditCard.getCvvCode());

			manager.setCheckBox(checkBox);

			// Comprobamos atributos
			this.managerService.checkAttributes(manager);

			// Reestructuramos y asignamos la credit card
			final Manager managerR = this.managerService.reconstruct(manager);
			managerR.setCreditCard(creditCard);

			// Guardamos
			this.managerService.saveForm(managerR);

			this.unauthenticate();

			// Ingresamos con el nuevo usuario
			this.authenticate(managerUsername);
			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------
	@Test
	public void driverRegisterManager() {

		final CreditCard badOne1 = new CreditCard();
		final CreditCard badOne2 = new CreditCard();
		final CreditCard goodOne = new CreditCard();
		final CreditCard goodOne1 = new CreditCard();

		badOne1.setBrandName("VISAa"); // Brand name inv�lido
		badOne1.setCvvCode(123);
		badOne1.setExpirationMonth(10);
		badOne1.setExpirationYear(2020);
		badOne1.setHolderName("Holder name");
		badOne1.setNumber("6382187214962573");

		badOne2.setBrandName("AMEX");
		badOne2.setCvvCode(123);
		badOne2.setExpirationMonth(10);
		badOne2.setExpirationYear(2020);
		badOne2.setHolderName("Holder name");
		badOne2.setNumber("3213387654563254657"); // N� inv�lido

		goodOne.setBrandName("MASTERCARD");
		goodOne.setCvvCode(123);
		goodOne.setExpirationMonth(10);
		goodOne.setExpirationYear(2020);
		goodOne.setHolderName("Holder name");
		goodOne.setNumber("6382187214962573");

		goodOne1.setBrandName("VISA");
		goodOne1.setCvvCode(123);
		goodOne1.setExpirationMonth(10);
		goodOne1.setExpirationYear(2020);
		goodOne1.setHolderName("Holder name");
		goodOne1.setNumber("6382187214962573");

		final Object testingData[][] = {
			// Creaci�n de manager con usuario no autentificado -> false
			{
				null, "newManager", "name", "surname", "email@domain.com", "678432123", "company", "VATNumber", goodOne, "password", "password", true, IllegalArgumentException.class
			},
			// Creaci�n de manager como chorbi -> false
			{
				"chorbi1", "newManager", "name", "surname", "email@domain.com", "678432123", "company", "VATNumber", goodOne, "password", "password", true, IllegalArgumentException.class
			},
			// Creaci�n de manager como manager -> false
			{
				"manager1", "newManager", "name", "surname", "email@domain.com", "678432123", "company", "VATNumber", goodOne, "password", "password", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con nombre de usuario ya existente -> false
			{
				"admin", "manager1", "name", "surname", "email@domain.com", "678432123", "company", "VATNumber", goodOne, "password", "password", true, IllegalArgumentException.class
			},
			// Creaci�n de manager sin aceptar las condiciones -> false
			{
				"admin", "newManager", "name", "surname", "email@domain.com", "678432123", "company", "VATNumber", goodOne, "password", "password", false, IllegalArgumentException.class
			},
			// Creaci�n de manager con credit card inv�lida (n�mero) -> false
			{
				"admin", "newManager", "name", "surname", "email@domain.com", "678432123321876321", "company", "VATNumber", badOne1, "password", "password", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con credit card inv�lida (brandName) -> false
			{
				"admin", "newManager", "name", "surname", "email@domain.com", "678432123", "company", "VATNumber", badOne2, "password", "password", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con las contrase�as sin coincidir -> false
			{
				"admin", "newManager", "name", "surname", "email@domain.com", "678432123", "company", "VATNumber", goodOne, "password", "passworddd", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con phone incorrecto -> false
			{
				"admin", "newManager", "", "surname", "email@domain.com", "+32131 678432123", "", "VATNumber", goodOne, "password", "password", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con email incorrecto -> false
			{
				"admin", "newManager", "name", "surname", "this is not an email", "678432123", "company", "VATNumber", goodOne, "password", "password", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con todo correcto -> true
			{
				"admin", "newManager", "name", "surname", "email@domain.com", "678432123", "company", "VATNumber", goodOne, "password", "password", true, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerManager((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (CreditCard) testingData[i][8], (String) testingData[i][9], (String) testingData[i][10], (Boolean) testingData[i][11], (Class<?>) testingData[i][12]);
	}

	/*
	 * "An actor who is authenticated as a manager must be able to:
	 * - Edit his or her profile."
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de editar un perfil.
	 * Para forzar el error pueden darse varios casos, como son:
	 * 
	 * � La persona autentificada es un administrador
	 * � La persona no est� autentificada
	 * � La persona autenticada es un chorbi
	 * � Alguno de los atributos no tiene el formato correcto o no es v�lido
	 */
	public void profileEditManager(final String username, final String name, final String surname, final String email, final String phone, final String company, final String VATNumber, final String holderName, final String brandName, final String number,
		final Integer expirationMonth, final Integer expirationYear, final Integer cvvCode, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos si la persona autentificada es manager
			this.managerService.checkIfManager();

			// Inicializamos los atributos para la edici�n
			Manager manager;
			CreditCard creditCard = new CreditCard();

			manager = this.managerService.findByPrincipal();
			creditCard = manager.getCreditCard();

			manager.setName(name);
			manager.setSurname(surname);
			manager.setEmail(email);
			manager.setPhone(phone);
			manager.setCompany(company);
			manager.setVATNumber(VATNumber);

			creditCard.setBrandName(brandName);
			creditCard.setHolderName(holderName);
			creditCard.setNumber(number);
			creditCard.setExpirationMonth(expirationMonth);
			creditCard.setExpirationYear(expirationYear);
			creditCard.setCvvCode(cvvCode);

			manager.setCreditCard(creditCard);

			// Comprobamos atributos

			// Guardamos
			this.managerService.saveForManager(manager);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverProfileEditManager() {

		final Object testingData[][] = {

			// Editar profile con administrador autentificado -> false
			{
				"admin", "name", "surname", "email@gmail.com", "635262365", "company", "VATNumber", "HolderNameCreditCard1", "VISA", "6382187214962573", 7, 2018, 854, IllegalArgumentException.class
			},
			// Editar profile sin autentificar -> false
			{
				null, "name", "surname", "email@gmail.com", "635262365", "company", "VATNumber", "HolderNameCreditCard1", "VISA", "6382187214962573", 7, 2018, 854, IllegalArgumentException.class
			},
			// Editar profile con chorbi autenticado -> false
			{
				"chorbi1", "name", "surname", "email@gmail.com", "635262365", "company", "VATNumber", "HolderNameCreditCard1", "VISA", "6382187214962573", 7, 2018, 854, IllegalArgumentException.class
			},
			// Editar profile con brandName no v�lido -> false
			{
				"manager1", "name", "surname", "email@gmail.com", "635262365", "company", "VATNumber", "HolderNameCreditCard1", "VISAAAAEAA", "6382187214962573", 7, 2018, 854, IllegalArgumentException.class
			},
			// Editar profile con expirationYear no v�lido -> false
			{
				"manager1", "name", "surname", "email@gmail.com", "635262365", "company", "VATNumber", "HolderNameCreditCard1", "VISA", "6382187214962573", 7, 1992, 854, IllegalArgumentException.class
			},
			// Editar profile con cvv no v�lido -> false
			{
				"manager1", "name", "surname", "email@gmail.com", "635262365", "company", "VATNumber", "HolderNameCreditCard1", "VISA", "6382187214962573", 7, 2018, 4, IllegalArgumentException.class
			},
			// Editar profile de manager(1) y atributos correctos (todos) -> true
			{
				"manager1", "name", "surname", "email@gmail.com", "635262365", "company", "VATNumber", "HolderNameCreditCard1", "VISA", "4013200209774812", 02, 2018, 365, null
			},
			// Editar profile de manager(1) y atributos correctos (todos) -> true
			{
				"manager1", "name", "surname", "email@gmail.com", "635262365", "company", "VATNumber", "HolderNameCreditCard1", "VISA", "4013200209774812", 02, 2018, 365, null
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.profileEditManager((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (String) testingData[i][9], (Integer) testingData[i][10], (Integer) testingData[i][11], (Integer) testingData[i][12], (Class<?>) testingData[i][13]);
	}

}
