
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Chorbi;
import domain.Fee;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class FeeServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private FeeService				feeService;
	@Autowired
	private ChorbiService			chorbiService;
	@Autowired
	private AdministratorService	adminService;
	@Autowired
	private TaxService				taxService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is authenticated as an administrator must be able to:
	 * Run a process to update the total monthly fees that the chorbies would have to pay.
	 * Recall that chorbies must not be aware of the simulation.
	 * 
	 * En este caso se llevar� a cabo el registro de los fees para chorbies, para lanzar el error se probar� que:
	 * 
	 * � El usuario no est� autentificado
	 * � El usuario est� autentificado pero no como administrador
	 * � El usuario est� autentificado como administrador pero no lo registra a un chorbi existente
	 * 
	 * Se considera �xito si se registra un fee a un chorbi, estando autentificado como administrador.
	 */
	public void updateMonthlyChorbiFees(final String username, final int chorbiId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos si es administrador
			this.adminService.checkIfAdministrator();

			//Creamos el fee
			Chorbi res;
			res = this.chorbiService.findOne(chorbiId);
			Assert.isTrue(res != null);

			final Fee fee = this.feeService.createChorbi(res);
			fee.setEvent(null);
			fee.setIsPaid(false);
			fee.setManager(null);

			//Aplicamos el valor de los fee para chorbis
			fee.setValue(this.taxService.findChorbiFeeValue());

			//Guardamos el fee
			this.feeService.save(fee);

			//Deslogeamos al administrador
			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverUpdateMonthlyChorbiFees() {

		final Object testingData[][] = {
			// Registrar el fee correctamente -> true
			{
				"admin", 95, null
			},
			// Registrar el fee a un chorbi inexistente -> false
			{
				"admin", 9999990, IllegalArgumentException.class
			},
			// Registrar el fee sin ser administrador -> false
			{
				"manager1", 97, IllegalArgumentException.class
			},
			// Registrar el fee sin estar logeado -> false
			{
				null, 97, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.updateMonthlyChorbiFees((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
