
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Chorbi;
import domain.Likes;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class LikesServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private LikesService	likesService;

	@Autowired
	private ChorbiService	chorbiService;


	// Templates --------------------------------------------------------------

	
	/*
	 * "An actor who is authenticated must be able to:
	 * - Browse the list of chorbies who have registered to the system and navigate to the chorbies who like them."
	 * 
	 * En este casao de uso se llevara a cabo el listado de los chorbies que le han dado "Me gusta" a otro chorbi.
	 * Para forzar el error pueden darse dos casos, como son:
	 * 
	 * � La persona no est� autentificada
	 * � La Id del chorbi no existe
	 */
	
	public void likesList(final String username, final int chorbiId, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);
			this.likesService.findLikingChorbies(chorbiId);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Actualizado en la version Acme-Chorbies 2.0
	 * 
	 * "An actor who is authenticated as a chorbi must be able to:
	 * - Like another chorbi; a like may be cancelled at any time."
	 * - When a chorbi likes another chorbi, he or she should be able to indicate a number of stars in range zero  three
	 * 
	 * En este caso de uso se llevara a cabo la acci�n de dar "Me gusta" a otro chorbie indicando el numero de estrellas.
	 * Para forzar el error pueden darse varios casos, como son:
	 * 
	 * � La persona est� autentificada como un administrador
	 * � La persona no esta autentificada
	 * � La Id del chorbi no existe
	 * � El chorbi ya tiene un Like de esa persona
	 * � El numero de estrellas es mayor de 3
	 * 
	 */
	
	public void likesChorbi(final String username, final int likedChorbiId, final String comment, final int numberStars , final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			final Chorbi likedChorbi = this.chorbiService.findOne(likedChorbiId);

			final Likes l = this.likesService.create(likedChorbi);

			l.setComment(comment);
			l.setNumberStars(numberStars);

			this.likesService.sendLike(l);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	
	
	/*
	 * "An actor who is authenticated as a chorbi must be able to:
	 * - Like another chorbi; a like may be cancelled at any time."
	 * 
	 * En este casao de uso se llevara a cabo la acci�n de cancelar un "Me gusta" que se ha dado a otro chorbie.
	 * Para forzar el error pueden darse varios casos, como son:
	 * 
	 * � La persona est� autentificada como un administrador
	 * � La persona no esta autentificada
	 * � La Id del chorbi no existe
	 * � El chorbi ya tiene un Like de esa persona
	 * 
	 */
	
	public void cancelLikesChorbi(final String username, final int chorbiId, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			Chorbi me;
			Chorbi cancelChorbi;

			Likes like = null;

			me = this.chorbiService.findByPrincipal();
			cancelChorbi = this.chorbiService.findOne(chorbiId);

			for (final Likes l : me.getSentLikes())
				if (l.getLikedChorbi().getId() == cancelChorbi.getId())
					like = l;

			this.likesService.checkLike2(like);
			this.likesService.delete(like);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverLikesList() {
		final Object testingData[][] = {
			// Lista de likes autentificado como admin -> true
			{
				"admin", 97, null
			},
			// Lista de likes autentificado como chorbi -> true
			{
				"chorbi2", 99, null
			},
			// Lista de likes sin autentificarse -> false
			{
				null, 99, IllegalArgumentException.class
			},
			// Lista de likes con un Id de un chorbi que no existe -> false
			{
				"chorbi2", 9999, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.likesList((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverLikesChorbi() {
		final Object testingData[][] = {
			// Likes autentificado como chorbi -> true
			{
				"chorbi2", 95, "Esto es una prueba" ,3, null
			},
			// Likes autentificado como admin -> false
			{
				"admin", 95, "Esto es una prueba", 2, IllegalArgumentException.class
			},
			// Likes sin autentificarse -> false
			{
				null, 95, "Esto es una prueba",1, IllegalArgumentException.class
			},
			// Likes con un Id de un chorbi que no existe -> false
			{
				"chorbi2", 989898, "Esto es una prueba", 2, IllegalArgumentException.class
			},
			// Likes a un chorbi que ya tiene un like de ese chorbi -> false
			{
				"chorbi2", 101, "Esto es una prueba", 2, IllegalArgumentException.class
			},
			// Likes con un numero de estrellas mayor que 3 -> true
			{
				"chorbi2", 95, "Esto es una prueba" ,4, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.likesChorbi((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (int) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void driverCancelLikesChorbi() {
		final Object testingData[][] = {
			// Cancela un Like autentificado como chorbi -> true
			{
				"chorbi2", 101, null
			},
			// Cancela un Like autentificado como admin -> false
			{
				"admin", 72, IllegalArgumentException.class
			},
			// Cancela un Like sin autentificarse -> false
			{
				null, 72, IllegalArgumentException.class
			},
			// Cancela un Like con un Id de un chorbi que no existe -> false
			{
				"chorbi2", 9999, IllegalArgumentException.class
			},
			// Cancela un Like que no existe -> false
			{
				"chorbi2", 95, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.cancelLikesChorbi((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
