
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Chirp;
import domain.Chorbi;
import domain.Event;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ChirpServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private ChirpService	chirpService;
	@Autowired
	private ChorbiService	chorbiService;
	//@Autowired
	//private ManagerService	managerService;
	@Autowired
	private EventService	eventService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is authenticated as a chorbi must be able to:
	 * 
	 * - Chirp to another chorbi.
	 * 
	 * En este caso de uso se crear� un chirp. Se forzar� el error cuando se intente
	 * realizar con un usuario no autentificado, con el t�tulo o el cuerpo del mensaje vac�os,
	 * con unos attachments que no coincidan con el pattern o cuando el receptor del mensaje no exista.
	 */

	public void createChirpTest(final String username, final String recipient, final String subject, final String text, final Collection<String> attachments, final Class<?> expected) {

		Class<?> caught = null;

		try {

			final Chorbi c = this.chorbiService.findChorbiByUsername(recipient);

			this.authenticate(username);

			final Chirp chirp = new Chirp();

			chirp.setSender(this.chorbiService.findByPrincipal());
			chirp.setRecipient(c);
			chirp.setSubject(subject);
			chirp.setText(text);
			chirp.setAttachments(attachments);

			this.chirpService.save(chirp);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Browse the list of chirps that he or she's got, and reply to any of them.
	 * Browse the list of chirps that he or she's sent, and re-send any of them.
	 * Erase any of the chirps that he or she's got or sent, which requires previous confirmation.
	 * 
	 * En estos casos de uso se listaran los chirps enviados y recibidos por el chorbi.
	 * Para forzar el error se acceder� con un usuario no autentificado.
	 */

	public void listChirpsTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Collection<Chirp> receivedChirps = new ArrayList<>();
			Collection<Chirp> sentChirps = new ArrayList<>();
			final Collection<Chirp> allChirps = new ArrayList<>();

			Chorbi chorbi;
			chorbi = this.chorbiService.findByPrincipal();

			receivedChirps = chorbi.getReceivedChirps();
			sentChirps = chorbi.getSentChirps();

			allChirps.addAll(receivedChirps);
			allChirps.addAll(sentChirps);

			Assert.notEmpty(allChirps);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Reply a chirp.
	 * 
	 * En este caso de uso se responder� a uno de los chirps recibidos por el chorbi.
	 * Para forzar el error se acceder� con un usuario no autentificado,
	 * se responder� a un chirp que no pertenece al usuario y se responder� a un mensaje que no existe.
	 */

	public void replyChirpTest(final String username, final int chirpId, final String subject, final String text, final Collection<String> attachments, final Class<?> expected) {

		Class<?> caught = null;

		try {

			Chirp m;

			this.authenticate(username);

			m = this.chirpService.reply(chirpId);
			m.setSubject(subject);
			m.setText(text);
			m.setAttachments(attachments);

			this.chirpService.save(m);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Forward a chirp.
	 * 
	 * En este caso de uso se reenviar� uno de los chirps recibidos por el chorbi.
	 * Para forzar el error se acceder� con un usuario no autentificado,
	 * se reenviar� un mensaje que no pertenece al usuario, se responder� a un chirp
	 * que no existe o se eligir� un destinatario no v�lido.
	 */

	public void forwardChirpTest(final String username, final String recipient, final int chirpId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			Chirp m;
			final Chorbi rec = this.chorbiService.findChorbiByUsername(recipient);

			this.authenticate(username);

			m = this.chirpService.forward(chirpId);
			m.setRecipient(rec);

			this.chirpService.save(m);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Erase any of the chirps that he or she's got or sent, which requires previous confirmation.
	 * 
	 * En este caso de uso se borrar� un chirp que posea el chorbi en alguna de sus bandejas.
	 * Se forzar� el error cuando el usuario no est� autentificado,
	 * si el mensaje no le pertenece o si la ID del mensaje no es v�lida.
	 */

	public void deleteChirpTest(final String username, final int chirpId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			final Chirp m = this.chirpService.findOne(chirpId);
			final Chorbi chorbi = this.chorbiService.findByPrincipal();

			String aux = null;

			if (chorbi.equals(m.getSender())) {
				this.chirpService.deleteSent(m);
				aux = "caso1";
			} else if (chorbi.equals(m.getRecipient())) {
				this.chirpService.deleteReceived(m);
				aux = "caso2";
			} else
				Assert.notNull(aux);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	/*
	 * Broadcast a chirp.
	 * 
	 * En este caso de uso se emitir� uno de los chirps creados por el manager.
	 * Para forzar el error se emitir� con un usuario no autentificado,
	 * se emitir� un mensaje que sin ser manager, se emitir� un mensaje a
	 * los chorbies de un evento que no existe, o no pertenece al manager
	 * y se emitir� un mensaje no v�lido.
	 */

	public void broadcastChirpTest(final String username, final int eventId, final String subject, final String text, final Collection<String> attachments, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			final Event event = this.eventService.findOne(eventId);

			final Chirp chirp = new Chirp();

			chirp.setSubject(subject);
			chirp.setText(text);
			chirp.setAttachments(attachments);

			this.chirpService.broadcast(chirp, event);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	// Drivers ---------------------------------------------------------------

	@SuppressWarnings("unchecked")
	@Test
	public void driverCreateChirp() {

		final Collection<String> attachementsValid = new ArrayList<>();
		attachementsValid.add("https://www.PruebaDeURLAdjuntaValida.com");

		final Collection<String> attachementsNotValid = new ArrayList<>();
		attachementsNotValid.add("adjuntoNoValido");

		final Object testingData[][] = {

			// Crear chirp con chorbi y atributos validos -> true
			{
				"chorbi1", "chorbi2", "Titulo test", "Text test", attachementsValid, null
			},
			// Crear chirp con text nulo -> false
			{
				"chorbi1", "chorbi2", "Titulo test", null, attachementsValid, IllegalArgumentException.class
			},
			// Crear chirp con subject nulo -> false
			{
				"chorbi1", "chorbi2", null, "Text test", attachementsValid, IllegalArgumentException.class
			},
			// Crear chirp con adjuntos no validos -> false
			{
				"chorbi1", "chorbi2", "Titulo test", "Text test", attachementsNotValid, IllegalArgumentException.class
			},
			// Crear chirp apuntando hacia un chorbi que no existe -> false
			{
				"chorbi1", "chxrbi16694687", "Titulo test", "Text test", attachementsValid, IllegalArgumentException.class
			},
			// Crear chirp con usuario sin autentificar en el sistema -> false
			{
				null, "chorbi2", "Titulo test", "Text test", attachementsValid, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.createChirpTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Collection<String>) testingData[i][4], (Class<?>) testingData[i][5]);

	}
	@Test
	public void driverListChirps() {
		final Object testingData[][] = {
			// Listado de chirps de un chorbi -> true
			{
				"chorbi1", null
			},
			// Listado de chirps de alguien no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.listChirpsTest((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void driverReplyChirp() {
		final Collection<String> attachementsValid = new ArrayList<>();
		attachementsValid.add("https://www.attachmenttest.com");
		final Object testingData[][] = {
			// Chirp y chorbi v�lidos -> true
			{
				"chorbi1", 119, "Title test", "Text test", attachementsValid, null
			},
			// Chorbi no autentificado y chirp v�lido -> false
			{
				null, 119, "Title test", "Text test", attachementsValid, IllegalArgumentException.class
			},
			// Reply a un chirp que no pertenece a ese chorbi -> false
			{
				"chorbi1", 120, "Title test", "Text test", attachementsValid, IllegalArgumentException.class
			},
			// Reply a un chirp que no existe -> false
			{
				"chorbi1", 1234, "Title test", "Text test", attachementsValid, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.replyChirpTest((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Collection<String>) testingData[i][4], (Class<?>) testingData[i][5]);

	}
	@Test
	public void driverForwardChirp() {
		final Object testingData[][] = {
			// Chorbis y chirp v�lidos -> true
			{
				"chorbi1", "chorbi2", 117, null
			},
			// Chorbi no autentificado y chirp v�lido -> false
			{
				null, "chorbi2", 117, IllegalArgumentException.class
			},
			// Forward chirp que no pertenece al chorbi -> false
			{
				"chorbi1", "chorbi2", 120, IllegalArgumentException.class
			},
			// Forward chirp no existente -> false
			{
				"chorbi1", "chorbi2", 1426, IllegalArgumentException.class
			},
			// Forward a un chorbi destinatario no valido -> false
			{
				"chorbi1", "chorbi21555", 117, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.forwardChirpTest((String) testingData[i][0], (String) testingData[i][1], (int) testingData[i][2], (Class<?>) testingData[i][3]);

	}

	@Test
	public void driverDeleteChirp() {
		final Object testingData[][] = {
			// Borrar chirp que le pertenece -> true
			{
				"chorbi1", 117, null
			},
			// Borrar chirp como chorbi no autentificado -> false
			{
				null, 117, IllegalArgumentException.class
			},
			// Borrar chirp que no le pertenece -> false
			{
				"chorbi1", 120, IllegalArgumentException.class
			},
			// Borrar chirp con ID no v�lida -> false
			{
				"chorbi1", 4321, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.deleteChirpTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void driverBroadcastChirp() {

		final Collection<String> attachementsValid = new ArrayList<>();
		attachementsValid.add("https://www.PruebaDeURLAdjuntaValida.com");

		final Collection<String> attachementsNotValid = new ArrayList<>();
		attachementsNotValid.add("adjuntoNoValido");

		final Object testingData[][] = {

			// Crear chirp con manager y atributos validos -> true
			{
				"manager1", 138, "Titulo test", "Text test", attachementsValid, null
			},
			// Crear chirp con text nulo -> false
			{
				"manager1", 138, "Titulo test", null, attachementsValid, IllegalArgumentException.class
			},
			// Crear chirp con subject nulo -> false
			{
				"manager1", 138, null, "Text test", attachementsValid, IllegalArgumentException.class
			},
			// Crear chirp con adjuntos no validos -> false
			{
				"manager1", 138, "Titulo test", "Text test", attachementsNotValid, IllegalArgumentException.class
			},
			// Crear chirp apuntando hacia un evento que no existe -> false
			{
				"manager1", 115, "Titulo test", "Text test", attachementsValid, IllegalArgumentException.class
			},
			// Crear chirp para un evento que no pertenece al manager -> false
			{
				"manager1", 140, "Titulo test", "Text test", attachementsValid, IllegalArgumentException.class
			},
			// Crear chirp con usuario sin autentificar en el sistema -> false
			{
				null, 120, "Titulo test", "Text test", attachementsValid, IllegalArgumentException.class
			},
			// Crear chirp con usuario que no es manager -> false
			{
				"chorbi1", 120, "Titulo test", "Text test", attachementsValid, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.broadcastChirpTest((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Collection<String>) testingData[i][4], (Class<?>) testingData[i][5]);

	}
}
