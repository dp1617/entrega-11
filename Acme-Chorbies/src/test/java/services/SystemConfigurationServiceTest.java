
package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.SystemConfiguration;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class SystemConfigurationServiceTest extends AbstractTest {

	@Autowired
	private SystemConfigurationService	systemConfigurationService;


	// Templates --------------------------------------------------------------
	/*
	 * See a welcome page with a banner that advertises Acme projects, including Acme
	 * Pad-Thai, Acme BnB, and Acme Carn go! The banners must be selected randomly.
	 * 
	 * 
	 * En este caso se mostrar� el systemConfiguration que hay creado en el sistema. Es accesible para cualquier usuario, tanto autenticado
	 * como no. El error se obtendr� cuando la url no sea v�lida.
	 */

	public void watchSystemConfigurationTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.systemConfigurationService.getRamdomBanner();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	public void editSystemConfigurationTest(final String username, final Collection<String> banners, final Date cacheTime, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			final SystemConfiguration systemConfiguration = this.systemConfigurationService.getSystemConfiguration();

			this.systemConfigurationService.checkBanners(banners);
			systemConfiguration.setBanners(banners);
			systemConfiguration.setCacheTime(cacheTime);
			this.systemConfigurationService.save(systemConfiguration);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverWatchSystemConfigurationTest() {
		final Object testingData[][] = {
			//watch systemConfiguration sin autenticarse -> true
			{
				null, null
			},
			// Watch systemConfiguration con chorbi -> true
			{
				"chorbi1", null
			},
			// Watch systemConfiguration con admin -> true
			{
				"admin", null
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.watchSystemConfigurationTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
	@SuppressWarnings("unchecked")
	@Test
	public void driverEditSystemConfigurationTest() {
		final Collection<String> bannersValid = new ArrayList<>();
		bannersValid.add("https://www.bannerstest.com");
		final Collection<String> bannersNotValid = new ArrayList<>();
		bannersNotValid.add("notvalidbanners");

		final Date now = new Date();
		final Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		cal.add(Calendar.HOUR, -2);
		cal.add(Calendar.MINUTE, -10);
		cal.add(Calendar.SECOND, -7);
		final Date goodCache = cal.getTime();

		final Object testingData[][] = {
			//edit SystemConfiguration con url correcta y siendo admin-> true
			{
				"admin", bannersValid, goodCache, null
			},
			// Edit SystemConfiguration sin autenticarse -> false
			{
				null, bannersValid, goodCache, IllegalArgumentException.class
			},
			// Edit a SystemConfiguration siendo chorbi-> false
			{
				"chorbi1", bannersValid, goodCache, IllegalArgumentException.class
			},
			//Edit SystemConfiguration con banners incorrectos -> false
			{
				"admin", bannersNotValid, goodCache, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.editSystemConfigurationTest((String) testingData[i][0], (Collection<String>) testingData[i][1], (Date) testingData[i][2], (Class<?>) testingData[i][3]);

	}
}
