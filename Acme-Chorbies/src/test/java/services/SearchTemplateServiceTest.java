
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Chorbi;
import domain.CreditCard;
import domain.SearchTemplate;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class SearchTemplateServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private SearchTemplateService	searchTemplateService;


	// Templates --------------------------------------------------------------

	/*
	 * "An actor who is authenticated as a chorbi must be able to:
	 * - Change his or her search template."
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de editar una searchTemplate.
	 * Para forzar el error pueden darse varios casos, como son:
	 * 
	 * � La persona autentificada es un administrador o un chorbi no propietario del searchTemplate
	 * � La persona no est� autentificada
	 * � Alguno de los atributos no tiene el formato correcto o no es v�lido
	 * � La Id de la searchTemplate no existe
	 */
	public void searchTemplateEdit(final String username, final int searchTemplateId, final String kindOfRelationship, final Integer estimatedAge, final String genre, final String keyword, final String country, final String state, final String province,
		final String city, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos si la persona autentificada es chorbi
			this.searchTemplateService.checkIfChorbi();

			// Inicializamos los atributos para la edici�n
			SearchTemplate searchTemplate;

			searchTemplate = this.searchTemplateService.findOne(searchTemplateId);
			searchTemplate.setCity(city);
			searchTemplate.setCountry(country);
			searchTemplate.setEstimatedAge(estimatedAge);
			searchTemplate.setGenre(genre);
			searchTemplate.setKeyword(keyword);
			searchTemplate.setKindOfRelationship(kindOfRelationship);
			searchTemplate.setProvince(province);
			searchTemplate.setState(state);

			// Comprobamos que el chorbi autentificado es el autor del searchTemplate a editar
			this.searchTemplateService.checkActualChorbiAuthor(searchTemplate);

			// Comprobamos atributos
			this.searchTemplateService.checkAttributesOfSearchTemplate(searchTemplate);

			// Guardamos
			this.searchTemplateService.save(searchTemplate);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * "An actor who is authenticated as a chorbi must be able to:
	 * - Browse the results of his or her search template as long as he or she's registered a valid credit card.
	 * Note that the validity of the credit card must be checked every time the results of the search template
	 * are displayed. The results of search tem-plates must be cached for at least 12 hours."
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de ver los resultados del searchTemplate de un chorbi actualmente
	 * autentificado. Para forzar el error se pueden dar varios casos, en resumen son:
	 * 
	 * � La persona autentificada es un admin
	 * � La persona no esta autentificada
	 * � La tarjeta de cr�dito actual del chorbi autentificado no es v�lida
	 */
	public void searchTemplateShow(final String username, final CreditCard creditCard, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos si la persona autentificada es chorbi
			this.searchTemplateService.checkIfChorbi();

			// Inicializamos los atributos para la visualizaci�n
			SearchTemplate searchTemplate;
			Chorbi chorbi;

			chorbi = this.searchTemplateService.findChorbiByPrincipal();
			searchTemplate = chorbi.getSearchTemplate();

			// Comprobamos que la tarjeta de cr�dito del chorbi actual es v�lida

			chorbi.setCreditCard(creditCard);
			this.searchTemplateService.checkCreditCard(chorbi);

			// Visualizamos
			this.searchTemplateService.getSearchTemplateResults(searchTemplate);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------
	@Test
	public void driverSearchTemplateEdit() {

		final Object testingData[][] = {
			// Editar searchTemplate con administrador autentificado -> false
			{
				"admin", 96, "activities", 20, "man", "keyword", "country", "state", "province", "city", IllegalArgumentException.class
			},
			// Editar searchTemplate sin autentificar -> false
			{
				null, 96, "love", 20, "man", "keyword", "country", "state", "province", "city", IllegalArgumentException.class
			},
			// Editar searchTemplate con chorbi no autor de la misma -> false
			{
				"chorbi2", 96, "friendship", 20, "woman", "keyword", "country", "state", "province", "city", IllegalArgumentException.class
			},
			// Editar searchTemplate con edad estimada menor que 18 -> false
			{
				"chorbi1", 96, "activities", 15, "man", "keyword", "country", "state", "province", "city", IllegalArgumentException.class
			},
			// Editar searchTemplate con kindOfRelationship no existente -> false
			{
				"chorbi1", 96, "thisIsNotCorrect", 20, "woman", "keyword", "country", "state", "province", "city", IllegalArgumentException.class
			},
			// Editar searchTemplate con genre no existente -> false
			{
				"chorbi1", 8999, "love", 20, "nothing", "keyword", "country", "state", "province", "city", IllegalArgumentException.class
			},
			// Editar searchTemplate con keyword mayor que una sola palabra -> false
			{
				"chorbi1", 96, "love", 20, "nothing", "more than one word", "country", "state", "province", "city", IllegalArgumentException.class
			},
			// Editar searchTemplate con chorbi(1) autor de la misma y atributos correctos (todos) -> true
			{
				"chorbi1", 96, "friendship", 23, "woman", "keyword", "country", "state", "province", "city", null
			},
			// Editar searchTemplate con chorbi(2) autor de la misma y atributos correctos (todos) -> true
			{
				"chorbi2", 98, "activities", 31, "man", "keyword", "country", "state", "province", "city", null
			},
			// Editar searchTemplate con chorbi(1) autor de la misma y atributos correctos (algunos en blanco) - 1 -> true
			{
				"chorbi1", 96, "friendship", 27, "woman", "keyword", "country", "", "", "", null
			},
			// Editar searchTemplate con chorbi(1) autor de la misma y atributos correctos (algunos en blanco) - 2 -> true
			{
				"chorbi1", 96, "activities", null, "", "keyword", "", "", "", "", null
			},
			// Editar searchTemplate con chorbi(2) autor de la misma y atributos correctos (algunos en blanco) - 3 -> true
			{
				"chorbi2", 98, "", 19, "man", "", "", "", "", "", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.searchTemplateEdit((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (Integer) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (String) testingData[i][9], (Class<?>) testingData[i][10]);
	}

	@Test
	public void driverSearchTemplateShow() {

		final CreditCard badOne1 = new CreditCard();
		final CreditCard badOne2 = new CreditCard();
		final CreditCard goodOne = new CreditCard();

		badOne1.setBrandName("VISAa"); // Brand name inv�lido
		badOne1.setCvvCode(123);
		badOne1.setExpirationMonth(10);
		badOne1.setExpirationYear(2020);
		badOne1.setHolderName("Holder name");
		badOne1.setNumber("6382187214962573");

		badOne2.setBrandName("AMEX");
		badOne2.setCvvCode(123);
		badOne2.setExpirationMonth(10);
		badOne2.setExpirationYear(2020);
		badOne2.setHolderName("Holder name");
		badOne2.setNumber("3213387964563254657"); // N� inv�lido

		goodOne.setBrandName("MASTERCARD");
		goodOne.setCvvCode(123);
		goodOne.setExpirationMonth(10);
		goodOne.setExpirationYear(2020);
		goodOne.setHolderName("Holder name");
		goodOne.setNumber("6382187214962573");

		final Object testingData[][] = {
			// Show searchTemplate con admin -> false
			{
				"admin", goodOne, IllegalArgumentException.class
			},
			// Show searchTemplate sin autentificarse -> false
			{
				null, goodOne, IllegalArgumentException.class
			},
			// Show searchTemplate con chorbi (1) -> true
			{
				"chorbi1", goodOne, null
			},
			// Show searchTemplate con chorbi (2) -> true
			{
				"chorbi2", goodOne, null
			},
			// Show searchTemplate con chorbi (1) y creditCard inv�lida(1) -> false
			{
				"chorbi2", badOne1, IllegalArgumentException.class
			},
			// Show searchTemplate con chorbi (1) y creditCard inv�lida(2) -> false
			{
				"chorbi2", badOne2, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.searchTemplateShow((String) testingData[i][0], (CreditCard) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
