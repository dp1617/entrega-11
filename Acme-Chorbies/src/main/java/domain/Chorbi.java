
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.data.annotation.Transient;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Chorbi extends Actor {

	// Own attributes

	private String		picture;
	private String		description;
	private String		kindOfRelationship;
	private Date		birthDate;
	private String		genre;
	private CreditCard	creditCard;
	private Boolean		banned;
	private String		country;
	private String		state;
	private String		province;
	private String		city;


	// Constructor

	public Chorbi() {
		super();
	}

	// Gets and sets

	@URL
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPicture() {
		return this.picture;
	}
	public void setPicture(final String picture) {
		this.picture = picture;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getDescription() {
		return this.description;
	}
	public void setDescription(final String description) {
		this.description = description;
	}

	@NotBlank
	@Pattern(regexp = "^(activities|friendship|love)$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getKindOfRelationship() {
		return this.kindOfRelationship;
	}
	public void setKindOfRelationship(final String kindOfRelationship) {
		this.kindOfRelationship = kindOfRelationship;
	}

	@Past
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	public Date getBirthDate() {
		return this.birthDate;
	}
	public void setBirthDate(final Date birthDate) {
		this.birthDate = birthDate;
	}

	@NotBlank
	@Pattern(regexp = "^(man|woman)$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getGenre() {
		return this.genre;
	}
	public void setGenre(final String genre) {
		this.genre = genre;
	}

	@NotNull
	public CreditCard getCreditCard() {
		return this.creditCard;
	}
	public void setCreditCard(final CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	@NotNull
	public Boolean getBanned() {
		return this.banned;
	}
	public void setBanned(final Boolean banned) {
		this.banned = banned;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getCountry() {
		return this.country;
	}
	public void setCountry(final String country) {
		this.country = country;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getState() {
		return this.state;
	}
	public void setState(final String state) {
		this.state = state;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getProvince() {
		return this.province;
	}
	public void setProvince(final String province) {
		this.province = province;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getCity() {
		return this.city;
	}
	public void setCity(final String city) {
		this.city = city;
	}


	//Relation ships

	private Collection<Likes>	sentLikes;
	private Collection<Likes>	receivedLikes;
	private SearchTemplate		searchTemplate;
	private Collection<Chirp>	receivedChirps;


	@Valid
	@OneToMany(mappedBy = "likingChorbi")
	public Collection<Likes> getSentLikes() {
		return this.sentLikes;
	}
	public void setSentLikes(final Collection<Likes> sentLikes) {
		this.sentLikes = sentLikes;
	}

	@Valid
	@OneToMany(mappedBy = "likedChorbi")
	public Collection<Likes> getReceivedLikes() {
		return this.receivedLikes;
	}
	public void setReceivedLikes(final Collection<Likes> receivedLikes) {
		this.receivedLikes = receivedLikes;
	}

	@Valid
	@OneToOne(cascade = CascadeType.ALL, optional = false, mappedBy = "chorbi")
	public SearchTemplate getSearchTemplate() {
		return this.searchTemplate;
	}
	public void setSearchTemplate(final SearchTemplate searchTemplate) {
		this.searchTemplate = searchTemplate;
	}

	@OneToMany(mappedBy = "recipient")
	public Collection<Chirp> getReceivedChirps() {
		return this.receivedChirps;
	}
	public void setReceivedChirps(final Collection<Chirp> receivedChirps) {
		this.receivedChirps = receivedChirps;
	}


	private Collection<Fee>		fees;
	private Collection<Event>	events;


	@Valid
	@OneToMany(mappedBy = "chorbi")
	public Collection<Fee> getFees() {
		return this.fees;
	}
	public void setFees(final Collection<Fee> fees) {
		this.fees = fees;
	}

	@Valid
	@ManyToMany
	public Collection<Event> getEvents() {
		return this.events;
	}
	public void setEvents(final Collection<Event> events) {
		this.events = events;
	}

	// Others

	@Transient
	public Integer getChorbiAge() {

		final LocalDate birthday = new LocalDate(this.getBirthDate());
		final LocalDate now = new LocalDate();
		final Years age = Years.yearsBetween(birthday, now);

		return age.getYears();
	}

	public void setChorbiAge(final Integer chorbiAge) {
	}

}
