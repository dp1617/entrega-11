
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

@Entity
@Access(AccessType.PROPERTY)
public class Manager extends Actor {

	// Own attributes ------------------------------------------------------

	private String		company;
	private String		VATNumber;
	private CreditCard	creditCard;


	// Constructor ------------------------------------------------------

	public Manager() {
		super();
	}

	// Gets and sets ------------------------------------------------------

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getCompany() {
		return this.company;
	}
	public void setCompany(final String company) {
		this.company = company;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getVATNumber() {
		return this.VATNumber;
	}
	public void setVATNumber(final String vATNumber) {
		this.VATNumber = vATNumber;
	}

	/*
	 * Validate credit card is used in the services
	 */
	@NotNull
	public CreditCard getCreditCard() {
		return this.creditCard;
	}
	public void setCreditCard(final CreditCard creditCard) {
		this.creditCard = creditCard;
	}


	// Relationships ------------------------------------------------------

	private Collection<Fee>		fees;
	private Collection<Event>	events;


	@Valid
	@OneToMany(mappedBy = "manager")
	public Collection<Fee> getFees() {
		return this.fees;
	}
	public void setFees(final Collection<Fee> fees) {
		this.fees = fees;
	}

	@Valid
	@OneToMany(mappedBy = "manager")
	public Collection<Event> getEvents() {
		return this.events;
	}
	public void setEvents(final Collection<Event> events) {
		this.events = events;
	}

}
