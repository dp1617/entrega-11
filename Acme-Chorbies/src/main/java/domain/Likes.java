
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Likes extends DomainEntity {

	// Own attributes

	private Date	likedMoment;
	private String	comment;
	private Integer numberStars;


	// Constructor

	public Likes() {
		super();
	}

	// Gets and sets

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getLikedMoment() {
		return this.likedMoment;
	}
	public void setLikedMoment(final Date likedMoment) {
		this.likedMoment = likedMoment;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getComment() {
		return this.comment;
	}
	public void setComment(final String comment) {
		this.comment = comment;
	}
	
	@Min(0)
	@Max(3)
	public Integer getNumberStars() {
		return numberStars;
	}

	public void setNumberStars(Integer numberStars) {
		this.numberStars = numberStars;
	}


	// Relationships

	private Chorbi	likingChorbi;
	private Chorbi	likedChorbi;


	@Valid
	@ManyToOne(optional = false)
	public Chorbi getLikingChorbi() {
		return this.likingChorbi;
	}
	public void setLikingChorbi(final Chorbi likingChorbi) {
		this.likingChorbi = likingChorbi;
	}

	@Valid
	@ManyToOne(optional = false)
	public Chorbi getLikedChorbi() {
		return this.likedChorbi;
	}
	public void setLikedChorbi(final Chorbi likedChorbi) {
		this.likedChorbi = likedChorbi;
	}

}
