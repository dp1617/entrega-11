
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Chirp extends DomainEntity {

	// Own attributes

	private String				fromActor;
	private String				toChorbi;
	private Date				sentMoment;
	private String				subject;
	private String				text;
	private Collection<String>	attachments;


	// Constructor

	public Chirp() {
		super();
	}

	// Getters and setters 

	@NotBlank
	public String getFromActor() {
		return this.fromActor;
	}
	public void setFromActor(final String fromActor) {
		this.fromActor = fromActor;
	}

	@NotBlank
	public String getToChorbi() {
		return this.toChorbi;
	}
	public void setToChorbi(final String toChorbi) {
		this.toChorbi = toChorbi;
	}

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getSentMoment() {
		return this.sentMoment;
	}
	public void setSentMoment(final Date sentMoment) {
		this.sentMoment = sentMoment;
	}

	@NotBlank
	public String getSubject() {
		return this.subject;
	}
	public void setSubject(final String subject) {
		this.subject = subject;
	}

	@NotBlank
	public String getText() {
		return this.text;
	}
	public void setText(final String text) {
		this.text = text;
	}

	@ElementCollection
	public Collection<String> getAttachments() {
		return this.attachments;
	}
	public void setAttachments(final Collection<String> attachments) {
		this.attachments = attachments;
	}


	// Relationships

	private Actor	sender;
	private Chorbi	recipient;


	@Valid
	@ManyToOne(optional = true)
	public Actor getSender() {
		return this.sender;
	}
	public void setSender(final Actor sender) {
		this.sender = sender;
	}

	@Valid
	@ManyToOne(optional = true)
	public Chorbi getRecipient() {
		return this.recipient;
	}
	public void setRecipient(final Chorbi recipient) {
		this.recipient = recipient;
	}

}
