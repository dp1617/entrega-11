
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Tax extends DomainEntity {

	// Own attributes

	private Double	managerValue;
	private Double	chorbiValue;


	@NotNull
	@Min(0)
	public Double getManagerValue() {
		return this.managerValue;
	}
	public void setManagerValue(final Double managerValue) {
		this.managerValue = managerValue;
	}

	@NotNull
	@Min(0)
	public Double getChorbiValue() {
		return this.chorbiValue;
	}
	public void setChorbiValue(final Double chorbiValue) {
		this.chorbiValue = chorbiValue;
	}

}
