
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Fee extends DomainEntity {

	private Double	value;
	private Boolean	isPaid;


	@NotNull
	@Min(0)
	public Double getValue() {
		return this.value;
	}
	public void setValue(final Double value) {
		this.value = value;
	}

	@NotNull
	public Boolean getIsPaid() {
		return this.isPaid;
	}
	public void setIsPaid(final Boolean isPaid) {
		this.isPaid = isPaid;
	}


	// Relationships ------------------------------------------------------

	/*
	 * If is a manager who registers a event, chorbi is null. If is a chorbi who enters a event, manager is null.
	 * No fee with manager and chorbi at null will be valid (method in the services that checks it)
	 */
	private Event	event;
	private Manager	manager;
	private Chorbi	chorbi;


	@Valid
	@OneToOne(cascade = CascadeType.ALL, optional = true)
	public Event getEvent() {
		return this.event;
	}
	public void setEvent(final Event event) {
		this.event = event;
	}

	@Valid
	@ManyToOne(optional = true)
	public Manager getManager() {
		return this.manager;
	}
	public void setManager(final Manager manager) {
		this.manager = manager;
	}

	@Valid
	@ManyToOne(optional = true)
	public Chorbi getChorbi() {
		return this.chorbi;
	}
	public void setChorbi(final Chorbi chorbi) {
		this.chorbi = chorbi;
	}

}
