
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.LikesRepository;
import domain.Chorbi;
import domain.Likes;

@Service
@Transactional
public class LikesService {

	// Managed Repository ---------------------------------------------------------
	@Autowired
	private LikesRepository	likesRepository;

	// Supporting services ---------------------------------------------------------
	@Autowired
	private ChorbiService	chorbiService;

	@Autowired
	private ActorService	actorService;


	// Constructor methods ---------------------------------------------------------
	public LikesService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------------

	public Likes create(final Chorbi chorbi) {
		this.chorbiService.checkIfChorbi();

		final Likes like = new Likes();

		like.setLikedChorbi(chorbi);
		like.setLikedMoment(new Date());

		Assert.notNull(like);

		return like;
	}

	public Likes findOne(final int likesiId) {
		this.chorbiService.checkIfChorbi();

		Assert.isTrue(likesiId != 0);
		Likes result;

		result = this.likesRepository.findOne(likesiId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Likes> findAll() {
		Collection<Likes> result;

		result = this.likesRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Likes l) {
		this.chorbiService.checkIfChorbi();
		Assert.notNull(l);

		this.likesRepository.save(l);
	}

	public void delete(final Likes like) {
		this.chorbiService.checkIfChorbi();
		this.checkLike2(like);
		Assert.notNull(like);
		this.likesRepository.delete(like);
	}

	// Auxiliar methods ---------------------------------

	public boolean checkLike1(final Likes likes) {
		boolean res = true;
		Chorbi likingChorbi;

		likingChorbi = this.chorbiService.findByPrincipal();

		for (final Likes l : likingChorbi.getSentLikes())
			if (l.getLikedChorbi().getId() == likes.getLikedChorbi().getId()) {
				res = false;
				break;
			}

		Assert.isTrue(res, "Ya has realizado esta acci�n antes");

		return res;
	}

	public boolean checkLike2(final Likes likes) {
		boolean res = false;
		Assert.notNull(likes, "Ya has realizado esta acci�n antes");
		Chorbi likingChorbi;

		likingChorbi = this.chorbiService.findByPrincipal();

		for (final Likes l : likingChorbi.getSentLikes())
			if (l.getLikedChorbi().getId() == likes.getLikedChorbi().getId()) {
				res = true;
				break;
			}

		Assert.isTrue(res, "Ya has realizado esta acci�n antes");

		return res;
	}

	public void sendLike(final Likes l) {
		this.chorbiService.checkIfChorbi();
		Assert.notNull(l);
		Assert.isTrue(l.getNumberStars()>=0 && l.getNumberStars()<=3);
		this.checkLike1(l);

		final Chorbi me = this.chorbiService.findByPrincipal();
		final Chorbi chorbi = l.getLikedChorbi();

		l.setLikingChorbi(me);

		final Collection<Likes> likedChorbiLikes = chorbi.getReceivedLikes();
		final Collection<Likes> likingChorbiLikes = me.getSentLikes();

		likedChorbiLikes.add(l);
		likingChorbiLikes.add(l);

		chorbi.setReceivedLikes(likedChorbiLikes);
		me.setSentLikes(likingChorbiLikes);
		this.save(l);

		this.chorbiService.save(chorbi);
		this.chorbiService.save(me);
	}

	public Collection<Chorbi> findLikingChorbies(final int chorbiId) {
		this.actorService.checkIfActor();
		Chorbi chorbi;
		chorbi = this.chorbiService.findOne(chorbiId);
		final Collection<Chorbi> res = new ArrayList<Chorbi>();
		for (final Likes l : chorbi.getReceivedLikes())
			if (!(res.contains(l.getLikingChorbi())))
				res.add(l.getLikingChorbi());
		return res;
	}

	public Collection<Likes> findComments(final int chorbiId) {
		final Collection<Likes> comments = new ArrayList<Likes>();
		Chorbi chorbi;
		chorbi = this.chorbiService.findOne(chorbiId);
		for (final Likes l : chorbi.getReceivedLikes())
			if (!(comments.contains(l)))
				comments.add(l);
		return comments;
	}

}
