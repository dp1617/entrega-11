
package services;

import java.util.Calendar;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.ActorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.CreditCard;

@Service
@Transactional
public class ActorService {

	// Managed Repository
	@Autowired
	private ActorRepository	actorRepository;


	// Constructor methods
	public ActorService() {
		super();
	}

	// Supporting services

	// Simple CRUD methods

	public Actor findOne(final int actorId) {
		Assert.isTrue(actorId != 0);
		Actor result;

		result = this.actorRepository.findOne(actorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Actor> findAll() {
		Collection<Actor> result;

		result = this.actorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Actor actor) {
		Assert.notNull(actor);

		this.actorRepository.save(actor);
	}

	// Other bussines methods

	public void checkIfActor() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.ADMIN) || a.getAuthority().equals(Authority.CHORBI) || a.getAuthority().equals(Authority.MANAGER))
				res = true;
		Assert.isTrue(res);
	}

	public Actor findByPrincipal() {
		Actor result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.actorRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public boolean authenticated() {
		Boolean res = true;
		Object principal;
		SecurityContext context;
		Authentication authentication;

		context = SecurityContextHolder.getContext();
		authentication = context.getAuthentication();
		principal = authentication.getPrincipal();

		if (!(principal instanceof UserAccount))
			res = false;

		return res;
	}

	public Actor getActorByID(final Integer a) {
		final Actor actor = null;
		this.actorRepository.getActorByID(a);

		return actor;
	}

	public Actor findActorByUsername(final String username) {
		return this.actorRepository.findActorByUsername(username);
	}

	// M�todo para comprobar credit card
	public static boolean validateCreditCard(final CreditCard creditCard) {
		Assert.notNull(creditCard.getBrandName());
		Assert.notNull(creditCard.getCvvCode());
		Assert.notNull(creditCard.getExpirationMonth());
		Assert.notNull(creditCard.getNumber());
		Assert.notNull(creditCard.getExpirationYear());
		Assert.notNull(creditCard.getHolderName());

		int sum = 0;
		boolean alternate = false;
		boolean valid = false;
		Integer anyo, mes;
		Calendar fecha;
		final String brandName = creditCard.getBrandName();
		final String ccNumber = creditCard.getNumber();

		fecha = Calendar.getInstance();
		anyo = fecha.get(Calendar.YEAR);
		mes = fecha.get(Calendar.MONTH) + 1;

		for (int i = ccNumber.length() - 1; i >= 0; i--) {
			int n = Integer.parseInt(ccNumber.substring(i, i + 1));
			if (alternate) {
				n *= 2;
				if (n > 9)
					n = (n % 10) + 1;
			}
			sum += n;
			alternate = !alternate;
		}

		if (sum % 10 == 0)
			valid = true;

		if (creditCard.getExpirationYear() < anyo)
			valid = false;
		else if (creditCard.getExpirationYear() == anyo)
			if (creditCard.getExpirationMonth() < mes)
				valid = false;

		if (!brandName.matches("^(VISA|MASTERCARD|AMEX|DINNERS|DISCOVER)$"))
			valid = false;

		if (creditCard.getCvvCode() > 999 || creditCard.getCvvCode() < 100)
			valid = false;

		return valid;
	}
}
