
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.FeeRepository;
import domain.Chorbi;
import domain.Event;
import domain.Fee;

@Service
@Transactional
public class FeeService {

	// Managed repository
	@Autowired
	private FeeRepository			feeRepository;

	// Servicios
	@Autowired
	private AdministratorService	adminService;
	@Autowired
	private ManagerService			managerService;
	@Autowired
	private TaxService				taxService;


	// Constructors
	public FeeService() {
		super();
	}

	// Simple CRUD methods

	// createManager crea un fee si un manager registra un evento
	// createChorbi crea un fee lanzado por el proceso que suma fees mensuales a cada chorbi

	public Fee createManager(final Event event) {
		this.managerService.checkIfManager();

		Fee fee;
		fee = new Fee();

		fee.setManager(this.managerService.findByPrincipal());
		fee.setChorbi(null);

		fee.setEvent(event);
		fee.setValue(this.taxService.findManagerFeeValue());
		fee.setIsPaid(false);

		Assert.notNull(fee);

		return fee;
	}

	public Fee createChorbi(final Chorbi chorbi) {
		this.adminService.checkIfAdministrator();

		Fee fee;
		fee = new Fee();

		fee.setManager(null);
		fee.setChorbi(chorbi);
		fee.setEvent(null);
		fee.setIsPaid(false);

		Assert.notNull(fee);

		return fee;
	}

	public Collection<Fee> findAll() {
		Collection<Fee> fees;
		fees = this.feeRepository.findAll();
		Assert.notNull(fees);
		return fees;
	}

	public Fee findOne(final int FeeId) {
		Assert.isTrue(FeeId != 0);
		Fee result;
		result = this.feeRepository.findOne(FeeId);
		Assert.notNull(result);
		return result;
	}

	public Fee save(final Fee fee) {
		Fee result;
		Assert.notNull(fee);
		result = this.feeRepository.save(fee);
		return result;
	}

	public Fee setToPaid(final Fee fee) {
		this.adminService.checkIfAdministrator();
		Fee result;
		Assert.notNull(fee);
		result = this.feeRepository.save(fee);
		return result;
	}

	public void delete(final Fee f) {
		this.feeRepository.delete(f);
	}

}
