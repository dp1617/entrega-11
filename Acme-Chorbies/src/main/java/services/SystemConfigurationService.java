
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.SystemConfigurationRepository;
import domain.SystemConfiguration;

@Service
@Transactional
public class SystemConfigurationService {

	// Managed Repository ---------------------------------------------------------

	@Autowired
	private SystemConfigurationRepository	systemConfigurationRepository;


	// Constructor methods ---------------------------------------------------------

	public SystemConfigurationService() {
		super();
	}


	// Supporting services ---------------------------------------------------------

	@Autowired
	private AdministratorService	adminService;


	// Simple CRUD methods ---------------------------------------------------------

	public Collection<SystemConfiguration> findAll() {

		Collection<SystemConfiguration> result;

		result = this.systemConfigurationRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	// Other bussines methods ---------------------------------------------------------

	// Get actual cache time
	public Date getCacheTime() {
		Date cacheTime = new Date();
		final Collection<SystemConfiguration> systemConfigurations = this.findAll();

		for (final SystemConfiguration sc : systemConfigurations) {
			cacheTime = sc.getCacheTime();
			break;
		}

		return cacheTime;
	}

	public String getRamdomBanner() {
		Collection<String> banners = new ArrayList<>();

		for (final SystemConfiguration s : this.findAll()) {
			banners = s.getBanners();
			break;
		}

		if (!banners.isEmpty()) {
			final List<String> bannersList = new ArrayList<>(banners);
			final int i = ThreadLocalRandom.current().nextInt(0, banners.size());

			return bannersList.get(i);
		} else
			return "https://image.ibb.co/mMOF35/banners.jpg";
	}

	public SystemConfiguration getSystemConfiguration() {
		this.adminService.checkIfAdministrator();
		SystemConfiguration res = new SystemConfiguration();

		for (final SystemConfiguration s : this.findAll()) {
			res = s;
			break;
		}

		return res;
	}

	public SystemConfiguration save(final SystemConfiguration systemConfiguration) {
		this.adminService.checkIfAdministrator();
		Assert.notNull(systemConfiguration);
		this.checkBanners(systemConfiguration.getBanners());
		this.systemConfigurationRepository.save(systemConfiguration);
		return systemConfiguration;
	}

	public void checkBanners(final Collection<String> banners) {
		final String regexp = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		for (final String s : banners)
			Assert.isTrue(s.matches(regexp), "El formato de los banners no es correcto");
	}
}
