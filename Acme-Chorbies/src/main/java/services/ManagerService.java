
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.ManagerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Chirp;
import domain.CreditCard;
import domain.Event;
import domain.Fee;
import domain.Manager;
import forms.ManagerForm;

@Service
@Transactional
public class ManagerService {

	// Managed Repository -------------------------------------------------------------------
	@Autowired
	private ManagerRepository		managerRepository;

	// Supporting services -------------------------------------------------------------------
	@Autowired
	private AdministratorService	administratorService;
	@Autowired
	private ActorService			actorService;


	// Simple CRUD methods -------------------------------------------------------------------

	public void save(final Manager manager) {
		Assert.notNull(manager);
		this.administratorService.checkIfAdministrator();

		if (manager.getCreditCard() != null)
			Assert.isTrue(ActorService.validateCreditCard(manager.getCreditCard()), "La tarjeta de credito no es valida");
		this.managerRepository.save(manager);
	}

	public void saveForManager(final Manager manager) {
		Assert.notNull(manager);
		this.checkIfManager();

		if (manager.getCreditCard() != null)
			Assert.isTrue(ActorService.validateCreditCard(manager.getCreditCard()), "La tarjeta de credito no es valida");
		this.managerRepository.save(manager);
	}

	// Other bussines methods -------------------------------------------------------------------

	public void checkIfManager() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.MANAGER))
				res = true;
		Assert.isTrue(res);
	}

	public Manager findByPrincipal() {
		Manager result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.managerRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public Manager findManagerByUsername(final String username) {
		return this.managerRepository.findManagerByUsername(username);
	}

	public Manager reconstruct(final ManagerForm c) {
		final Manager manager = new Manager();

		final UserAccount account = new UserAccount();
		account.setPassword(c.getPassword());
		account.setUsername(c.getUsername());

		final CreditCard creditCard = new CreditCard();

		creditCard.setHolderName(c.getHolderName());
		creditCard.setBrandName(c.getBrandName());
		creditCard.setNumber(c.getNumber());
		creditCard.setExpirationMonth(c.getExpirationMonth());
		creditCard.setExpirationYear(c.getExpirationYear());
		creditCard.setCvvCode(c.getCvvCode());

		manager.setId(0);
		manager.setVersion(0);
		manager.setName(c.getName());
		manager.setSurname(c.getSurname());
		manager.setPhone(c.getPhone());
		manager.setEmail(c.getEmail());
		manager.setCompany(c.getCompany());
		manager.setVATNumber(c.getVATNumber());
		manager.setUserAccount(account);

		manager.setEvents(new ArrayList<Event>());
		manager.setFees(new ArrayList<Fee>());
		manager.setSentChirps(new ArrayList<Chirp>());

		manager.setCreditCard(creditCard);

		if (!manager.getUserAccount().getPassword().equals(c.getSecondPassword()))
			throw new IllegalArgumentException("Passwords do not match");

		if (c.getCheckBox() == false)
			throw new IllegalArgumentException("You must accept the term and conditions");

		return manager;
	}

	public void saveForm(Manager manager) {

		Assert.notNull(manager);
		String password;
		final Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		final Collection<Authority> auths = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("MANAGER");
		auths.add(auth);

		password = manager.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		manager.getUserAccount().setPassword(password);

		manager.getUserAccount().setAuthorities(auths);

		if (manager.getCreditCard() != null)
			Assert.isTrue(ActorService.validateCreditCard(manager.getCreditCard()), "La tarjeta de credito no es valida");

		manager = this.managerRepository.saveAndFlush(manager);
	}

	// Para testeo
	public void checkAttributes(final ManagerForm m) {
		Assert.isTrue(m.getPhone().matches("(^(\\+\\d{1,3})?\\d{9}$)|(^(\\*\\*\\*)$)"));
		Assert.isTrue(m.getEmail().matches("^[A-Za-z0-9+_.-]+@(.+)$|^(\\*\\*\\*)"));
		for (final Actor a : this.actorService.findAll())
			Assert.isTrue(!a.getUserAccount().getUsername().equals(m.getUsername()));
	}

	// Dashboard 2.0
	public Collection<Object[]> managersSortedByNumberOfEvents() {
		Collection<Object[]> res;
		res = this.managerRepository.managersSortedByNumberOfEvents();
		return res;
	}

	public Collection<Object[]> managersAndAmountDueInFees() {
		Collection<Object[]> res;
		res = this.managerRepository.managersAndAmountDueInFees();
		return res;
	}

}
