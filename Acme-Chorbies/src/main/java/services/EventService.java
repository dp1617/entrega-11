
package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.EventRepository;
import domain.Chirp;
import domain.Chorbi;
import domain.Event;
import domain.Fee;
import domain.Manager;

@Service
@Transactional
public class EventService {

	// Managed Repository -------------------------------------------------------------------
	@Autowired
	private EventRepository	eventRepository;

	// Supporting services -------------------------------------------------------------------7

	@Autowired
	private ManagerService	managerService;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private FeeService		feeService;
	@Autowired
	private ChirpService	chirpService;
	@Autowired
	private ChorbiService	chorbiService;


	// Simple CRUD methods -------------------------------------------------------------------

	public Event create() {
		this.checkIfManager();

		final Manager manager = this.findManagerByPrincipal();
		final Event event = new Event();

		event.setChorbies(new ArrayList<Chorbi>());
		event.setManager(manager);
		event.setFreeSeats(0);
		event.setType("none");

		Assert.notNull(event);

		return event;
	}

	public Collection<Event> findAll() {
		Collection<Event> result;
		result = this.eventRepository.findAll();
		Assert.notNull(result);
		return result;
	}

	public Event findOne(final int eventId) {
		this.checkIfManager();
		Assert.isTrue(eventId != 0);
		Event result;

		result = this.eventRepository.findOne(eventId);
		Assert.notNull(result);

		return result;
	}

	public Event findOne2(final int eventId) {
		this.chorbiService.checkIfChorbi();
		Assert.isTrue(eventId != 0);
		Event result;

		result = this.eventRepository.findOne(eventId);
		Assert.notNull(result);

		return result;
	}

	public void delete(final Event event) {
		this.checkIfManager();
		this.checkIfAutor(event);
		Assert.notNull(event);
		this.sendChangesDelete(event);
		this.deleteEventFees(event);
		this.eventRepository.delete(event);
	}

	public void save(final Event event) {
		this.checkIfManager();
		this.checkIfAutor(event);

		Assert.notNull(event);
		Assert.isTrue(event.getOrganisedMoment().after(new Date()), "La fecha es incorrecta");

		this.setType(event);

		this.sendChangesSave(event);

		this.eventRepository.save(event);
	}

	public void saveCreate(final Event event) {
		this.checkIfManager();
		this.checkCreditCard(this.findManagerByPrincipal());
		Assert.notNull(event);

		event.setFreeSeats(event.getSeats());

		Assert.isTrue(event.getOrganisedMoment().after(new Date()), "La fecha es incorrecta");
		this.checkIfUnique(event);

		// Atribuir type (para colores en la tabla)
		this.setType(event);

		// Atribuir fee a event
		final Fee fee = this.feeService.createManager(event);
		this.feeService.save(fee);
	}

	public void saveEventChorbi(final Event event) {
		this.chorbiService.checkIfChorbi();
		Assert.notNull(event);

		// Atribuir type (para colores en la tabla)
		this.setType(event);

		this.eventRepository.save(event);

	}

	public void saveType(final Collection<Event> evs) {
		for (final Event e : evs) {
			this.setType(e);
			this.eventRepository.save(e);
		}
	}

	// Other bussines methods -------------------------------------------------------------------

	private void checkIfUnique(final Event ev) {
		Boolean res = true;
		for (final Event e : this.findAll())
			if (e.getManager().equals(ev.getManager()) && e.getDescription().equals(ev.getDescription()) && e.getOrganisedMoment().getTime() == ev.getOrganisedMoment().getTime() && e.getSeats().equals(ev.getSeats()) && e.getTitle().equals(ev.getTitle())) {
				res = false;
				break;
			}
		Assert.isTrue(res, "Ya has creado uno igual");
	}
	private void deleteEventFees(final Event e) {
		final Collection<Event> aux = new ArrayList<>();
		Collection<Event> aux1 = new ArrayList<>();
		for (final Chorbi c : e.getChorbies())
			for (final Event ev : c.getEvents())
				if (ev.getId() == e.getId()) {
					aux.add(ev);
					aux1 = c.getEvents();
					aux1.removeAll(aux);
					c.setEvents(aux1);
					this.chorbiService.save(c);
				}
		for (final Fee f : this.getEventFees(e)) {
			f.setEvent(null);
			this.feeService.save(f);
			this.feeService.delete(f);
		}
	}

	private Collection<Fee> getEventFees(final Event e) {
		final Collection<Fee> res = new ArrayList<>();

		for (final Fee f : this.feeService.findAll())
			if (f.getEvent().equals(e))
				res.add(f);

		return res;
	}

	public void getUnRegisterEvent(final Event e) {
		final Chorbi c = this.chorbiService.findByPrincipal();
		final Collection<Chorbi> chorbies = e.getChorbies();
		final Collection<Event> myevents = c.getEvents();
		myevents.remove(e);
		chorbies.remove(c);
		e.setChorbies(chorbies);
		e.setFreeSeats(e.getFreeSeats() + 1);
		this.saveEventChorbi(e);

	}
	public void getRegisterEvent(final Event e) {
		final Collection<Chorbi> chorbies = e.getChorbies();
		final Chorbi c = this.chorbiService.findByPrincipal();
		final Collection<Event> myevents = c.getEvents();
		Assert.isTrue(!c.getEvents().contains(e), "Ya estas registrado en este evento");
		Assert.isTrue(e.getFreeSeats() > 0, "Todas las plazas est�n llenas");
		Assert.isTrue(!e.getType().equals("pastEvent"), "Este evento ya ha pasado");
		if (e.getFreeSeats() > 0) {
			chorbies.add(c);
			e.setFreeSeats(e.getFreeSeats() - 1);
			this.saveEventChorbi(e);
			myevents.add(e);

		}

	}

	private void sendChangesSave(final Event e) {
		Event toSee = null;
		Boolean state = false;
		String text = "The following changes had been done: ";

		for (final Event ev : this.findManagerByPrincipal().getEvents())
			if (ev.getId() == e.getId())
				toSee = ev;

		if (!toSee.getDescription().equals(e.getDescription())) {
			state = true;
			text += "DESCRIPTION: " + e.getDescription() + ".\n";
		}
		if (!toSee.getPicture().equals(e.getPicture())) {
			state = true;
			text += "PICTURE: " + e.getPicture() + ".\n";
		}
		if (toSee.getOrganisedMoment().getTime() != e.getOrganisedMoment().getTime()) {
			state = true;
			text += "ORGANISED MOMENT: " + e.getOrganisedMoment() + ".\n";
		}
		if (!toSee.getTitle().equals(e.getTitle())) {
			state = true;
			text += "TITLE: " + e.getTitle() + ".\n";
		}

		if (state) {
			final Chirp chirp = this.chirpService.create();
			for (final Chorbi ch : e.getChorbies()) {
				chirp.setRecipient(ch);
				chirp.setSubject("The event with title " + toSee.getTitle() + " has been modified");
				chirp.setText(text);
				chirp.setAttachments(new ArrayList<String>());
				final Chirp chirp1 = this.chirpService.reconstruct(chirp);
				this.chirpService.save(chirp1);
			}
		}
	}

	private void sendChangesDelete(final Event e) {
		final Chirp chirp = this.chirpService.create();
		for (final Chorbi ch : e.getChorbies()) {
			chirp.setRecipient(ch);
			chirp.setSubject("The event with title " + e.getTitle() + " has been deleted");
			chirp.setText("You have no longer access to the event, it has been deleted by his manager.");
			chirp.setAttachments(new ArrayList<String>());
			final Chirp chirp1 = this.chirpService.reconstruct(chirp);
			this.chirpService.save(chirp1);
		}
	}

	public void checkCreditCard(final Manager m) {
		Assert.isTrue(ActorService.validateCreditCard(m.getCreditCard()), "La tarjeta de credito no es valida");
	}

	public void setType(final Event e) {
		final Date now = new Date();
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 30);

		final Date oneMonth = cal.getTime();

		if (e.getOrganisedMoment().getTime() >= now.getTime() && e.getOrganisedMoment().getTime() <= oneMonth.getTime() && e.getFreeSeats() > 0)
			e.setType("oneMonthAndSeats");
		else if (e.getOrganisedMoment().getTime() < now.getTime())
			e.setType("pastEvent");
		else
			e.setType("none");
	}

	public Collection<Event> findSpecials() {
		final Collection<Event> res = new ArrayList<>();

		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 30);

		final Date oneMonth = cal.getTime();

		for (final Event e : this.findAll())
			if (e.getOrganisedMoment().before(oneMonth) && e.getFreeSeats() > 0)
				res.add(e);

		return res;
	}
	public Manager findManagerByPrincipal() {
		return this.managerService.findByPrincipal();
	}

	public Chorbi findChorbiByPrincipal() {
		return this.chorbiService.findByPrincipal();
	}

	public void checkIfManager() {
		this.managerService.checkIfManager();
	}

	public void checkIfAutor(final Event e) {
		if (this.actorService.findByPrincipal() instanceof Manager)
			Assert.isTrue(e.getManager().getId() == this.findManagerByPrincipal().getId(), "No eres el autor de este evento");
	}

	// Para testeo
	public void checkAttributes(final Event e) {
		Assert.isTrue(e.getType().matches("^(pastEvent|oneMonthAndSeats|none)$"));
		Assert.isTrue(e.getPicture().matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"));
		Assert.isTrue(!e.getDescription().isEmpty() && !e.getTitle().isEmpty());
		Assert.isTrue(e.getSeats() >= 0);
	}
}
