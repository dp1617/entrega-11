
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.AdministratorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Administrator;
import domain.Chorbi;

@Service
@Transactional
public class AdministratorService {

	// Managed Repository
	@Autowired
	private AdministratorRepository	administratorRepository;


	// Constructor methods
	public AdministratorService() {
		super();
	}


	// Supporting services

	@Autowired
	private ChorbiService	chorbiService;


	// Simple CRUD methods

	public Administrator findOne(final int administratorId) {
		Assert.isTrue(administratorId != 0);
		Administrator result;

		result = this.administratorRepository.findOne(administratorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Administrator> findAll() {
		Collection<Administrator> result;

		result = this.administratorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Administrator administrator) {
		Assert.notNull(administrator);

		this.administratorRepository.save(administrator);
	}

	// Other bussines methods

	// Ban chorbi
	public void ban(final Chorbi c) {
		this.checkIfAdministrator();
		Assert.isTrue(!c.getBanned());
		c.setBanned(true);
		this.chorbiService.save(c);
	}

	// Unban chorbi
	public void unban(final Chorbi c) {
		this.checkIfAdministrator();
		Assert.isTrue(c.getBanned());
		c.setBanned(false);
		this.chorbiService.save(c);
	}

	public void checkIfAdministrator() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.ADMIN))
				res = true;
		Assert.isTrue(res);
	}

	public Administrator findByPrincipal() {
		Administrator result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.administratorRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}
}
