
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.ChorbiRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Chirp;
import domain.Chorbi;
import domain.CreditCard;
import domain.Likes;
import domain.SearchTemplate;
import forms.ChorbiForm;

@Service
@Transactional
public class ChorbiService {

	// Managed Repository ---------------------------------------------------------
	@Autowired
	private ChorbiRepository	chorbiRepository;

	// Supporting services ---------------------------------------------------------
	@Autowired
	private ActorService		actorService;


	// Constructor methods ---------------------------------------------------------
	public ChorbiService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------------

	public Chorbi findOne(final int chorbiId) {
		Assert.isTrue(chorbiId != 0);
		Chorbi result;

		result = this.chorbiRepository.findOne(chorbiId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Chorbi> findAll() {
		this.actorService.checkIfActor();
		Collection<Chorbi> result;

		result = this.chorbiRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Chorbi chorbi) {
		Assert.notNull(chorbi);
		this.checkChorbi(chorbi);

		this.checkKindOfRelationShip(chorbi.getKindOfRelationship());
		this.checkGenre(chorbi.getGenre());

		if (chorbi.getCreditCard() != null)
			Assert.isTrue(ActorService.validateCreditCard(chorbi.getCreditCard()), "La tarjeta de credito no es valida");
		Assert.isTrue(chorbi.getChorbiAge() >= 18, "No puede ser menor de 18 a�os");
		this.chorbiRepository.save(chorbi);
	}

	// Other bussines methods ---------------------------------------------------------

	// Querys for search template ********************************
	public Collection<Chorbi> getResultsByKinOfRelationship(final String kindOfRelationship) {
		return this.chorbiRepository.getResultsByKinOfRelationship(kindOfRelationship);
	}

	public Collection<Chorbi> getResultsByEstimatedAge(final int fiveLess, final int fiveMore) {
		return this.chorbiRepository.getResultsByEstimatedAge(fiveLess, fiveMore);
	}

	public Collection<Chorbi> getResultsByGenre(final String genre) {
		return this.chorbiRepository.getResultsByGenre(genre);
	}

	public Collection<Chorbi> getResultsByKeyword(final String keyword) {
		return this.chorbiRepository.getResultsByKeyword(keyword);
	}

	public Collection<Chorbi> getResultsByCountry(final String country) {
		return this.chorbiRepository.getResultsByCountry(country);
	}

	public Collection<Chorbi> getResultsByState(final String state) {
		return this.chorbiRepository.getResultsByState(state);
	}

	public Collection<Chorbi> getResultsByProvince(final String province) {
		return this.chorbiRepository.getResultsByProvince(province);
	}

	public Collection<Chorbi> getResultsByCity(final String city) {
		return this.chorbiRepository.getResultsByCity(city);
	}

	// ***************************************

	// Check if the actual user is a chorbi
	public void checkIfChorbi() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.CHORBI))
				res = true;
		Assert.isTrue(res);
	}

	// Finds the actual chorbi
	public Chorbi findByPrincipal() {
		Chorbi result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.chorbiRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	// Find a chorbi by his or her username
	public Chorbi getChorbiByUsername(final String username) {
		return this.chorbiRepository.findChorbiByUsername(username);
	}

	public Chorbi reconstruct(final ChorbiForm c) {
		final Chorbi chorbi = new Chorbi();

		final Collection<Likes> sentLikes = new ArrayList<>();
		final Collection<Likes> receivedLikes = new ArrayList<>();
		final Collection<Chirp> sentChirps = new ArrayList<>();
		final Collection<Chirp> receivedChirps = new ArrayList<>();
		final SearchTemplate search = new SearchTemplate();
		final UserAccount account = new UserAccount();
		account.setPassword(c.getPassword());
		account.setUsername(c.getUsername());

		final CreditCard creditCard = new CreditCard();

		creditCard.setHolderName(c.getHolderName());
		creditCard.setBrandName(c.getBrandName());
		creditCard.setNumber(c.getNumber());
		creditCard.setExpirationMonth(c.getExpirationMonth());
		creditCard.setExpirationYear(c.getExpirationYear());
		creditCard.setCvvCode(c.getCvvCode());

		chorbi.setId(0);
		chorbi.setVersion(0);
		chorbi.setName(c.getName());
		chorbi.setSurname(c.getSurname());
		chorbi.setPhone(c.getPhone());
		chorbi.setEmail(c.getEmail());
		chorbi.setPicture(c.getPicture());
		chorbi.setDescription(c.getDescription());
		chorbi.setKindOfRelationship(c.getKindOfRelationship());
		chorbi.setBirthDate(c.getBirthDate());
		chorbi.setGenre(c.getGenre());
		chorbi.setBanned(false);
		chorbi.setCountry(c.getCountry());
		chorbi.setState(c.getState());
		chorbi.setProvince(c.getProvince());
		chorbi.setCity(c.getCity());
		chorbi.setUserAccount(account);
		chorbi.setSentLikes(sentLikes);
		chorbi.setReceivedLikes(receivedLikes);
		chorbi.setSentChirps(sentChirps);
		chorbi.setReceivedChirps(receivedChirps);
		chorbi.setCreditCard(creditCard);
		search.setKindOfRelationship(c.getKindOfRelationship());
		search.setChorbi(chorbi);
		search.setEstimatedAge(chorbi.getChorbiAge());
		search.setCity(c.getCity());
		search.setCountry(c.getCountry());
		search.setState(c.getState());
		search.setProvince(c.getProvince());
		search.setGenre(c.getGenre());
		search.setKeyword("");
		chorbi.setSearchTemplate(search);

		Assert.isTrue(c.getPassword().equals(c.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(c.getCheckBox(), "You must accept the term and conditions");

		return chorbi;
	}

	public void saveForm(Chorbi chorbi) {

		Assert.notNull(chorbi);
		Assert.isTrue(ActorService.validateCreditCard(chorbi.getCreditCard()), "La tarjeta de credito no es valida");
		Assert.isTrue(chorbi.getChorbiAge() >= 18, "No puede ser menor de 18 a�os");
		String password;
		final Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		final Collection<Authority> auths = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("CHORBI");
		auths.add(auth);

		password = chorbi.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		chorbi.getUserAccount().setPassword(password);

		chorbi.getUserAccount().setAuthorities(auths);

		chorbi = this.chorbiRepository.saveAndFlush(chorbi);
	}

	// *** para email y phone
	public Collection<Chorbi> getChorbiesWithStars(final Collection<Chorbi> c) {
		final Collection<Chorbi> res = new ArrayList<>();
		for (final Chorbi ch : c)
			res.add(this.createCopyChorbi(ch));
		return res;
	}

	private Chorbi createCopyChorbi(final Chorbi c) {
		final Chorbi res = new Chorbi();
		res.setBanned(c.getBanned());
		res.setBirthDate(c.getBirthDate());
		res.setCity(c.getCity());
		res.setCountry(c.getCountry());
		res.setCreditCard(c.getCreditCard());
		res.setDescription(c.getDescription());
		res.setEmail("***");
		res.setEvents(c.getEvents());
		res.setFees(c.getFees());
		res.setGenre(c.getGenre());
		res.setId(c.getId());
		res.setVersion(c.getVersion());
		res.setKindOfRelationship(c.getKindOfRelationship());
		res.setName(c.getName());
		res.setPhone("***");
		res.setPicture(c.getPicture());
		res.setUserAccount(c.getUserAccount());
		res.setSurname(c.getSurname());
		res.setState(c.getState());
		res.setSentLikes(c.getSentLikes());
		res.setSentChirps(c.getSentChirps());
		res.setSearchTemplate(c.getSearchTemplate());
		res.setReceivedLikes(c.getReceivedLikes());
		res.setReceivedChirps(c.getReceivedChirps());
		res.setProvince(c.getProvince());
		return res;
	}
	// Filtrado de chorbies baneados
	public Collection<Chorbi> filter(final Collection<Chorbi> cs) {
		final Actor actor = this.actorService.findByPrincipal();
		final Collection<Chorbi> res = new ArrayList<>();

		for (final Chorbi b : cs)
			if (!b.getBanned())
				res.add(b);

		if (actor instanceof Chorbi)
			return res;
		else
			return cs;
	}

	// For tests
	public void checkAttributes(final ChorbiForm ch) {
		final Chorbi c = this.reconstruct(ch);
		Assert.isTrue(c.getPhone().matches("(^(\\+\\d{1,3})?\\d{9}$)|(^(\\*\\*\\*)$)"));
		Assert.isTrue(c.getEmail().matches("^[A-Za-z0-9+_.-]+@(.+)$|^(\\*\\*\\*)"));
		Assert.isTrue(c.getPicture().matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"));
		Assert.isTrue(c.getGenre().matches("^(man|woman)$"));
		Assert.isTrue(c.getKindOfRelationship().matches("^(activities|friendship|love)$"));
		Assert.isTrue(c.getChorbiAge() >= 18);
		for (final Actor a : this.actorService.findAll())
			Assert.isTrue(!a.getUserAccount().getUsername().equals(ch.getUsername()));
	}

	// For tests
	public void checkAttributes(final Chorbi c) {
		Assert.isTrue(c.getPhone().matches("(^(\\+\\d{1,3})?\\d{9}$)|(^(\\*\\*\\*)$)"));
		Assert.isTrue(c.getEmail().matches("^[A-Za-z0-9+_.-]+@(.+)$|^(\\*\\*\\*)"));
		Assert.isTrue(c.getPicture().matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"));
		Assert.isTrue(c.getGenre().matches("^(man|woman)$"));
		Assert.isTrue(c.getKindOfRelationship().matches("^(activities|friendship|love)$"));
		Assert.isTrue(c.getChorbiAge() >= 18);
	}

	// Dashboard

	public Collection<Object[]> minMaxAvgAgeOfChorbies() {
		Collection<Object[]> result;
		result = this.chorbiRepository.minMaxAvgAgeOfChorbies();
		return result;
	}

	public Double ratioChorbiesSearchActivities() {
		Double result;
		result = this.chorbiRepository.ratioChorbiesSearchActivities();
		return result;
	}

	public Double ratioChorbiesSearchFriendship() {
		Double result;
		result = this.chorbiRepository.ratioChorbiesSearchFriendship();
		return result;
	}

	public Double ratioChorbiesSearchLove() {
		Double result;
		result = this.chorbiRepository.ratioChorbiesSearchLove();
		return result;
	}

	public Collection<Object[]> minMaxAvgSentChirps() {
		Collection<Object[]> result;
		result = this.chorbiRepository.minMaxAvgSentChirps();
		return result;
	}

	public Collection<Chorbi> chorbiMoreChirpsSent() {
		Collection<Chorbi> result;
		result = this.chorbiRepository.chorbiMoreChirpsSent();
		return result;
	}

	public Collection<Object[]> minMaxAvgReceivedChirps() {
		Collection<Object[]> result;
		result = this.chorbiRepository.minMaxAvgReceivedChirps();
		return result;
	}

	public Collection<Chorbi> chorbiMoreChirpsReceived() {
		Collection<Chorbi> result;
		result = this.chorbiRepository.chorbiMoreChirpsReceived();
		return result;
	}

	public Collection<Chorbi> chorbiesOrdeByNumberOfLikes() {
		Collection<Chorbi> res;
		res = this.chorbiRepository.chorbiesOrdeByNumberOfLikes();
		return res;
	}

	public Collection<Object[]> minMaxAvgLikesPerChorbi() {
		Collection<Object[]> res;
		res = this.chorbiRepository.minMaxAvgLikesPerChorbi();
		return res;
	}

	public Collection<Object[]> numChorbiesPerCountryAndCity() {
		Collection<Object[]> res;
		res = this.chorbiRepository.numChorbiesPerCountryAndCity();
		return res;
	}

	public Double ratioChorbiesInvalidCreditcard() {
		Double res;
		res = this.chorbiRepository.ratioChorbiesInvalidCreditcard();
		return res;
	}

	// Dashboard 2.0
	public Collection<Object[]> listChorbiesOrderByEventsSize() {
		Collection<Object[]> res;
		res = this.chorbiRepository.listChorbiesOrderByEventsSize();
		return res;
	}

	public Collection<Object[]> chorbiesAndAmountDueInFees() {
		Collection<Object[]> res;
		res = this.chorbiRepository.chorbiesAndAmountDueInFees();
		for (int i = 0; i <= 2; i++)
			for (final Object[] o : res)
				if (o[i] == null)
					o[i] = 0;
		return res;
	}

	public Collection<Object[]> chorbiesSortedByStars() {
		Collection<Object[]> res;
		res = this.chorbiRepository.chorbiesSortedByStars();
		for (final Object[] o : res)
			if (o[2] == null)
				o[2] = 0;
		return res;
	}
	public Collection<Object[]> minMaxAvgStarsPerChorbi() {
		Collection<Object[]> res;
		res = this.chorbiRepository.minMaxAvgStarsPerChorbi();
		for (int i = 0; i <= 3; i++)
			for (final Object[] o : res)
				if (o[i] == null)
					o[i] = 0;
		return res;
	}

	// Additional query: Find chorbi by username
	public Chorbi findChorbiByUsername(final String username) {
		return this.chorbiRepository.findChorbiByUsername(username);
	}

	// Additional query: Find chorbi by useraccount
	public Chorbi findChorbiByUserAccount(final UserAccount userAccount) {
		return this.chorbiRepository.findByUserAccount(userAccount);
	}

	//Check chorbi
	public void checkChorbi(final Chorbi chorbi) {
		Boolean result = true;

		if (chorbi.getPicture() == null || chorbi.getDescription() == null || chorbi.getKindOfRelationship() == null || chorbi.getBirthDate() == null || chorbi.getGenre() == null || chorbi.getCreditCard() == null || chorbi.getCountry() == null
			|| chorbi.getCity() == null)
			result = false;

		Assert.isTrue(result);
	}

	public void checkKindOfRelationShip(final String kind) {
		final String regexp = "^(activities|friendship|love)$";
		Assert.isTrue(kind.matches(regexp));
	}

	public void checkGenre(final String genre) {
		final String regexp = "^(man|woman)$";
		Assert.isTrue(genre.matches(regexp));
	}
}
