
package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.SearchTemplateRepository;
import domain.Chorbi;
import domain.CreditCard;
import domain.SearchTemplate;

@Service
@Transactional
public class SearchTemplateService {

	// Managed Repository ---------------------------------------------------------
	@Autowired
	private SearchTemplateRepository	searchTemplateRepository;
	@Autowired
	private SystemConfigurationService	systemConfigurationService;


	// Constructor methods ---------------------------------------------------------
	public SearchTemplateService() {
		super();
	}


	// Supporting services ---------------------------------------------------------
	@Autowired
	private ChorbiService	chorbiService;


	// Necessary Simple CRUD methods ---------------------------------------------------------

	public SearchTemplate findOne(final int searchTemplateId) {
		this.checkIfChorbi();
		Assert.isTrue(searchTemplateId != 0);

		SearchTemplate result;

		result = this.searchTemplateRepository.findOne(searchTemplateId);

		Assert.notNull(result);

		return result;
	}

	public void save(final SearchTemplate searchTemplate) {
		this.chorbiService.checkIfChorbi();
		Assert.notNull(searchTemplate);

		if (this.checkSearchTemplateAttributesForCache(searchTemplate) == false)
			searchTemplate.setLastTimeUsed(null);

		this.searchTemplateRepository.save(searchTemplate);
	}

	public void save2(final SearchTemplate searchTemplate) {
		Assert.notNull(searchTemplate);
		this.searchTemplateRepository.save(searchTemplate);
	}

	// Other bussines methods ---------------------------------------------------------

	// Get found chorbies by the actual searchTemplate
	@SuppressWarnings("deprecation")
	public Collection<Chorbi> getSearchTemplateResults(final SearchTemplate searchTemplate) {
		this.checkIfChorbi();
		Assert.notNull(searchTemplate);

		Collection<Chorbi> searchTemplateResults = new ArrayList<>();

		// Cache time regarding 'cacheTime'

		final Date now = new Date();
		final Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		cal.add(Calendar.HOUR, -this.systemConfigurationService.getCacheTime().getHours());
		cal.add(Calendar.MINUTE, -this.systemConfigurationService.getCacheTime().getMinutes());
		cal.add(Calendar.SECOND, -this.systemConfigurationService.getCacheTime().getSeconds());
		final Date cacheTime = cal.getTime();

		// Check if cache have to be applied or not

		if (searchTemplate.getFoundChorbies() == null || searchTemplate.getLastTimeUsed() == null || searchTemplate.getLastTimeUsed().before(cacheTime)) {

			searchTemplate.setLastTimeUsed(now);
			searchTemplate.setFoundChorbies(this.getFoundChorbies(searchTemplate));
			this.save(searchTemplate);
			searchTemplateResults = searchTemplate.getFoundChorbies();

		} else
			searchTemplateResults = searchTemplate.getFoundChorbies();

		return searchTemplateResults;
	}

	// Auxiliary method for searchTemplate one: finds chorbies by search template attributes
	private Collection<Chorbi> getFoundChorbies(final SearchTemplate searchTemplate) {
		this.chorbiService.checkIfChorbi();

		final Collection<Chorbi> allChorbies = this.chorbiService.findAll();
		final Collection<Chorbi> foundChorbies = new ArrayList<>();

		// Se retienen los chorbies que cumplen con los atributos de la search template. Debe hacerse por
		// separado por temas de 'null' y 'empty'.

		if (!searchTemplate.getKindOfRelationship().isEmpty() && searchTemplate.getKindOfRelationship() != null)
			allChorbies.retainAll(this.chorbiService.getResultsByKinOfRelationship(searchTemplate.getKindOfRelationship()));

		if (searchTemplate.getEstimatedAge() != null)
			allChorbies.retainAll(this.chorbiService.getResultsByEstimatedAge(searchTemplate.getEstimatedAge() - 5, searchTemplate.getEstimatedAge() + 5));

		if (!searchTemplate.getGenre().isEmpty() && searchTemplate.getGenre() != null)
			allChorbies.retainAll(this.chorbiService.getResultsByGenre(searchTemplate.getGenre()));

		if (!searchTemplate.getKeyword().isEmpty() && searchTemplate.getKeyword() != null)
			allChorbies.retainAll(this.chorbiService.getResultsByKeyword(searchTemplate.getKeyword()));

		if (!searchTemplate.getCountry().isEmpty() && searchTemplate.getCountry() != null)
			allChorbies.retainAll(this.chorbiService.getResultsByCountry(searchTemplate.getCountry()));

		if (!searchTemplate.getState().isEmpty() && searchTemplate.getState() != null)
			allChorbies.retainAll(this.chorbiService.getResultsByState(searchTemplate.getState()));

		if (!searchTemplate.getCity().isEmpty() && searchTemplate.getCity() != null)
			allChorbies.retainAll(this.chorbiService.getResultsByCity(searchTemplate.getCity()));

		if (!searchTemplate.getProvince().isEmpty() && searchTemplate.getProvince() != null)
			allChorbies.retainAll(this.chorbiService.getResultsByProvince(searchTemplate.getProvince()));

		foundChorbies.addAll(allChorbies);

		return foundChorbies;
	}

	// Check attributes for search template. If it's true, all are the same. If not, some of them are different and
	// a new cache have to be applied
	public Boolean checkSearchTemplateAttributesForCache(final SearchTemplate st) {
		this.checkIfChorbi();

		Boolean result = false;
		final SearchTemplate mine = this.findChorbiByPrincipal().getSearchTemplate();

		if (st.getCity().equals(mine.getCity()) && st.getCountry().equals(mine.getCountry()) && st.getGenre().equals(mine.getGenre()) && st.getKeyword().equals(mine.getKeyword()) && st.getKindOfRelationship().equals(mine.getKindOfRelationship())
			&& st.getProvince().equals(mine.getProvince()) && st.getState().equals(mine.getState()) && st.getEstimatedAge() == mine.getEstimatedAge())
			result = true;

		return result;
	}

	// Check all attributes of a searchTemplate (for tests' use)
	public void checkAttributesOfSearchTemplate(final SearchTemplate searchTemplate) {
		final String keywordPattern = "^$|\\w+";
		final String genrePattern = "^$|^(man|woman)$";
		final String kindOfRelationShipPattern = "^$|^(activities|friendship|love)$";
		final int minEstimatedAge = 18;

		Assert.isTrue(searchTemplate.getKeyword().matches(keywordPattern));
		Assert.isTrue(searchTemplate.getKindOfRelationship().matches(kindOfRelationShipPattern));
		Assert.isTrue(searchTemplate.getGenre().matches(genrePattern));

		if (searchTemplate.getEstimatedAge() != null)
			Assert.isTrue(searchTemplate.getEstimatedAge() >= minEstimatedAge);
	}

	// Check if it's a chorbi
	public void checkIfChorbi() {
		this.chorbiService.checkIfChorbi();
	}

	// Find actual chorbi
	public Chorbi findChorbiByPrincipal() {
		return this.chorbiService.findByPrincipal();
	}

	// Find chorbi by username
	public Chorbi findChorbyByUsername(final String username) {
		return this.chorbiService.getChorbiByUsername(username);
	}

	// Check if actual chorbi is author of the search template
	public void checkActualChorbiAuthor(final SearchTemplate st) {
		Assert.isTrue(this.findChorbiByPrincipal().getId() == st.getChorbi().getId(), "No eres el autor");
	}

	// Check if creditCard is valid so the searchTemplate can be used
	public void checkCreditCard(final Chorbi chorbi) {
		CreditCard creditCard;

		creditCard = chorbi.getCreditCard();

		Assert.isTrue(ActorService.validateCreditCard(creditCard), "La tarjeta de cr�dito no es v�lida, cambiala antes de continuar");
	}
}
