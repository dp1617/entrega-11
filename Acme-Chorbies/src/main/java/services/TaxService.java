
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.TaxRepository;
import domain.Tax;

@Service
@Transactional
public class TaxService {

	// Managed repository
	@Autowired
	private TaxRepository			taxRepository;

	// Servicios
	@Autowired
	private AdministratorService	adminService;


	// Constructors
	public TaxService() {
		super();
	}

	// Simple CRUD methods

	public Tax create() {
		Tax tax;
		tax = new Tax();
		Assert.notNull(tax);
		return tax;
	}

	public Collection<Tax> findAll() {
		Collection<Tax> taxs;
		taxs = this.taxRepository.findAll();
		Assert.notNull(taxs);
		return taxs;
	}

	public Tax findOne(final int TaxId) {
		Assert.isTrue(TaxId != 0);
		Tax result;
		result = this.taxRepository.findOne(TaxId);
		Assert.notNull(result);
		return result;
	}

	public Tax save(final Tax tax) {
		this.adminService.checkIfAdministrator();
		Tax result;
		Assert.notNull(tax);
		result = this.taxRepository.save(tax);
		return result;
	}

	// Acme-Chorbies 2.0 methods
	// Los siguientes metodos obtienen el valor de la unica entrada que contiene
	// la tabla de esta entidad en la base de datos, unica necesaria para guardar
	// el valor de los Fee, y la convierte a Double, en funcion del que queramos obtener.

	public Double findManagerFeeValue() {
		final Collection<Tax> tax = this.taxRepository.findAll();
		final List<Double> res = new ArrayList<Double>();

		for (final Tax t : tax)
			res.add(t.getManagerValue());

		final Double result = res.get(0);
		return result;
	}

	public Double findChorbiFeeValue() {
		final Collection<Tax> tax = this.taxRepository.findAll();
		final List<Double> res = new ArrayList<Double>();

		for (final Tax t : tax)
			res.add(t.getChorbiValue());

		final Double result = res.get(0);
		return result;
	}
}
