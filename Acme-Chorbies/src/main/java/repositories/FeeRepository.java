
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import domain.Fee;

public interface FeeRepository extends JpaRepository<Fee, Integer> {

}
