
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import domain.Chirp;

public interface ChirpRepository extends JpaRepository<Chirp, Integer> {

}
