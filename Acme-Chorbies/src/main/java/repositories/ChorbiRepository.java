
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import security.UserAccount;
import domain.Chorbi;

@Repository
public interface ChorbiRepository extends JpaRepository<Chorbi, Integer> {

	@Query("select cho from Chorbi cho where cho.userAccount.username = ?1")
	Chorbi findChorbiByUsername(String username);

	@Query("select cho from Chorbi cho where cho.userAccount = ?1")
	Chorbi findByUserAccount(UserAccount userAccount);

	// For search template, It have to be separately queries. For example, when all the fields are "null", there is no chorbi with all null fields
	@Query("select cho from Chorbi cho where cho.kindOfRelationship = ?1")
	Collection<Chorbi> getResultsByKinOfRelationship(String kindOfRelationship);

	@Query("select cho from Chorbi cho where cho.chorbiAge >= ?1 and cho.chorbiAge <= ?2")
	Collection<Chorbi> getResultsByEstimatedAge(int fiveLess, int fiveMore);

	@Query("select cho from Chorbi cho where cho.genre = ?1")
	Collection<Chorbi> getResultsByGenre(String genre);

	@Query("select cho from Chorbi cho where cho.description like %?1% or cho.name like %?1% or cho.surname like %?1%")
	Collection<Chorbi> getResultsByKeyword(String keyword);

	@Query("select cho from Chorbi cho where cho.country = ?1")
	Collection<Chorbi> getResultsByCountry(String country);

	@Query("select cho from Chorbi cho where cho.state = ?1")
	Collection<Chorbi> getResultsByState(String state);

	@Query("select cho from Chorbi cho where cho.province = ?1")
	Collection<Chorbi> getResultsByProvince(String province);

	@Query("select cho from Chorbi cho where cho.city = ?1")
	Collection<Chorbi> getResultsByCity(String city);

	// The minimum, the maximum, and the average ages of the chorbies.
	@Query("select min(cho.chorbiAge), max(cho.chorbiAge), avg(cho.chorbiAge) from Chorbi cho")
	Collection<Object[]> minMaxAvgAgeOfChorbies();

	// The ratios of chorbies who search for "activities".
	@Query("select count(cho1)*1.0/(select count(cho2) from Chorbi cho2) from Chorbi cho1 where cho1.searchTemplate.kindOfRelationship = 0")
	Double ratioChorbiesSearchActivities();

	// The ratios of chorbies who search for "friendship".
	@Query("select count(cho1)*1.0/(select count(cho2) from Chorbi cho2) from Chorbi cho1 where cho1.searchTemplate.kindOfRelationship = 1")
	Double ratioChorbiesSearchFriendship();

	// The ratios of chorbies who search for "love".
	@Query("select count(cho1)*1.0/(select count(cho2) from Chorbi cho2) from Chorbi cho1 where cho1.searchTemplate.kindOfRelationship = 2")
	Double ratioChorbiesSearchLove();

	// The minimum, the maximum, and the average number of chirps that a chorbi sends to other chorbies.
	@Query("select min(c.sentChirps.size),avg(c.sentChirps.size),max(c.sentChirps.size) from Chorbi c")
	Collection<Object[]> minMaxAvgSentChirps();

	// The chorbies who have sent more chirps.
	@Query("select c from Chorbi c where c.sentChirps.size >= (select max(c.sentChirps.size) from Chorbi c)")
	Collection<Chorbi> chorbiMoreChirpsSent();

	// The minimum, the maximum, and the average number of chirps that a chorbi receives from other chorbies.
	@Query("select min(c.receivedChirps.size),avg(c.receivedChirps.size),max(c.receivedChirps.size) from Chorbi c")
	Collection<Object[]> minMaxAvgReceivedChirps();

	// The chorbies who have received more chirps.
	@Query("select c from Chorbi c where c.receivedChirps.size >= (select max(c.receivedChirps.size) from Chorbi c)")
	Collection<Chorbi> chorbiMoreChirpsReceived();

	// All chorbies order by number of likes.
	@Query("select c from Chorbi c order by c.receivedLikes.size")
	Collection<Chorbi> chorbiesOrdeByNumberOfLikes();

	// The minimum, the maximum, and the average number of likes per chorbi.
	@Query("select max(c.receivedLikes.size), min(c.receivedLikes.size), avg(c.receivedLikes.size) from Chorbi c")
	Collection<Object[]> minMaxAvgLikesPerChorbi();

	//A listing with the number of chorbies per country and city.
	@Query("select c.country, c.city, count(c) from Chorbi c group by c.country, c.city")
	Collection<Object[]> numChorbiesPerCountryAndCity();

	//The ratio of chorbies who have not registered a credit card or have registered an invalid credit card.
	@Query("select (count(c)*1.0)/(select count(ch) from Chorbi ch) from Chorbi c where c.creditCard is null")
	Double ratioNotRegisteredCreditCard();

	@Query("select count(s) from Chorbi s join s.creditCard cr where cr.expirationYear <= YEAR(CURRENT_DATE)")
	Double ChorbiesWithInvalidCreditcardYear();

	@Query("select count(s) from Chorbi s join s.creditCard cr where cr.expirationMonth <= MONTH(CURRENT_DATE)")
	Double ChorbiesWithInvalidCreditcardMonth();

	//Todo lo que piden en una query
	@Query("select (count(c)*1.0)/(select count(ch) from Chorbi ch) from Chorbi c where c.creditCard is null OR(c.creditCard.expirationMonth<=MONTH(CURRENT_DATE) AND(c.creditCard.expirationYear<=YEAR(CURRENT_DATE)))")
	Double ratioChorbiesInvalidCreditcard();

	//Querys Acme-Chorbies 2.0

	//A listing of chorbies sorted by the number of events to which they have registered
	@Query("select c.surname, c.name from Chorbi c order by c.events.size")
	Collection<Object[]> listChorbiesOrderByEventsSize();

	//C4: A listing of chorbies that includes the amount that they due in fees
	@Query("select c.surname, c.name, (select sum(f.value) from Chorbi c join c.fees f where f.isPaid = 'false') from Chorbi c")
	Collection<Object[]> chorbiesAndAmountDueInFees();

	// Listing of chorbies, sorted by the average number of stars that they've got.
	@Query("select c.surname, c.name,(select avg(l.numberStars) from Likes l where l.likedChorbi.id=c.id)as stars from Chorbi c order by stars desc")
	Collection<Object[]> chorbiesSortedByStars();

	// The minimum, the maximum, and the average number of stars per chorbi.
	@Query("select c.name,(select min(l.numberStars) from Likes l where l.likedChorbi.id=c.id), (select max(l.numberStars) from Likes l where l.likedChorbi.id=c.id), (select avg(l.numberStars) from Likes l where l.likedChorbi.id=c.id)as stars from Chorbi c")
	Collection<Object[]> minMaxAvgStarsPerChorbi();
}
