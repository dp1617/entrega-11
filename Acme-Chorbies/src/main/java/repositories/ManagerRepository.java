
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import security.UserAccount;
import domain.Manager;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Integer> {

	@Query("select m from Manager m where m.userAccount.username = ?1")
	Manager findManagerByUsername(String username);

	@Query("select m from Manager m where m.userAccount = ?1")
	Manager findByUserAccount(UserAccount userAccount);

	// Chorbies 2.0 ------------------------------------------------

	//A listing of managers sorted by the number of events that they organise.
	@Query("select m.surname, m.name from Manager m order by m.events.size")
	Collection<Object[]> managersSortedByNumberOfEvents();

	//A listing of managers that includes the amount that they due in fees. --------------------
	@Query("select m.surname, m.name, (select sum(f.value) from Manager m join m.fees f where f.isPaid = 'false') from Manager m")
	Collection<Object[]> managersAndAmountDueInFees();

}
