
package controllers.administrator;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.SystemConfigurationService;
import domain.SystemConfiguration;

@Controller
@RequestMapping("/systemConfiguration")
public class SystemConfigurationAdministratorController {

	//Services
	@Autowired
	private SystemConfigurationService	systemConfigurationService;


	// Creation--------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		SystemConfiguration systemConfiguration;

		systemConfiguration = this.systemConfigurationService.getSystemConfiguration();

		res = new ModelAndView("systemConfiguration/edit");
		res.addObject("systemConfiguration", systemConfiguration);

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final SystemConfiguration systemConfiguration, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createEditModelAndView(systemConfiguration);
		else
			try {
				this.systemConfigurationService.save(systemConfiguration);
				res = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(systemConfiguration, "systemConfiguration.commit.error");
				if (oops.getLocalizedMessage().contains("El formato de los banners no es correcto"))
					res = this.createEditModelAndView(systemConfiguration, "systemConfiguration.banners.error");
			}
		return res;
	}
	// Ancillary methods-------------------------------------
	protected ModelAndView createEditModelAndView(final SystemConfiguration systemConfiguration) {
		ModelAndView res;
		res = this.createEditModelAndView(systemConfiguration, null);
		return res;
	}

	protected ModelAndView createEditModelAndView(final SystemConfiguration systemConfiguration, final String message) {
		ModelAndView res;

		res = new ModelAndView("systemConfiguration/edit");
		res.addObject("systemConfiguration", systemConfiguration);
		res.addObject("message", message);

		return res;
	}

}
