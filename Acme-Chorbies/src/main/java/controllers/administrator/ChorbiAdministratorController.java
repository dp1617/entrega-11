
package controllers.administrator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AdministratorService;
import services.ChorbiService;
import controllers.AbstractController;

@Controller
@RequestMapping("/chorbi/administrator")
public class ChorbiAdministratorController extends AbstractController {

	// Services -------------------------------------------

	@Autowired
	private AdministratorService	administratorService;
	@Autowired
	private ChorbiService			chorbiService;


	// Methods --------------------------------------------

	// Ban chorbi
	@RequestMapping(value = "/ban", method = RequestMethod.GET)
	public ModelAndView ban(@RequestParam final int chorbiId) {
		ModelAndView res;

		this.administratorService.ban(this.chorbiService.findOne(chorbiId));

		res = new ModelAndView("redirect:/chorbi/list.do");

		return res;
	}

	// Unban chorbi
	@RequestMapping(value = "/unban", method = RequestMethod.GET)
	public ModelAndView unban(@RequestParam final int chorbiId) {
		ModelAndView res;

		this.administratorService.unban(this.chorbiService.findOne(chorbiId));

		res = new ModelAndView("redirect:/chorbi/list.do");

		return res;
	}
}
