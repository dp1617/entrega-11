
package controllers.administrator;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.TaxService;
import controllers.AbstractController;
import domain.Tax;

@Controller
@RequestMapping("/tax")
public class TaxController extends AbstractController {

	// Services
	@Autowired
	private TaxService	taxService;


	// Constructors
	public TaxController() {
		super();
	}

	// Listar para admin
	@RequestMapping(value = "/administrator/list", method = RequestMethod.GET)
	public ModelAndView listAdmin() {
		ModelAndView result;

		final Collection<Tax> tax = this.taxService.findAll();

		result = new ModelAndView("tax/administrator/list");
		result.addObject("tax", tax);
		result.addObject("requestURI", "tax/administrator/list.do");

		return result;
	}

	// Modificar valor (obtener el tax que buscamos por taxID)
	@RequestMapping(value = "/administrator/modify", method = RequestMethod.GET)
	public ModelAndView modify(@RequestParam final int taxID) {
		ModelAndView res;
		Tax tax;

		tax = this.taxService.findOne(taxID);
		Assert.notNull(tax);
		res = this.createEditModelAndView(tax);
		return res;
	}

	// Guardar valor modificado (guardar tax)
	@RequestMapping(value = "/administrator/modify", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Tax tax, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = this.createEditModelAndView(tax);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.taxService.save(tax);
				res = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(tax, "tax.error");
			}
		return res;
	}

	// Ancillary methods
	protected ModelAndView createEditModelAndView(final Tax tax) {
		ModelAndView result;
		result = this.createEditModelAndView(tax, null);
		return result;
	}

	protected ModelAndView createEditModelAndView(final Tax tax, final String message) {
		ModelAndView result;

		result = new ModelAndView("tax/administrator/modify");
		result.addObject("tax", tax);
		result.addObject("message", message);

		return result;
	}
}
