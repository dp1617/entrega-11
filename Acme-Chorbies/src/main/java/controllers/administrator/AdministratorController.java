/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.ChorbiService;
import services.ManagerService;
import controllers.AbstractController;
import domain.Chorbi;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Services -------------------------------------------

	//@Autowired
	//private AdministratorService	administratorService;

	@Autowired
	private ChorbiService	chorbiService;

	@Autowired
	private ManagerService	managerService;


	// Methods --------------------------------------------

	// Dashboard for administrator

	@RequestMapping("/dashboard")
	public ModelAndView dashboard() {
		final ModelAndView result = new ModelAndView();

		// Level C - Queries

		final Collection<Object[]> numChorbiesPerCountryAndCity;
		final Collection<Object[]> minMaxAvgAgeOfChorbies;
		final Double ratioChorbiesInvalidCreditcard;
		final Double ratioChorbiesSearchActivities;
		final Double ratioChorbiesSearchFriendship;
		final Double ratioChorbiesSearchLove;

		// Level B - Queries

		final Collection<Chorbi> chorbiesOrdeByNumberOfLikes;
		final Collection<Object[]> minMaxAvgLikesPerChorbi;

		// Level A - Queries

		final Collection<Object[]> minMaxAvgReceivedChirps;
		final Collection<Object[]> minMaxAvgSentChirps;
		final Collection<Chorbi> chorbiMoreChirpsReceived;
		final Collection<Chorbi> chorbiMoreChirpsSent;

		// ***Chorbies 2.0 Queries***

		// Level C - Queries

		final Collection<Object[]> managersSortedByNumberOfEvents;
		final Collection<Object[]> managersAndAmountDueInFees;
		final Collection<Object[]> listChorbiesOrderByEventsSize;
		final Collection<Object[]> chorbiesAndAmountDueInFees;

		// Level B - Queries

		Collection<Object[]> minMaxAvgStarsPerChorbi;
		final Collection<Object[]> chorbiesSortedByStars;

		// Initiate variables

		numChorbiesPerCountryAndCity = this.chorbiService.numChorbiesPerCountryAndCity();
		minMaxAvgAgeOfChorbies = this.chorbiService.minMaxAvgAgeOfChorbies();
		ratioChorbiesInvalidCreditcard = this.chorbiService.ratioChorbiesInvalidCreditcard();
		ratioChorbiesSearchActivities = this.chorbiService.ratioChorbiesSearchActivities();
		ratioChorbiesSearchFriendship = this.chorbiService.ratioChorbiesSearchFriendship();
		ratioChorbiesSearchLove = this.chorbiService.ratioChorbiesSearchLove();

		chorbiesOrdeByNumberOfLikes = this.chorbiService.chorbiesOrdeByNumberOfLikes();
		minMaxAvgLikesPerChorbi = this.chorbiService.minMaxAvgLikesPerChorbi();

		minMaxAvgReceivedChirps = this.chorbiService.minMaxAvgReceivedChirps();
		minMaxAvgSentChirps = this.chorbiService.minMaxAvgSentChirps();
		chorbiMoreChirpsReceived = this.chorbiService.chorbiMoreChirpsReceived();
		chorbiMoreChirpsSent = this.chorbiService.chorbiMoreChirpsSent();

		managersSortedByNumberOfEvents = this.managerService.managersSortedByNumberOfEvents();
		managersAndAmountDueInFees = this.managerService.managersAndAmountDueInFees();
		listChorbiesOrderByEventsSize = this.chorbiService.listChorbiesOrderByEventsSize();
		chorbiesAndAmountDueInFees = this.chorbiService.chorbiesAndAmountDueInFees();

		minMaxAvgStarsPerChorbi = this.chorbiService.minMaxAvgStarsPerChorbi();
		chorbiesSortedByStars = this.chorbiService.chorbiesSortedByStars();

		// Adding objects

		result.addObject("numChorbiesPerCountryAndCity", numChorbiesPerCountryAndCity);
		result.addObject("minMaxAvgAgeOfChorbies", minMaxAvgAgeOfChorbies);
		result.addObject("ratioChorbiesInvalidCreditcard", ratioChorbiesInvalidCreditcard);
		result.addObject("ratioChorbiesSearchActivities", ratioChorbiesSearchActivities);
		result.addObject("ratioChorbiesSearchFriendship", ratioChorbiesSearchFriendship);
		result.addObject("ratioChorbiesSearchLove", ratioChorbiesSearchLove);

		result.addObject("chorbiesOrdeByNumberOfLikes", chorbiesOrdeByNumberOfLikes);
		result.addObject("minMaxAvgLikesPerChorbi", minMaxAvgLikesPerChorbi);

		result.addObject("minMaxAvgReceivedChirps", minMaxAvgReceivedChirps);
		result.addObject("minMaxAvgSentChirps", minMaxAvgSentChirps);
		result.addObject("chorbiMoreChirpsReceived", chorbiMoreChirpsReceived);
		result.addObject("chorbiMoreChirpsSent", chorbiMoreChirpsSent);

		result.addObject("managersSortedByNumberOfEvents", managersSortedByNumberOfEvents);
		result.addObject("managersAndAmountDueInFees", managersAndAmountDueInFees);
		result.addObject("listChorbiesOrderByEventsSize", listChorbiesOrderByEventsSize);
		result.addObject("chorbiesAndAmountDueInFees", chorbiesAndAmountDueInFees);

		result.addObject("minMaxAvgStarsPerChorbi", minMaxAvgStarsPerChorbi);
		result.addObject("chorbiesSortedByStars", chorbiesSortedByStars);

		return result;
	}
}
