
package controllers.administrator;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ManagerService;
import domain.Manager;
import forms.ManagerForm;

@Controller
@RequestMapping("/manager/administrator")
public class ManagerAdministratorController {

	// Services ------------------------------------------------------------------
	@Autowired
	private ManagerService	managerService;


	// Methods ------------------------------------------------------------------

	// (REGISTRO) Creaci�n de manager
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final ManagerForm managerForm = new ManagerForm();

		res = this.createFormModelAndView(managerForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo manager
	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final ManagerForm managerForm, final BindingResult binding) {
		ModelAndView res;
		Manager manager;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(managerForm);
			System.out.println(binding.getAllErrors());
		} else
			try {

				manager = this.managerService.reconstruct(managerForm);
				Assert.isTrue(ActorService.validateCreditCard(manager.getCreditCard()), "No es valida");
				this.managerService.saveForm(manager);
				res = new ModelAndView("redirect:/");

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(managerForm);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("El formato del tel�fono es incorrecto") || oops.getLocalizedMessage().contains("The format of the phone is incorrect"))
					res.addObject("phoneError", "phoneError");
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					res.addObject("duplicate", "duplicate");
				if (oops.getLocalizedMessage().contains("You must accept the term and conditions"))
					res.addObject("terms", "terms");
				if (oops.getLocalizedMessage().contains("Passwords do not match"))
					res.addObject("passwordMatch", "pass");
				if (oops.getLocalizedMessage().contains("No es valida"))
					res = this.createFormModelAndView(managerForm, "manager.error.creditCard");
			}

		return res;
	}

	// Auxiliary methods ------------------------------------------------------------------

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final ManagerForm managerForm) {
		ModelAndView res;

		res = this.createFormModelAndView(managerForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final ManagerForm managerForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("manager/administrator/register");
		res.addObject("managerForm", managerForm);
		res.addObject("message", message);

		return res;
	}

}
