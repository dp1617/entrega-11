
package controllers.administrator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ChorbiService;
import services.FeeService;
import services.TaxService;
import controllers.AbstractController;
import domain.Chorbi;
import domain.Fee;

@Controller
@RequestMapping("/fee")
public class FeeAdministratorController extends AbstractController {

	// Services
	@Autowired
	private FeeService		feeService;
	@Autowired
	private ChorbiService	chorbiService;
	@Autowired
	private TaxService		taxService;


	// Constructors
	public FeeAdministratorController() {
		super();
	}

	// Listar fee de chorbies (solo muestra los fees que no contengan el campo chorbi a null)
	@RequestMapping(value = "/administrator/list", method = RequestMethod.GET)
	public ModelAndView listAdmin() {
		ModelAndView result;

		final Collection<Fee> aux = this.feeService.findAll();
		final List<Fee> fee = new ArrayList<Fee>();

		for (final Fee a : aux)
			if (a.getChorbi() != null)
				fee.add(a);

		result = new ModelAndView("fee/administrator/list");
		result.addObject("fee", fee);
		result.addObject("requestURI", "fee/administrator/list.do");

		return result;
	}

	// Marcar como pagado un fee por un administrador
	@RequestMapping(value = "/administrator/updateChorbiMonthlyFees", method = RequestMethod.GET)
	public ModelAndView updateChorbiMonthlyFees() {
		ModelAndView result;

		final Collection<Chorbi> chorbies = this.chorbiService.findAll();
		for (final Chorbi c : chorbies)
			if (c.getFees() == null || c.getFees().isEmpty()) {
				final Fee initialFee = this.feeService.createChorbi(c);
				initialFee.setValue(this.taxService.findChorbiFeeValue());
				this.feeService.save(initialFee);
			} else {
				final Collection<Fee> monthlyFees = c.getFees();

				for (final Fee f : monthlyFees) {
					Double d = f.getValue();
					d += this.taxService.findChorbiFeeValue();
					f.setValue(d);

					this.feeService.save(f);
				}
			}

		result = new ModelAndView("redirect:list.do");
		return result;
	}

	// Ancillary methods
	protected ModelAndView createEditModelAndView(final Fee fee) {
		ModelAndView result;
		result = this.createEditModelAndView(fee, null);
		return result;
	}

	protected ModelAndView createEditModelAndView(final Fee fee, final String message) {
		ModelAndView result;

		result = new ModelAndView("fee/create");
		result.addObject("fee", fee);
		result.addObject("message", message);

		return result;
	}
}
