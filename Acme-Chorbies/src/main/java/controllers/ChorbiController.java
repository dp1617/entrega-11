
package controllers;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ChorbiService;
import domain.Actor;
import domain.Chorbi;
import forms.ChorbiForm;

@Controller
@RequestMapping("/chorbi")
public class ChorbiController extends AbstractController {

	// Services -------------------------------------------

	@Autowired
	private ChorbiService	chorbiService;
	@Autowired
	private ActorService	actorService;


	// Listado de chirps recibidos
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final ModelAndView res;
		Actor actor;
		Collection<Chorbi> result = new ArrayList<>();
		final Collection<Chorbi> chorbies = this.chorbiService.findAll();

		actor = this.actorService.findByPrincipal();

		if (actor instanceof Chorbi)
			chorbies.remove(actor);

		result = this.chorbiService.getChorbiesWithStars(chorbies);

		res = new ModelAndView("chorbi/list");
		res.addObject("chorbies", result);
		if (actor instanceof Chorbi)
			res.addObject("toIterate", this.chorbiService.findByPrincipal().getSentLikes());
		res.addObject("requestURI", "chorbi/list.do");

		return res;
	}

	// (REGISTRO) Creaci�n de un chorbi
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final ChorbiForm chorbiForm = new ChorbiForm();

		res = this.createFormModelAndView(chorbiForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo chorbi
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final ChorbiForm chorbiForm, final BindingResult binding) {
		ModelAndView res;
		Chorbi chorbi;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(chorbiForm);
			System.out.println(binding.getAllErrors());
		} else
			try {

				chorbi = this.chorbiService.reconstruct(chorbiForm);
				this.chorbiService.saveForm(chorbi);
				res = new ModelAndView("redirect:/security/login.do");

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(chorbiForm);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("El formato del tel�fono es incorrecto") || oops.getLocalizedMessage().contains("The format of the phone is incorrect"))
					res.addObject("phoneError", "phoneError");
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					res.addObject("duplicate", "duplicate");
				if (oops.getLocalizedMessage().contains("You must accept the term and conditions"))
					res.addObject("terms", "terms");
				if (oops.getLocalizedMessage().contains("Passwords do not match"))
					res.addObject("passwordMatch", "pass");
				if (oops.getLocalizedMessage().contains("La tarjeta de credito no es valida"))
					res = this.createFormModelAndView(chorbiForm, "chorbi.error.creditCard");
				if (oops.getLocalizedMessage().contains("No puede ser menor"))
					res = this.createFormModelAndView(chorbiForm, "chorbi.age.error");
			}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView save() {
		ModelAndView result;
		Chorbi chorbi;

		chorbi = this.chorbiService.findByPrincipal();

		result = this.createEditModelAndView(chorbi);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Chorbi chorbi, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = this.createEditModelAndView(chorbi);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.chorbiService.save(chorbi);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(chorbi, "chorbi.commit.error");
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("La tarjeta de credito no es valida"))
					result = this.createEditModelAndView(chorbi, "chorbi.error.creditCard");
				if (oops.getLocalizedMessage().contains("No puede ser menor"))
					result = this.createEditModelAndView(chorbi, "chorbi.age.error");

			}
		return result;
	}

	//Ancillary methods

	protected ModelAndView createEditModelAndView(final Chorbi chorbi) {
		ModelAndView result;

		result = this.createEditModelAndView(chorbi, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Chorbi chorbi, final String message) {
		ModelAndView result;

		result = new ModelAndView("chorbi/edit");

		result.addObject("chorbi", chorbi);
		result.addObject("message", message);

		return result;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final ChorbiForm chorbiForm) {
		ModelAndView res;

		res = this.createFormModelAndView(chorbiForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final ChorbiForm chorbiForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("chorbi/create");
		res.addObject("chorbiForm", chorbiForm);
		res.addObject("message", message);

		return res;
	}

}
