
package controllers;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ChirpService;
import services.ChorbiService;
import services.EventService;
import domain.Actor;
import domain.Chirp;
import domain.Chorbi;
import domain.Event;

@Controller
@RequestMapping("/chirp")
public class ChirpController extends AbstractController {

	// Services
	@Autowired
	private ChorbiService	chorbiService;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private ChirpService	chirpService;
	@Autowired
	private EventService	eventService;


	// Methods
	// Listado de chirps recibidos
	@RequestMapping(value = "/received", method = RequestMethod.GET)
	public ModelAndView received() {
		ModelAndView res;
		Chorbi chorbi;
		Collection<Chirp> receivedChirps = new ArrayList<>();

		chorbi = this.chorbiService.findByPrincipal();
		receivedChirps = chorbi.getReceivedChirps();

		res = new ModelAndView("chirp/received");
		res.addObject("receivedChirps", receivedChirps);
		res.addObject("requestURI", "chirp/received.do");

		return res;
	}

	// Borrar chirp recibido
	@RequestMapping(value = "/deleteReceived", method = RequestMethod.GET)
	public ModelAndView deleteReceived(@RequestParam final int chirpId) {
		ModelAndView result;
		Chirp chirp;

		chirp = this.chirpService.findOne(chirpId);
		try {

			this.chirpService.deleteReceived(chirp);
		} catch (final Throwable th) {

		}

		result = new ModelAndView("redirect:/chirp/received.do");

		return result;
	}

	// Listado de chirps enviados
	@RequestMapping(value = "/sent", method = RequestMethod.GET)
	public ModelAndView sent() {
		ModelAndView res;
		Actor actor;
		Collection<Chirp> sentChirps = new ArrayList<>();

		actor = this.actorService.findByPrincipal();
		sentChirps = actor.getSentChirps();

		res = new ModelAndView("chirp/sent");
		res.addObject("sentChirps", sentChirps);
		res.addObject("requestURI", "chirp/sent.do");

		return res;
	}

	// Borrar chirp enviado
	@RequestMapping(value = "/deleteSent", method = RequestMethod.GET)
	public ModelAndView deleteSent(@RequestParam final int chirpId) {
		ModelAndView result;
		Chirp chirp;

		chirp = this.chirpService.findOne(chirpId);
		try {
			this.chirpService.deleteSent(chirp);
		} catch (final Throwable th) {

		}

		result = new ModelAndView("redirect:/chirp/sent.do");

		return result;
	}

	// Creaci�n de chirps
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		Chirp chirp;
		chirp = this.chirpService.create();
		res = this.createCreateModelAndView(chirp);
		return res;
	}

	// Post chirp
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Chirp chirp, final BindingResult binding) {
		ModelAndView result;
		final Chirp m;

		if (binding.hasErrors()) {
			result = this.createCreateModelAndView(chirp);
			System.out.println(binding.getAllErrors());
		} else
			try {
				m = this.chirpService.reconstruct(chirp);
				this.chirpService.save(m);
				result = new ModelAndView("redirect:/chirp/sent.do");
			} catch (final Throwable oops) {
				result = this.createCreateModelAndView(chirp, "chirp.commit.error");
			}
		return result;
	}

	// Reply chirp
	@RequestMapping(value = "/reply", method = RequestMethod.GET)
	public ModelAndView reply(@Valid final int chirpId) {
		ModelAndView res;
		Chirp mess;

		mess = this.chirpService.reply(chirpId);
		res = new ModelAndView("chirp/reply");

		Collection<Chorbi> recipients = new ArrayList<>();
		recipients = this.chirpService.getRecipients();

		res.addObject("mess", mess);
		res.addObject("recipients", recipients);
		res.addObject("requestURI", "chirp/reply.do");
		return res;
	}

	// Forward chirp
	@RequestMapping(value = "/forward", method = RequestMethod.GET)
	public ModelAndView forward(@Valid final int chirpId) {
		ModelAndView res;
		Chirp mess;

		Collection<Chorbi> recipients = new ArrayList<>();
		recipients = this.chirpService.getRecipients();

		mess = this.chirpService.forward(chirpId);

		res = new ModelAndView("chirp/forward");

		res.addObject("recipients", recipients);
		res.addObject("mess", mess);
		res.addObject("requestURI", "chirp/forward.do");
		return res;
	}


	int	aux	= 0;


	// Broadcast event
	@RequestMapping(value = "/broadcast", method = RequestMethod.GET)
	public ModelAndView broadcast(@RequestParam final int eventId) {
		ModelAndView res;
		Chirp mess;

		this.aux = eventId;

		mess = this.chirpService.create();

		res = new ModelAndView("chirp/broadcast");

		res.addObject("mess", mess);
		res.addObject("requestURI", "chirp/broadcast.do");
		return res;
	}

	// Post broadcast
	@RequestMapping(value = "/broadcast", method = RequestMethod.POST, params = "sendBroadcast")
	public ModelAndView sendBroadcast(@Valid final Chirp chirp, final BindingResult binding) {
		ModelAndView result;
		final Event event = this.eventService.findOne(this.aux);

		if (binding.hasErrors()) {
			result = new ModelAndView("chirp/broadcast");

			result.addObject("mess", chirp);
			result.addObject("requestURI", "chirp/broadcast.do");
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.chirpService.broadcast(chirp, event);
				result = new ModelAndView("redirect:/chirp/sent.do");
			} catch (final Throwable oops) {
				result = new ModelAndView("chirp/broadcast");

				result.addObject("mess", chirp);
				result.addObject("requestURI", "chirp/broadcast.do");
				System.out.println(oops.getLocalizedMessage());
			}
		return result;
	}

	// M�todos auxiliares
	protected ModelAndView createCreateModelAndView(final Chirp chirp) {
		ModelAndView res;
		res = this.createCreateModelAndView(chirp, null);
		return res;
	}

	protected ModelAndView createCreateModelAndView(final Chirp mess, final String message) {
		ModelAndView res;
		Collection<Chorbi> recipients = new ArrayList<>();

		recipients = this.chirpService.getRecipients();

		res = new ModelAndView("chirp/create");

		res.addObject("recipients", recipients);
		res.addObject("mess", mess);
		res.addObject("message", message);

		return res;
	}

}
