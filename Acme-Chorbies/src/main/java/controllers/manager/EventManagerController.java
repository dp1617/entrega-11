
package controllers.manager;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.EventService;
import controllers.AbstractController;
import domain.Event;
import domain.Manager;

@Controller
@RequestMapping("/event/manager")
public class EventManagerController extends AbstractController {

	@Autowired
	private EventService	eventService;


	// Methods -------------------------------------------------------------------------

	// List
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final ModelAndView res;
		Manager manager;
		Collection<Event> events = new ArrayList<>();

		manager = this.eventService.findManagerByPrincipal();
		events = manager.getEvents();

		this.eventService.saveType(events);

		res = new ModelAndView("event/manager/list");
		res.addObject("events", events);
		res.addObject("requestURI", "event/manager/list.do");

		return res;
	}

	// Create
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		Event event;

		event = this.eventService.create();
		res = this.createCreateModelAndView(event);

		return res;
	}
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Event event, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createCreateModelAndView(event);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.eventService.saveCreate(event);

				//Charging a fee to the manager who created this event

				res = new ModelAndView("redirect:/event/manager/list.do");

			} catch (final Throwable oops) {
				System.out.println(oops);
				if (oops.getLocalizedMessage().contains("La fecha es incorrecta"))
					res = this.createCreateModelAndView(event, "event.organisedDate.error");
				if (oops.getLocalizedMessage().contains("La tarjeta de credito no es valida"))
					res = this.createCreateModelAndView(event, "event.creditCard.error");

				else
					res = this.createCreateModelAndView(event, "event.commit.error");
			}

		return res;
	}
	// Edit
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int eventId) {
		ModelAndView res;
		Event event;

		event = this.eventService.findOne(eventId);
		res = this.createEditModelAndView(event);

		return res;
	}
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Event event, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(event);
			System.out.println(binding.getAllErrors());
		} else
			try {

				this.eventService.save(event);
				res = new ModelAndView("redirect:/event/manager/list.do");

			} catch (final Throwable oops) {
				System.out.println(oops);
				if (oops.getLocalizedMessage().contains("La fecha es incorrecta"))
					res = this.createEditModelAndView(event, "event.organisedDate.error");
				if (oops.getLocalizedMessage().contains("Ya has creado uno igual"))
					res = this.createEditModelAndView(event, "event.unique.error");
				else
					res = this.createEditModelAndView(event, "event.commit.error");
			}

		return res;
	}
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView deleteEdit(@Valid final Event event, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors())
			res = this.createEditModelAndView(event);
		else
			try {

				this.eventService.delete(event);
				res = new ModelAndView("redirect:/event/manager/list.do");

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createEditModelAndView(event, "event.commit.error");
			}

		return res;
	}

	// Ancillary methods ---------------------------------------------------------------

	protected ModelAndView createCreateModelAndView(final Event event) {
		ModelAndView result;

		result = this.createCreateModelAndView(event, null);

		return result;
	}
	protected ModelAndView createCreateModelAndView(final Event event, final String message) {
		ModelAndView result;

		result = new ModelAndView("event/manager/create");

		result.addObject("event", event);
		result.addObject("message", message);

		result.addObject("requestURI", "event/manager/create.do");

		return result;
	}

	protected ModelAndView createEditModelAndView(final Event event) {
		ModelAndView result;

		result = this.createEditModelAndView(event, null);

		return result;
	}
	protected ModelAndView createEditModelAndView(final Event event, final String message) {
		ModelAndView result;
		Boolean aux = true;

		result = new ModelAndView("event/manager/edit");

		if (event.getType().equals("pastEvent"))
			aux = false;

		result.addObject("event", event);
		result.addObject("message", message);
		result.addObject("status", aux);
		result.addObject("requestURI", "event/manager/edit.do");

		return result;
	}

}
