
package controllers.manager;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ManagerService;
import controllers.AbstractController;
import domain.Manager;

@Controller
@RequestMapping("/manager")
public class ManagerController extends AbstractController {

	// Services -------------------------------------------

	@Autowired
	private ManagerService	managerService;


	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView save() {
		ModelAndView result;
		Manager manager;

		manager = this.managerService.findByPrincipal();

		result = this.createEditModelAndView(manager);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Manager manager, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = this.createEditModelAndView(manager);
			System.out.println(binding.getAllErrors());
		} else
			try {
				Assert.isTrue(ActorService.validateCreditCard(manager.getCreditCard()), "No es valida");
				this.managerService.saveForManager(manager);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(manager);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("El formato del tel�fono es incorrecto") || oops.getLocalizedMessage().contains("The format of the phone is incorrect"))
					result.addObject("phoneError", "phoneError");
				if (oops.getLocalizedMessage().contains("No es valida"))
					result = this.createEditModelAndView(manager, "manager.commit.error");
			}
		return result;
	}

	//Ancillary methods

	protected ModelAndView createEditModelAndView(final Manager manager) {
		ModelAndView result;

		result = this.createEditModelAndView(manager, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Manager manager, final String message) {
		ModelAndView result;

		result = new ModelAndView("manager/edit");

		result.addObject("manager", manager);
		result.addObject("message", message);

		return result;
	}

}
