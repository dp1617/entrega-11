
package controllers.manager;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ChorbiService;
import services.EventService;
import controllers.AbstractController;
import domain.Chorbi;

@Controller
@RequestMapping("/chorbi/manager")
public class ChorbiManagerController extends AbstractController {

	// Services -------------------------------------------

	@Autowired
	private ChorbiService	chorbiService;
	@Autowired
	private EventService	eventService;


	// List de chorbies participantes
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam final int eventId) {
		final ModelAndView res;
		Collection<Chorbi> result = new ArrayList<>();
		Collection<Chorbi> chorbies = new ArrayList<>();

		chorbies = this.eventService.findOne(eventId).getChorbies();
		result = this.chorbiService.getChorbiesWithStars(chorbies);

		res = new ModelAndView("chorbi/manager/list");
		res.addObject("chorbies", result);
		res.addObject("requestURI", "chorbi/manager/list.do");

		return res;
	}

}
