
package controllers.chorbi;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Actor;
import domain.Chorbi;
import domain.Likes;
import services.ActorService;
import services.ChorbiService;
import services.LikesService;

@Controller
@RequestMapping("/likes/chorbi")
public class LikesChorbiController extends AbstractController {

	// Services -------------------------------------------

	@Autowired
	private LikesService	likesService;
	@Autowired
	private ChorbiService	chorbiService;
	@Autowired
	private ActorService	actorService;


	// Crear un like
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int chorbiId) {
		ModelAndView res;
		Likes likes;
		Chorbi chorbi;

		chorbi = this.chorbiService.findOne(chorbiId);
		likes = this.likesService.create(chorbi);

		res = this.createModelAndView(likes);

		return res;
	}

	// Guardar
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Likes likes, final BindingResult binding, final HttpServletRequest request) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(likes);
			System.out.println(binding.getAllErrors());
		} else
			try {
				if (likes.getNumberStars() == null) {
					likes.setNumberStars(0);
				}
				this.likesService.sendLike(likes);

				if (request.getHeader("referer").contains("foundChorbies"))
					res = new ModelAndView("redirect:/chorbi/chorbi/foundChorbies.do");
				else
					res = new ModelAndView("redirect:/chorbi/list.do");

			} catch (final Throwable oops) {
				res = this.createModelAndView(likes, "like.error");
			}

		return res;
	}
	// Guardar en la base de datos el nuevo like
	@RequestMapping(value = "/cancelLikes", method = RequestMethod.GET)
	public ModelAndView cancelLikes(@RequestParam final int chorbiId, final HttpServletRequest request) {
		ModelAndView res;
		Chorbi me;
		Chorbi cancelChorbi;

		Likes like = null;

		me = this.chorbiService.findByPrincipal();
		cancelChorbi = this.chorbiService.findOne(chorbiId);

		for (final Likes l : me.getSentLikes())
			if (l.getLikedChorbi().getId() == cancelChorbi.getId())
				like = l;

		this.likesService.checkLike2(like);

		this.likesService.delete(like);

		if (request.getHeader("referer").contains("foundChorbies"))
			res = new ModelAndView("redirect:/chorbi/chorbi/foundChorbies.do");
		else
			res = new ModelAndView("redirect:/chorbi/list.do");

		return res;
	}
	
	// Show likes
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView showLikingChorbis() {
		ModelAndView result;
		
		try {
			Collection<Likes> likes = new ArrayList<>();
			Chorbi chorbi = chorbiService.findByPrincipal();
			
			likes = chorbi.getReceivedLikes();
			Actor actor = actorService.findByPrincipal();
			this.chorbiService.checkIfChorbi();
			Assert.isTrue(ActorService.validateCreditCard(chorbi.getCreditCard()), "<spring:message code='chorbi.genre'/>");

			result = new ModelAndView("likes/chorbi/list");
			result.addObject("likes", likes);
			if (actor instanceof Chorbi)
				result.addObject("toIterate", this.chorbiService.findByPrincipal().getSentLikes());
			result.addObject("requestURI", "likes/chorbi/list.do");

		} catch (final Throwable oops) {
			result = this.createModelAndView("creditCardError.error");
		}

		

		return result;
	}

	//Ancillary methods

	protected ModelAndView createModelAndView(final Likes like) {
		ModelAndView result;

		result = this.createModelAndView(like, null);

		return result;
	}
	protected ModelAndView createModelAndView(final Likes likes, final String message) {
		ModelAndView result;

		result = new ModelAndView("chorbi/like/create");

		result.addObject("likes", likes);
		result.addObject("message", message);

		return result;
	}
	
	//Ancillary methods

	protected ModelAndView createModelAndView(final String message) {
		ModelAndView result;

		result = new ModelAndView("welcome/index");

		result.addObject("creditCardError",message);

		return result;
	}
}
