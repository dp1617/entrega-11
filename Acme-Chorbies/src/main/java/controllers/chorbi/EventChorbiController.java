
package controllers.chorbi;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.EventService;
import controllers.AbstractController;
import domain.Chorbi;
import domain.Event;

@Controller
@RequestMapping("/event/chorbi")
public class EventChorbiController extends AbstractController {

	@Autowired
	private EventService	eventService;


	// Methods -------------------------------------------------------------------------

	// List
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final ModelAndView res;
		Chorbi chorbi;
		Collection<Event> events = new ArrayList<>();

		chorbi = this.eventService.findChorbiByPrincipal();
		events = chorbi.getEvents();
		res = new ModelAndView("event/chorbi/list");
		res.addObject("events", events);
		res.addObject("requestURI", "event/chorbi/list.do");

		return res;
	}

	// UnRegister --------------------------------------------------------
	@RequestMapping(value = "/unregister", method = RequestMethod.GET)
	public ModelAndView unregister(@RequestParam final int eventID) {
		ModelAndView res;
		final Event e = this.eventService.findOne2(eventID);

		this.eventService.getUnRegisterEvent(e);
		res = new ModelAndView("redirect:/event/chorbi/list.do");
		return res;
	}
	// Register --------------------------------------------------------
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView register(@RequestParam final int eventID) {
		ModelAndView res;
		final Event e = this.eventService.findOne2(eventID);

		this.eventService.getRegisterEvent(e);
		res = new ModelAndView("redirect:/event/chorbi/list.do");
		return res;
	}
}
