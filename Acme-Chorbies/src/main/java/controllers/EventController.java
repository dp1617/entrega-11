
package controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ChorbiService;
import services.EventService;
import domain.Event;

@Controller
@RequestMapping("/event")
public class EventController extends AbstractController {

	// Services -------------------------------------------

	@Autowired
	private EventService	eventService;
	@Autowired
	private ChorbiService	chorbiService;
	@Autowired
	private ActorService	actorService;


	// List de todos
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final ModelAndView res;
		Collection<Event> events = new ArrayList<>();

		events = this.eventService.findAll();

		this.eventService.saveType(events);

		res = new ModelAndView("event/list");
		res.addObject("events", events);
		res.addObject("requestURI", "event/list.do");
		if (this.actorService.authenticated())
			res.addObject("chorbi", this.chorbiService.findByPrincipal());

		return res;
	}

	// List específico
	@RequestMapping(value = "/specialList", method = RequestMethod.GET)
	public ModelAndView specialList() {
		final ModelAndView res;
		Collection<Event> events = new ArrayList<>();

		events = this.eventService.findSpecials();

		this.eventService.saveType(events);

		res = new ModelAndView("event/specialList");
		res.addObject("events", events);
		res.addObject("requestURI", "event/specialList");
		if (this.actorService.authenticated())
			res.addObject("chorbi", this.chorbiService.findByPrincipal());

		return res;
	}

}
