
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Chorbi;

@Component
@Transactional
public class ChorbiToStringConverter implements Converter<Chorbi, String> {

	@Override
	public String convert(final Chorbi chorbi) {
		String res;

		if (chorbi == null)
			res = null;
		else
			res = String.valueOf(chorbi.getId());

		return res;

	}

}
