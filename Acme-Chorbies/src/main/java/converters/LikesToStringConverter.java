
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Likes;

@Component
@Transactional
public class LikesToStringConverter implements Converter<Likes, String> {

	@Override
	public String convert(final Likes likes) {
		String res;

		if (likes == null)
			res = null;
		else
			res = String.valueOf(likes.getId());

		return res;

	}

}
