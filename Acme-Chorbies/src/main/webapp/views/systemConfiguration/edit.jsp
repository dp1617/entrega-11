<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="systemConfiguration/edit.do" modelAttribute="systemConfiguration">

	<!-- atributos -->

	<form:hidden path="id" />
	<form:hidden path="version" />
		

	<acme:textbox code="systemConfiguration.banners" path="banners"/>	
	<acme:textbox code="systemConfiguration.cacheTime" path="cacheTime" placeholder="HH:mm:ss"/>

	<br />
	
	<!-- botones -->
	<acme:submit name="save" code="systemConfiguration.save" />

	<acme:cancel url="" code="systemConfiguration.cancel" />

</form:form>
