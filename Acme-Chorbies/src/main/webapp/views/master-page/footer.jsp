<%--
 * footer.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:useBean id="date" class="java.util.Date" />

<hr />

<b>Copyright &copy; <fmt:formatDate value="${date}" pattern="yyyy" />
	Acme Chorbies Co., Inc.
</b>

<p style="text-align: center">
	<a href="misc/terms.do"><spring:message code="master.page.law" /></a>
</p>
<p style="text-align: center">
	<a href="misc/aboutUs.do"><spring:message code="master.page.about" /></a>
</p>

<spring:message code='master.page.cookies' var="text"
	javaScriptEscape="true" />

<!-- Script for cookies (dissapear when click in cross) -->

<script type="text/javascript">
	function closeCookies() {

		localStorage.setItem("cw_Enabled", 'falso');
		window.location.reload();

	}

	var value = localStorage.getItem("cw_Enabled");

	if (value != 'falso') {
		document
				.write("<div class='cookiePopUp'><div class='cookies_close'><a href='javascript:closeCookies()'><img src='images/arrow_off.png' alt='Close' border='0' /></a></div><p class='pCookiesText'>${text}</p></div>");
	}
</script>


<!-- Script for acme:cancel -->

<script type="text/javascript">
	function relativeRedir(loc) {
		var b = document.getElementsByTagName('base');
		if (b && b[0] && b[0].href) {
			if (b[0].href.substr(b[0].href.length - 1) == '/' && loc.charAt(0) == '/')
				loc = loc.substr(1);
			loc = b[0].href + loc;
		}
		window.location.replace(loc);
	}
</script>