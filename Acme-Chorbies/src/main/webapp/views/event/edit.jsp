<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${requestURI}" modelAttribute="event">

	<jstl:set var="aux" value="${status}" />

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="chorbies" />
	<form:hidden path="manager" />
	<jstl:if test="${requestURI.contains('edit')}">
		<form:hidden path="seats" />
	</jstl:if>
	<form:hidden path="freeSeats" />
	<form:hidden path="type" />
	<jstl:if test="${event.type == 'pastEvent'}">
		<form:hidden path="organisedMoment" />
	</jstl:if>

	<acme:textbox code="event.title" path="title" />
	<jstl:if test="${event.type != 'pastEvent'}">
		<acme:textbox code="event.organisedMoment" path="organisedMoment"
			placeholder="dd/MM/yyyy HH:mm" />
	</jstl:if>
	<acme:textbox code="event.description" path="description" />
	<acme:textbox code="event.picture" path="picture" />

	<jstl:if test="${requestURI.contains('create')}">
		<acme:textbox code="event.seats" path="seats" />
	</jstl:if>

	<acme:submit name="save" code="event.manager.save" />

	<jstl:if test="${requestURI.contains('edit')}">
		<acme:submit name="delete" code="event.manager.delete" />
	</jstl:if>

	<acme:cancel url="event/manager/list.do" code="event.manager.cancel" />

</form:form>
