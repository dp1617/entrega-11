<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="events" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<!-- Este listado es para anonymous y chorbi -->

	<jstl:if
		test="${!requestURI.contains('manager') && !requestURI.contains('chorbi')}">

		<jstl:set var="rowColor" value="none" />

		<jstl:if test="${!requestURI.contains('specialList')}">

			<jstl:if test="${row.type.equals('oneMonthAndSeats')}">
				<jstl:set var="rowColor" value="background-color:#800080" />
			</jstl:if>
			<jstl:if test="${row.type.equals('pastEvent')}">
				<jstl:set var="rowColor" value="background-color:#808080" />
			</jstl:if>

		</jstl:if>

		<spring:message code="event.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}"
			sortable="true" style="${rowColor}" />

		<spring:message code="event.organisedMoment"
			var="organisedMomentHeader" />
		<display:column property="organisedMoment" style="${rowColor}"
			title="${organisedMomentHeader}" sortable="true"
			sortProperty="organisedMoment" format="{0,date,dd/MM/YYYY HH:mm}" />

		<spring:message code="event.description" var="descriptionHeader" />
		<display:column property="description" title="${descriptionHeader}"
			style="${rowColor}" />

		<spring:message code="event.picture" var="pictureHeader" />
		<display:column title="${pictureHeader}" style="${rowColor}">
			<IMG src="${row.picture}" width="100" height="100">
		</display:column>

		<spring:message code="event.seats" var="seatsHeader" />
		<display:column property="seats" title="${seatsHeader}"
			style="${rowColor}" sortable="true" />

		<spring:message code="event.freeSeats" var="freeSeatsHeader" />
		<display:column property="freeSeats" title="${freeSeatsHeader}"
			style="${rowColor}" sortable="true" />

		<security:authorize access="hasRole('CHORBI')">

			<jstl:set var="aux" value="${0}" />
			<jstl:if test="${chorbi.getEvents().contains(row)}">
				<jstl:set var="aux" value="${1}" />
			</jstl:if>
			<jstl:if test="${row.freeSeats <= 0}">
				<jstl:set var="aux" value="${1}" />
			</jstl:if>
			<jstl:if test="${row.getType().equals('pastEvent')}">
				<jstl:set var="aux" value="${1}" />
			</jstl:if>


			<display:column title="">
				<jstl:set var="chorbi" value="${chorbi}" />
				<jstl:if test="${aux != 1}">
					<acme:button href="event/chorbi/register.do?eventID=${row.id}"
						name="register" code="event.register" />
				</jstl:if>
			</display:column>
		</security:authorize>



	</jstl:if>

	<!-- Este listado es para manager -->

	<jstl:if test="${requestURI.contains('manager')}">

		<security:authorize access="hasRole('MANAGER')">
			<display:column title="${editHeader}" sortable="false">
				<jstl:if test="${!row.type.equals('pastEvent')}">
					<input type="button" name="edit"
						value="<spring:message code="event.manager.edit" />"
						onclick="javascript: window.location.replace('event/manager/edit.do?eventId=${row.id}');" />
				</jstl:if>
			</display:column>
		</security:authorize>

		<spring:message code="event.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}"
			sortable="true" />

		<spring:message code="event.organisedMoment"
			var="organisedMomentHeader" />
		<display:column property="organisedMoment"
			title="${organisedMomentHeader}" sortable="true"
			sortProperty="organisedMoment" format="{0,date,dd/MM/YYYY HH:mm}" />

		<spring:message code="event.description" var="descriptionHeader" />
		<display:column property="description" title="${descriptionHeader}" />

		<spring:message code="event.picture" var="pictureHeader" />
		<display:column title="${pictureHeader}">
			<IMG src="${row.picture}" width="100" height="100">
		</display:column>

		<spring:message code="event.seats" var="seatsHeader" />
		<display:column property="seats" title="${seatsHeader}"
			sortable="true" />

		<spring:message code="event.freeSeats" var="freeSeatsHeader" />
		<display:column property="freeSeats" title="${freeSeatsHeader}"
			sortable="true" />

		<security:authorize access="hasRole('MANAGER')">
			<spring:message code="event.chorbies" var="chorbiesHeader" />
			<display:column title="${chorbiesHeader}">
				<input type="button" name="see"
					value="<spring:message code="event.see" />"
					onclick="javascript: window.location.replace('chorbi/manager/list.do?eventId=${row.id}');" />
			</display:column>
		</security:authorize>

		<security:authorize access="hasRole('MANAGER')">
			<spring:message code="event.broadcast" var="broadcastHeader" />
			<display:column title="${broadcastHeader}">
				<input type="button" name="broadcast"
					value="<spring:message code="event.broadcast" />"
					onclick="javascript: window.location.replace('chirp/broadcast.do?eventId=${row.id}');" />
			</display:column>
		</security:authorize>



	</jstl:if>

	<!-- Este listado es para chorbi -->

	<jstl:if test="${requestURI.contains('chorbi')}">


		<spring:message code="event.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}"
			sortable="true" />

		<spring:message code="event.organisedMoment"
			var="organisedMomentHeader" />
		<display:column property="organisedMoment"
			title="${organisedMomentHeader}" sortable="true"
			sortProperty="organisedMoment" format="{0,date,dd/MM/YYYY HH:mm}" />

		<spring:message code="event.description" var="descriptionHeader" />
		<display:column property="description" title="${descriptionHeader}" />

		<spring:message code="event.picture" var="pictureHeader" />
		<display:column title="${pictureHeader}">
			<IMG src="${row.picture}" width="100" height="100">
		</display:column>

		<spring:message code="event.seats" var="seatsHeader" />
		<display:column property="seats" title="${seatsHeader}"
			sortable="true" />

		<spring:message code="event.freeSeats" var="freeSeatsHeader" />
		<display:column property="freeSeats" title="${freeSeatsHeader}"
			sortable="true" />

		<security:authorize access="hasRole('CHORBI')">
			<display:column title="">
				<acme:button href="event/chorbi/unregister.do?eventID=${row.id}"
					name="unregister" code="event.unregister" />
			</display:column>
		</security:authorize>


	</jstl:if>


</display:table>

<jstl:if
	test="${!requestURI.contains('manager') && !requestURI.contains('chorbi')}">

	<jstl:if test="${!requestURI.contains('specialList')}">

		<p style="background-color: #800080">
			<spring:message code="event.rowColor2" />
		</p>
		<p style="background-color: #808080">
			<spring:message code="event.rowColor1" />

		</p>
	</jstl:if>


</jstl:if>