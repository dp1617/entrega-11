<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="likes" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">
	
	<spring:message code="chorbi.picture" var="pictureHeader" />
	<display:column title="${pictureHeader}" >
		<IMG src="${row.getLikingChorbi().picture}" width="100" height="100">
	</display:column>
	
	<spring:message code="chorbi.name" var="nameHeader" />
	<display:column title="${nameHeader}">
		${row.getLikingChorbi().name}
	</display:column>
	
	<spring:message code="chorbi.surname" var="surnameHeader" />
	<display:column title="${surnameHeader}">
		${row.getLikingChorbi().surname}
	</display:column>
	
	<spring:message code="chorbi.email" var="emailHeader" />
	<display:column title="${emailHeader}">
		${row.getLikingChorbi().email}
	</display:column>
	
	<spring:message code="chorbi.phone" var="phoneHeader" />
	<display:column title="${phoneHeader}">
		${row.getLikingChorbi().phone}
	</display:column>
	
	<spring:message code="chorbi.description" var="descriptionHeader" />
	<display:column title="${descriptionHeader}">
		${row.getLikingChorbi().description}
	</display:column>
	
	<spring:message code="chorbi.kindOfRelationship" var="kindOfRelationshipHeader" />
	<display:column  title="kindOfRelationshipHeader" sortable="true">
		${row.getLikingChorbi().kindOfRelationship}
	</display:column>

	<spring:message code="chorbi.birthDate" var="birthDateHeader" />
	<display:column title="${birthDateHeader}" sortable="true" format="{0,date,dd/MM/YYYY}">
		${row.getLikingChorbi().getBirthDate().getDate()}/${row.getLikingChorbi().getBirthDate().getMonth()+1}/${row.getLikingChorbi().getBirthDate().getYear()}	
	</display:column>

	<spring:message code="chorbi.genre" var="genreHeader" />
	<display:column title="${genreHeader}" sortable="true">
		${row.getLikingChorbi().genre}
	</display:column>
	
	<!-- TODO: CreditCard -->
	
	<spring:message code="chorbi.country" var="countryHeader" />
	<display:column title="${countryHeader}" sortable="true">
		${row.getLikingChorbi().country}
	</display:column>
	
	<spring:message code="chorbi.state" var="stateHeader" />
	<display:column title="${stateHeader}" sortable="true">
		${row.getLikingChorbi().state}
	</display:column>
	
	<spring:message code="chorbi.province" var="provinceHeader" />
	<display:column title="${provinceHeader}" sortable="true">
		${row.getLikingChorbi().province}
	</display:column>
	
	<spring:message code="chorbi.city" var="cityHeader" />
	<display:column title="${cityHeader}" sortable="true">
		${row.getLikingChorbi().city}
	</display:column>
	
	<spring:message code="chorbi.likes" var="likesHeader" />
	<display:column title="${likesHeader}" sortable="true">
		<acme:button href="chorbi/chorbi/likes.do?chorbiID=${row.getLikingChorbi().id}"
			name="see" code="chorbi.see" />
	</display:column>
	
	<spring:message code="chorbi.likes.comment" var="likeCommentHeader"/> 
	<display:column property="comment" title="${likeCommentHeader}" sortable="false"/>
	
	<spring:message code="chorbi.likes.numberStars" var="likeNumberStarsHeader"/> 
	<display:column property="numberStars" title="${likeNumberStarsHeader}" sortable="true"/>
	
	<jsp:useBean id="loginService" class="security.LoginService"
				scope="page" />
				
	<jstl:set var="aux" value="${0}"/>
				
	<jstl:forEach var="c" items="${toIterate}">
		<jstl:if test="${c.getLikedChorbi().getId() == row.getLikingChorbi().id}">
			<jstl:set var="aux" value="${aux + 1}"/>
		</jstl:if>
	</jstl:forEach>
				
	<security:authorize access="hasRole('CHORBI')">
		<display:column sortable="false">
			<jstl:if test="${row.getLikingChorbi().getUserAccount().getId() != loginService.getPrincipal().getId()}">
				<jstl:if test="${aux == 0}">
					<a href="likes/chorbi/create.do?chorbiId=${row.getLikingChorbi().id}"><button><spring:message code="chorbi.like"/></button></a>
				</jstl:if>
			</jstl:if>
		</display:column>
		
		<display:column sortable="false">
			<jstl:if test="${row.getLikingChorbi().getUserAccount().getId() != loginService.getPrincipal().getId()}">
				<jstl:if test="${aux != 0}">
					<a href="likes/chorbi/cancelLikes.do?chorbiId=${row.getLikingChorbi().id}"><button><spring:message code="chorbi.cancelLike"/></button></a>
				</jstl:if>
			</jstl:if>
		</display:column>	
	</security:authorize>
	
	
	
</display:table>