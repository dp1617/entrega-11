<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Creaci�n de chorbi (como usuario no autentificado) -->

<form:form action="likes/chorbi/create.do" modelAttribute="likes">

	<form:hidden path="id"/>
	<form:hidden path="version"/>
	<form:hidden path="likedChorbi"/>
	<form:hidden path="likingChorbi"/>
	<form:hidden path="likedMoment"/>

	<spring:message code="likes.numberStars" />:
	<div class="clasificacion">
		<input id="radio3" type="radio" name="numberStars" value="3"><!--
		--><label for="radio3">&#9733;</label><!--
		--><input id="radio4" type="radio" name="numberStars" value="2"><!--
		--><label for="radio4">&#9733;</label><!--
		--><input id="radio5" type="radio" name="numberStars" value="1"><!--
		--><label for="radio5">&#9733;</label>
	 </div>
	
	<acme:textarea code="like.comment" path="comment" />


	<!-- Acciones -->
	
	<acme:submit name="save" code="like.save"/>
	
	<acme:cancel url="chorbi/list.do" code="like.cancel"/>

</form:form>

<br>

<!-- Errores -->
