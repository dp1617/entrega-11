<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="manager/edit.do" modelAttribute="manager">

	<!-- atributos -->

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount" />
	
	<form:hidden path="sentChirps" />
	<form:hidden path="fees" />
	<form:hidden path="events" />
	
	<!-- Datos para el perfil -->
	
	<b><spring:message code="manager.PersonalData" /></b>

	<br />

	<acme:textbox code="manager.name" path="name" />
	<acme:textbox code="manager.surname" path="surname" />
	<acme:textbox code="manager.phone" path="phone" />
	<acme:textbox code="manager.email" path="email" />
	<acme:textbox code="manager.VATNumber" path="VATNumber" />
	<acme:textbox code="manager.company" path="company" />

	<br />
	
	<!-- Datos para la credit card -->

	<b><spring:message code="manager.CreditCard" /></b>

	<br />

	<acme:textbox code="manager.holderName" path="creditCard.holderName" />
	<acme:textbox code="manager.brandName" path="creditCard.brandName" placeholder="VISA, MASTERCARD, AMEX, DINNERS or DISCOVER"/>
	<acme:textbox code="manager.number" path="creditCard.number" />
	<acme:textbox code="manager.expirationMonth" path="creditCard.expirationMonth" />
	<acme:textbox code="manager.expirationYear" path="creditCard.expirationYear" />
	<acme:textbox code="manager.cvvCode" path="creditCard.cvvCode" />

	<br />
	
	<!-- botones -->
	<acme:submit name="save" code="manager.save" />

	<acme:cancel url="" code="manager.cancel" />

</form:form>
