<%--
 * index.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<div class="systemConfiguration">

	<jstl:if test="${!empty ramdomBanner}">
		<img src="${ramdomBanner}" WIDTH=500 HEIGHT=400 />
	</jstl:if>
	
	<jstl:if test="${creditCardError!=null}">
		<h3 style="color:red;"><spring:message code="chorbi.ValidateCreditCard"/></h3> 
	</jstl:if>

</div>
<p><spring:message code="welcome.greeting.current.time" /> ${moment}</p> 
