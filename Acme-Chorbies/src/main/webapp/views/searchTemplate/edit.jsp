<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de la plantilla de b�squeda -->

<form:form action="searchTemplate/chorbi/edit.do" modelAttribute="searchTemplate">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="foundChorbies" />
	<form:hidden path="lastTimeUsed" />
	<form:hidden path="chorbi" />

	<acme:textbox code="searchTemplate.kindOfRelationship" path="kindOfRelationship" placeholder="activities, love or friendship"/>
	<acme:textbox code="searchTemplate.estimatedAge" path="estimatedAge" />
	<acme:textbox code="searchTemplate.genre" path="genre" placeholder="man or woman" />
	<acme:textbox code="searchTemplate.keyword" path="keyword" />
	<acme:textbox code="searchTemplate.country" path="country" />
	<acme:textbox code="searchTemplate.state" path="state" />
	<acme:textbox code="searchTemplate.province" path="province" />
	<acme:textbox code="searchTemplate.city" path="city" />
	
	<br />

	<acme:submit name="save" code="searchTemplate.save" />

	<acme:cancel url="searchTemplate/chorbi/show.do" code="searchTemplate.cancel" />

</form:form>