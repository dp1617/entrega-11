<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Atributos para mostrar plantilla de b�squeda -->


<div class="searchTemplate">

	<b><spring:message code="searchTemplate.kindOfRelationship" />:</b> ${searchTemplate.kindOfRelationship}<br>
	<b><spring:message code="searchTemplate.estimatedAge" />:</b> ${searchTemplate.estimatedAge}<br>
	<b><spring:message code="searchTemplate.genre" />:</b> ${searchTemplate.genre}<br>
	<b><spring:message code="searchTemplate.keyword" />:</b> ${searchTemplate.keyword}<br>
	<b><spring:message code="searchTemplate.country" />:</b> ${searchTemplate.country}<br>
	<b><spring:message code="searchTemplate.state" />:</b> ${searchTemplate.state}<br>
	<b><spring:message code="searchTemplate.province" />:</b> ${searchTemplate.province}<br>
	<b><spring:message code="searchTemplate.city" />:</b> ${searchTemplate.city}<br>

</div>

<br/>

<input type="submit" name="edit" value="<spring:message code="searchTemplate.edit" />"
	onclick="javascript: window.location.replace('searchTemplate/chorbi/edit.do?searchTemplateId=${searchTemplate.id}');" />




