<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n -->

<jsp:useBean id="chirpService" class="services.ChirpService"
	scope="page" />

<form:form action="chirp/create.do" modelAttribute="mess">

	<!-- Atributos -->

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="sender" />
	<form:hidden path="sentMoment" />
	<form:hidden path="fromActor" />
	<form:hidden path="toChorbi" />

	<jstl:if test="${requestURI.contains('reply.do')}">
	
		<jstl:if test="${mess.recipient == null}">
			<form:label path="recipient">
				<spring:message code="chirp.toChorbi" />
			</form:label>
			<form:select id="recipient" path="recipient">
				<jstl:forEach var="r" items="${recipients}">
					<form:option label="${r.name}" value="${r.id}" />
				</jstl:forEach>
			</form:select>
			<form:errors path="recipient" cssClass="error" />
			<br><br>
			<spring:message code="chirp.selectRecipientAgain" /><br>
			<spring:message code="chirp.selectRecipientAgain2" />
			<br><br>
		</jstl:if>

		<jstl:if test="${mess.recipient != null && mess.sender != null}">
			<form:hidden path="recipient" />
		</jstl:if>
	</jstl:if>

	<jstl:if test="${requestURI.contains('forward.do')}">
		<form:hidden path="subject" />
		<form:hidden path="text" />
		<form:hidden path="attachments" />
	</jstl:if>

	<jstl:if test="${!requestURI.contains('reply.do')}">
		<form:label path="recipient">
			<spring:message code="chirp.toChorbi" />
		</form:label>
		<form:select id="recipient" path="recipient">
			<jstl:forEach var="r" items="${recipients}">
				<form:option label="${r.name}" value="${r.id}" />
			</jstl:forEach>
		</form:select>
		<form:errors path="recipient" cssClass="error" />
	</jstl:if>

	<jstl:if test="${!requestURI.contains('forward.do')}">
		<acme:textbox code="chirp.subject" path="subject" />

		<acme:textbox code="chirp.text" path="text" />

		<acme:textbox code="chirp.attachments" path="attachments" />
	</jstl:if>

	<br />

	<!-- Acciones -->

	<acme:submit name="save" code="chirp.save" />

	<acme:cancel url="" code="chirp.cancel" />
</form:form>