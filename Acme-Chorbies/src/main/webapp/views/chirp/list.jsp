<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:if test="${requestURI.contains('received.do')}">

	<display:table name="receivedChirps" id="row"
		requestURI="${requestURI}" pagesize="5" class="displaytag">
		
		<spring:message code="chirp.fromActor" var="fromActorHeader" />
		<display:column property="fromActor" title="${fromActorHeader}" />

		<spring:message code="chirp.subject" var="subjectHeader" />
		<display:column property="subject" title="${subjectHeader}" sortable="true" />

		<spring:message code="chirp.text" var="textHeader" />
		<display:column property="text" title="${textHeader}" sortable="true" />

		<fmt:formatDate var="sentMoment" value="${row.sentMoment}"
			pattern="dd/MM/yyyy HH:mm" />
		<spring:message code="chirp.sentMoment" var="sentMomentHeader" />
		<display:column value="${sentMoment}" title="${sentMomentHeader}"
			sortable="true" />

		<spring:message code="chirp.attachments" var="attachmentsHeader" />
		<display:column property="attachments" title="${attachmentsHeader}" sortable="true" />
		
		<display:column>
   			<acme:confirmation
    		url="chirp/deleteReceived.do?chirpId=${row.id}"
    		code="chirp.delete" codeConfirm="chirp.confirm.delete" />
  		</display:column>
  		
  		<display:column>
			<acme:button
				href="chirp/reply.do?chirpId=${row.id}"
				name="reply" code="chirp.reply" />
		</display:column>
		
		<display:column>
			<acme:button
				href="chirp/forward.do?chirpId=${row.id}"
				name="forward" code="chirp.forward" />
		</display:column>

	</display:table>
</jstl:if>


<jstl:if test="${requestURI.contains('sent.do')}">

	<display:table name="sentChirps" id="row"
		requestURI="${requestURI}" pagesize="5" class="displaytag">
	
		<spring:message code="chirp.toChorbi" var="toChorbiHeader" />
		<display:column property="toChorbi" title="${toChorbiHeader}" sortable="true" />

		<spring:message code="chirp.subject" var="subjectHeader" />
		<display:column property="subject" title="${subjectHeader}" sortable="true" />

		<spring:message code="chirp.text" var="textHeader" />
		<display:column property="text" title="${textHeader}" sortable="true" />

		<fmt:formatDate var="sentMoment" value="${row.sentMoment}"
			pattern="dd/MM/yyyy HH:mm" />
		<spring:message code="chirp.sentMoment" var="sentMomentHeader" />
		<display:column value="${sentMoment}" title="${sentMomentHeader}"
			sortable="true" />
		
		<spring:message code="chirp.attachments" var="attachmentsHeader" />
		<display:column property="attachments" title="${attachmentsHeader}" sortable="true" />
		
		<display:column>
   			<acme:confirmation
    		url="chirp/deleteSent.do?chirpId=${row.id}"
    		code="chirp.delete" codeConfirm="chirp.confirm.delete" />
  		</display:column>
  		
  		<security:authorize access="hasRole('CHORBI')">
  			<display:column>
				<acme:button
					href="chirp/forward.do?chirpId=${row.id}"
					name="forward" code="chirp.forward" />
			</display:column>
		</security:authorize>

	</display:table>
</jstl:if>	