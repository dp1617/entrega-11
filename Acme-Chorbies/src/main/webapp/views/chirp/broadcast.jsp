<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n -->

<jsp:useBean id="chirpService" class="services.ChirpService"
	scope="page" />

<form:form action="chirp/broadcast.do" modelAttribute="mess">

	<!-- Atributos -->

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="sender" />
	<form:hidden path="sentMoment" />
	<form:hidden path="fromActor" />
	<form:hidden path="toChorbi" />
	<form:hidden path="recipient" />
		
	<acme:textbox code="chirp.subject" path="subject" />

	<acme:textbox code="chirp.text" path="text" />

	<acme:textbox code="chirp.attachments" path="attachments" />


	<br />

	<!-- Acciones -->

	<acme:submit name="sendBroadcast" code="chirp.broadcast" />

	<acme:cancel url="event/manager/list.do" code="chirp.cancel" />
</form:form>