<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<spring:message code="fee.info" />
<br>
<br>
<input type="submit" name="updateChorbiMonthlyFees"
	value="<spring:message code="fee.updateChorbiMonthlyFees" />"
	onclick="javascript: window.location.replace('fee/administrator/updateChorbiMonthlyFees.do') " />
<br>

<!-- Listado -->

<display:table name="fee" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<!-- Atributos -->

	<spring:message code="fee.chorbi" var="chorbiHeader" />
	<display:column value="${row.chorbi.name}" title="${chorbiHeader}"
		sortable="true" />

	<spring:message code="fee.value" var="valueHeader" />
	<display:column property="value" title="${valueHeader}" sortable="true" />

</display:table>

