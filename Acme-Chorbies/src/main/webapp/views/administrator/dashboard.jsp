<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- ACME-CHORBIES 1.0 QUERIES -->

<legend><b><spring:message code="administrator.queriesChorbiesv1"/></b></legend>

<!-- LEVEL C -->

<fieldset>
	<legend><b><spring:message code="administrator.numChorbiesPerCountryAndCity"/></b></legend>
	
	<display:table class="displaytag" name="numChorbiesPerCountryAndCity" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="country" property="[0]"/>
  		<acme:column code="city" property="[1]"/>
  		<acme:column code="number" property="[2]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.minMaxAvgAgeOfChorbies"/></b></legend>
	
	<display:table class="displaytag" name="minMaxAvgAgeOfChorbies" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="min" property="[0]"/>
  		<acme:column code="avg" property="[1]"/>
  		<acme:column code="max" property="[2]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.ratioChorbiesInvalidCreditcard"/></b></legend>
	
	<spring:message code="administrator.ratioChorbiesInvalidCreditcard" /><br>
	<jstl:out value="${ratioChorbiesInvalidCreditcard}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.ratioChorbiesSearchActivities"/></b></legend>
	
	<spring:message code="administrator.ratioChorbiesSearchActivities" /><br>
	<jstl:out value="${ratioChorbiesSearchActivities}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.ratioChorbiesSearchFriendship"/></b></legend>
	
	<spring:message code="administrator.ratioChorbiesSearchFriendship" /><br>
	<jstl:out value="${ratioChorbiesSearchFriendship}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.ratioChorbiesSearchLove"/></b></legend>
	
	<spring:message code="administrator.ratioChorbiesSearchLove" /><br>
	<jstl:out value="${ratioChorbiesSearchLove}"></jstl:out><br>
</fieldset>

<!-- LEVEL B -->

<fieldset>
	<legend><b><spring:message code="administrator.chorbiesOrdeByNumberOfLikes"/></b></legend>
	
	<display:table name="chorbiesOrdeByNumberOfLikes" id="row"
	requestURI="${requestURI}" pagesize="10" class="displaytag">
	
	<jstl:out value="${chorbiesOrdeByNumberOfLikes}"></jstl:out>

</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.minMaxAvgLikesPerChorbi"/></b></legend>
	
	<display:table class="displaytag" name="minMaxAvgLikesPerChorbi" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="min" property="[0]"/>
  		<acme:column code="avg" property="[1]"/>
  		<acme:column code="max" property="[2]"/>
</display:table>
</fieldset>

<!-- LEVEL A -->

<fieldset>
	<legend><b><spring:message code="administrator.minMaxAvgReceivedChirps"/></b></legend>
	
	<display:table class="displaytag" name="minMaxAvgReceivedChirps" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="min" property="[0]"/>
  		<acme:column code="avg" property="[1]"/>
  		<acme:column code="max" property="[2]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.minMaxAvgSentChirps"/></b></legend>
	
	<display:table class="displaytag" name="minMaxAvgSentChirps" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="min" property="[0]"/>
  		<acme:column code="avg" property="[1]"/>
  		<acme:column code="max" property="[2]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.chorbiMoreChirpsReceived"/></b></legend>
	
	<display:table name="chorbiMoreChirpsReceived" id="row"
	requestURI="${requestURI}" pagesize="5" class="displaytag">
	
	<jstl:out value="${chorbiMoreChirpsReceived}"></jstl:out>

</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.chorbiMoreChirpsSent"/></b></legend>
	
	<display:table name="chorbiMoreChirpsSent" id="row"
	requestURI="${requestURI}" pagesize="5" class="displaytag">
	
	<jstl:out value="${chorbiMoreChirpsSent}"></jstl:out>

</display:table>
</fieldset>

<!-- ACME-CHORBIES 2.0 QUERIES -->

<legend><b><spring:message code="administrator.queriesChorbiesv2"/></b></legend>

<fieldset>
	<legend><b><spring:message code="administrator.managersSortedByNumberOfEvents"/></b></legend>
	
	<display:table class="displaytag" name="managersSortedByNumberOfEvents" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="surname" property="[0]"/>
  		<acme:column code="name" property="[1]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.managersAndAmountDueInFees"/></b></legend>
	
	<display:table class="displaytag" name="managersAndAmountDueInFees" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="surname" property="[0]"/>
  		<acme:column code="name" property="[1]"/>
  		<acme:column code="due" property="[2]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.listChorbiesOrderByEventsSize"/></b></legend>
	
	<display:table class="displaytag" name="listChorbiesOrderByEventsSize" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="surname" property="[0]"/>
  		<acme:column code="name" property="[1]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.chorbiesAndAmountDueInFees"/></b></legend>
	
	<display:table class="displaytag" name="chorbiesAndAmountDueInFees" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="surname" property="[0]"/>
  		<acme:column code="name" property="[1]"/>
  		<acme:column code="due" property="[2]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.minMaxAvgStarsPerChorbi"/></b></legend>
	
	<display:table class="displaytag" name="minMaxAvgStarsPerChorbi" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="name" property="[0]"/>
  		<acme:column code="min" property="[1]"/>
  		<acme:column code="max" property="[2]"/>
  		<acme:column code="max" property="[2]"/>
</display:table>
</fieldset>


<fieldset>
	<legend><b><spring:message code="administrator.chorbiesSortedByStars"/></b></legend>
	
	<display:table class="displaytag" name="chorbiesSortedByStars" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="surname" property="[0]"/>
  		<acme:column code="name" property="[1]"/>
  		<acme:column code="stars" property="[2]"/>
</display:table>
</fieldset>


