<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<spring:message code="tax.info" />
<br>
<spring:message code="tax.info2" />
<br>

<display:table name="tax" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<!-- Atributos -->
	<spring:message code="tax.managerValue" var="managerValueHeader" />
	<display:column property="managerValue" title="${managerValueHeader}" />

	<spring:message code="tax.chorbiValue" var="chorbiValueHeader" />
	<display:column property="chorbiValue" title="${chorbiValueHeader}" />

	<security:authorize access="hasRole('ADMIN')">
		<display:column>
			<a href="tax/administrator/modify.do?taxID=${row.id}"> <spring:message
					code="tax.modify" />
			</a>
		</display:column>
	</security:authorize>

</display:table>