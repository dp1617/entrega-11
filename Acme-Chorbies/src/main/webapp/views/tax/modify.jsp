<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<form:form action="tax/administrator/modify.do" modelAttribute="tax">

	<form:hidden path="id" />
	<form:hidden path="version" />

	<form:label path="managerValue">
		<spring:message code="tax.managerValue" />
	</form:label>
	<form:input path="managerValue" />
	<form:errors class="error" path="managerValue" />
	<br />

	<form:label path="chorbiValue">
		<spring:message code="tax.chorbiValue" />
	</form:label>
	<form:input path="chorbiValue" />
	<form:errors class="error" path="chorbiValue" />
	<br />

	<input type="submit" name="save"
		value="<spring:message code="tax.save" />" />&nbsp;

	<spring:message code="tax.cancelConfirm" var="cancelConfirm" />
	<input type="button" value="<spring:message code="tax.goBack" />"
		onclick="javascript: window.location.replace('tax/administrator/list.do')" />

</form:form>
