<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="chorbies" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<security:authorize access="hasRole('ADMIN')">
		<display:column title="${banHeader}" sortable="false">
			<jstl:if test="${row.banned == false}">
				<input type="submit" name="ban" value="<spring:message code="chorbi.ban" />"
					onclick="javascript: window.location.replace('chorbi/administrator/ban.do?chorbiId=${row.id}');" />
			</jstl:if>
		</display:column>
		
		<display:column title="${unbanHeader}" sortable="false">
			<jstl:if test="${row.banned == true}">
				<input type="submit" name="ban" value="<spring:message code="chorbi.unban" />"
					onclick="javascript: window.location.replace('chorbi/administrator/unban.do?chorbiId=${row.id}');" />
			</jstl:if>
		</display:column>
	</security:authorize>
	
	<spring:message code="chorbi.picture" var="pictureHeader" />
	<display:column title="${pictureHeader}" >
		<IMG src="${row.picture}" width="100" height="100">
	</display:column>
	
	<spring:message code="chorbi.name" var="nameHeader" />
	<display:column property="name" title="${nameHeader}" />
	
	<spring:message code="chorbi.surname" var="surnameHeader" />
	<display:column property="surname" title="${surnameHeader}" />
	
	<spring:message code="chorbi.email" var="emailHeader" />
	<display:column property="email" title="${emailHeader}" />
	
	<spring:message code="chorbi.phone" var="phoneHeader" />
	<display:column property="phone" title="${phoneHeader}" />
	
	<spring:message code="chorbi.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}" />
	
	<spring:message code="chorbi.kindOfRelationship" var="kindOfRelationshipHeader" />
	<display:column property="kindOfRelationship" title="${kindOfRelationshipHeader}" sortable="true"/>

	<spring:message code="chorbi.birthDate" var="birthDateHeader" />
	<display:column property="birthDate" title="${birthDateHeader}" 
		sortable="true" sortProperty="birthDate" format="{0,date,dd/MM/YYYY}" />

	<spring:message code="chorbi.genre" var="genreHeader" />
	<display:column property="genre" title="${genreHeader}" sortable="true"/>
	
	<!-- TODO: CreditCard -->
	
	<spring:message code="chorbi.country" var="countryHeader" />
	<display:column property="country" title="${countryHeader}" sortable="true"/>
	
	<spring:message code="chorbi.state" var="stateHeader" />
	<display:column property="state" title="${stateHeader}" sortable="true"/>
	
	<spring:message code="chorbi.province" var="provinceHeader" />
	<display:column property="province" title="${provinceHeader}" sortable="true"/>
	
	<spring:message code="chorbi.city" var="cityHeader" />
	<display:column property="city" title="${cityHeader}" sortable="true"/>
	
	<spring:message code="chorbi.likes" var="likesHeader" />
	<display:column title="${likesHeader}" sortable="true">
		<acme:button href="chorbi/chorbi/likes.do?chorbiID=${row.id}"
			name="see" code="chorbi.see" />
	</display:column>
	
	<jstl:if test="${likes!=null}">
			<spring:message code="chorbi.likes.comment" var="likeCommentHeader"/> 
			<display:column title="${likeCommentHeader}" sortable="false">
				<jstl:forEach var="like" items="${likes}">
					<jstl:if test="${like.getLikingChorbi().getId() == row.id}">
						${like.comment}
					</jstl:if>
				</jstl:forEach>
			</display:column>
			
			<spring:message code="chorbi.likes.numberStars" var="likeNumberStarsHeader"/> 
			<display:column title="${likeNumberStarsHeader}" sortable="true">
				<jstl:forEach var="like" items="${likes}">
					<jstl:if test="${like.getLikingChorbi().getId() == row.id}">
						${like.numberStars}
					</jstl:if>
				</jstl:forEach>
			</display:column>
	</jstl:if>
	
	<jsp:useBean id="loginService" class="security.LoginService"
				scope="page" />
				
	<jstl:set var="aux" value="${0}"/>
				
	<jstl:forEach var="c" items="${toIterate}">
		<jstl:if test="${c.getLikedChorbi().getId() == row.id}">
			<jstl:set var="aux" value="${aux + 1}"/>
		</jstl:if>
	</jstl:forEach>
				
	<security:authorize access="hasRole('CHORBI')">
		<display:column sortable="false">
			<jstl:if test="${row.getUserAccount().getId() != loginService.getPrincipal().getId()}">
				<jstl:if test="${aux == 0}">
					<a href="likes/chorbi/create.do?chorbiId=${row.id}"><button><spring:message code="chorbi.like"/></button></a>
				</jstl:if>
			</jstl:if>
		</display:column>
		
		<display:column sortable="false">
			<jstl:if test="${row.getUserAccount().getId() != loginService.getPrincipal().getId()}">
				<jstl:if test="${aux != 0}">
					<a href="likes/chorbi/cancelLikes.do?chorbiId=${row.id}"><button><spring:message code="chorbi.cancelLike"/></button></a>
				</jstl:if>
			</jstl:if>
		</display:column>	
	</security:authorize>
	
	
	
</display:table>