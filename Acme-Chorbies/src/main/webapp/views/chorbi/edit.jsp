<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="chorbi/edit.do" modelAttribute="chorbi">

	<!-- atributos -->

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount" />
	
	<form:hidden path="banned" />
	<form:hidden path="sentLikes" />
	<form:hidden path="receivedLikes" />
	<form:hidden path="searchTemplate" />
	<form:hidden path="sentChirps" />
	<form:hidden path="receivedChirps" />
	
	<!-- Datos para el perfil -->
	
	<b><spring:message code="chorbi.PersonalData" /></b>

	<br />

	<acme:textbox code="chorbi.name" path="name" />
	<acme:textbox code="chorbi.surname" path="surname" />
	<acme:textbox code="chorbi.phone" path="phone" />
	<acme:textbox code="chorbi.email" path="email" />
	<acme:textbox code="chorbi.picture" path="picture" />
	<acme:textbox code="chorbi.description" path="description" />
	<acme:textbox code="chorbi.kindOfRelationship" path="kindOfRelationship" placeholder="activities, friendship or love"/>
	<acme:textbox code="chorbi.birthDate" path="birthDate" placeholder="dd/MM/yyyy"/>
	<acme:textbox code="chorbi.genre" path="genre" placeholder="man or woman"/>
	<acme:textbox code="chorbi.country" path="country" />
	<acme:textbox code="chorbi.state" path="state" />
	<acme:textbox code="chorbi.province" path="province" />
	<acme:textbox code="chorbi.city" path="city" />

	<br />
	
	<!-- Datos para la credit card -->

	<b><spring:message code="chorbi.CreditCard" /></b>

	<br />

	<acme:textbox code="chorbi.holderName" path="creditCard.holderName" />
	<acme:textbox code="chorbi.brandName" path="creditCard.brandName" placeholder="VISA, MASTERCARD, AMEX, DINNERS or DISCOVER"/>
	<acme:textbox code="chorbi.number" path="creditCard.number" />
	<acme:textbox code="chorbi.expirationMonth" path="creditCard.expirationMonth" />
	<acme:textbox code="chorbi.expirationYear" path="creditCard.expirationYear" />
	<acme:textbox code="chorbi.cvvCode" path="creditCard.cvvCode" />

	<br />
	
	<!-- botones -->
	<acme:submit name="save" code="chorbi.save" />

	<acme:cancel url="" code="chorbi.cancel" />

</form:form>
