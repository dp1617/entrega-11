<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Creaci�n de chorbi (como usuario no autentificado) -->

<form:form action="chorbi/create.do" modelAttribute="chorbiForm">

	<!-- Campos obligatorios a rellenar -->

	<b><spring:message code="chorbi.PersonalData" /></b>

	<br />

	<acme:textbox code="chorbi.name" path="name" />

	<acme:textbox code="chorbi.surname" path="surname" />

	<acme:textbox code="chorbi.phone" path="phone" />

	<acme:textbox code="chorbi.email" path="email" />
	
	<acme:textbox code="chorbi.picture" path="picture" />
	
	<acme:textbox code="chorbi.description" path="description" />
	
	<acme:textbox code="chorbi.kindOfRelationship" path="kindOfRelationship" placeholder="activities, friendship or love"/>
	
	<acme:textbox code="chorbi.birthDate" path="birthDate" placeholder="dd/MM/yyyy"/>
	
	<acme:textbox code="chorbi.genre" path="genre" placeholder="man or woman"/>
	
	<acme:textbox code="chorbi.country" path="country" />
	
	<acme:textbox code="chorbi.state" path="state" />
	
	<acme:textbox code="chorbi.province" path="province" />
	
	<acme:textbox code="chorbi.city" path="city" />

	<br />

	<!-- Usuario y contrase�a -->

	<b><spring:message code="chorbi.LoginData" /></b>

	<br />

	<acme:textbox code="chorbi.username" path="username" />

	<acme:password code="chorbi.password" path="password" />

	<acme:password code="chorbi.secondPassword" path="secondPassword" />

	<br />

	<!-- Datos para la credit card -->

	<b><spring:message code="chorbi.CreditCard" /></b>

	<br />

	<acme:textbox code="chorbi.holderName" path="holderName" />

	<acme:textbox code="chorbi.brandName" path="brandName" placeholder="VISA, MASTERCARD, AMEX, DINNERS or DISCOVER"/>

	<acme:textbox code="chorbi.number" path="number" />

	<acme:textbox code="chorbi.expirationMonth" path="expirationMonth" />

	<acme:textbox code="chorbi.expirationYear" path="expirationYear" />

	<acme:textbox code="chorbi.cvvCode" path="cvvCode" />

	<br />

	<!-- Aceptar para continuar -->

	<form:label path="checkBox">
		<spring:message code="chorbi.checkBox" />
	</form:label>
	<form:checkbox path="checkBox" />
	<a href="misc/terms.do"> <spring:message
			code="chorbi.moreInfo" />
	</a>
	<form:errors class="error" path="checkBox" />

	<br />
	<br />

	<!-- Acciones -->
	
	<acme:submit name="save" code="chorbi.signIn"/>
	
	<acme:cancel url="" code="chorbi.cancel"/>

</form:form>

<br>

<!-- Errores -->

<jstl:if test="${phoneError != null}">
	<span class="message"><spring:message code="${phoneError}" /></span>
</jstl:if>

<jstl:if test="${passwordMatch != null}">
	<span class="message"><spring:message code="${pass}" /></span>
</jstl:if>

<jstl:if test="${duplicate != null}">
	<span class="message"><spring:message code="${duplicate}" /></span>
</jstl:if>

<jstl:if test="${terms != null}">
	<span class="message"><spring:message code="${terms}" /></span>
</jstl:if>
